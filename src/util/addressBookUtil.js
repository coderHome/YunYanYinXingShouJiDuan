/**
 * Created by qianyf on 2018/1/2.
 */
export function categryFriends(arr){
    if(!String.prototype.localeCompare)
        return null;
    var segs = [];
    var curr;
    var charArray = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ#';
    var letters = charArray.split('');
    for(var i = 0;i<letters.length;i++){
        let id = letters[i];
        curr = {key: id, data:[]};
        arr.forEach(function(book,index){
            let firstLetter = book.spell.substring(0,1).toUpperCase();
            if(firstLetter == id){
                curr.data.push(book);
            }
            if(id == '#'){
                if(charArray.indexOf(firstLetter)== -1){
                    curr.data.push(book);
                }
            }
        });
        if(curr.data.length) {
            segs.push(curr);
            curr.data.sort(function(a,b){
                let aSpell = a.spell;
                let bSpell = b.spell;
                return aSpell.localeCompare(bSpell);
            });
        }
    }
    return segs;
}

export function getMembersByGroupId(members,groupId){
    let results = members.filter((item)=>{
        return item.groupId == groupId;
    })
    return results;
}


/**
 * 对通讯录进行分组
 * @param groupData
 * @param members
 * @returns {Array}
 */
export function sortAllMembersByGroup(groupData,members){
    var results = [];
    var sum= 0;
    for(var i = 0;i<groupData.length;i++){
        var tmpGroupId = groupData[i].id;
        var tmpGroupName = groupData[i].name;
        var group = {
            id:tmpGroupId,
            name:tmpGroupName,
            data:[]
        }
        for(var j=0;j<members.length;j++){
            sum++;
            if(members[j].groupId == tmpGroupId){
                group.data.push(members[j]);
            }
        }

        results.push(group);
    }

    var undefinedGroup = {
        id:'0',
        name:'未分组',
        data:[]
    }
    for(var i=0;i<members.length;i++){
        let flag = 0;
        for(var j=0;j<groupData.length;j++){
            if(members[i].groupId == groupData[j].id){
                flag = 1;
                break;
            }
        }
        if(flag == 0){
            undefinedGroup.data.push(members[i]);
        }
    }
    var arrayIndex = results.findIndex((item)=>{
        return item.id == '0';
    })
    results[arrayIndex].data = undefinedGroup.data;
    return results;
}

export function getNavigationKeyByRouterName(routers,routerName){
    console.log("routers========>",routers);
    // const router = routers.filter((value)=>{
    //     return value.routeName == routerName
    // });
    let index;
    for(var i=0;i<routers.length;i++){
        if(routers[i].routeName == routerName){
            index = i;
            break;
        }
    }
    return routers[index+1].key;
}

export function sortMembersTag(origineMembers){
    var tagArr = [];
    origineMembers.forEach((item)=>{
        var tmpTips = item.tips;
        if(tmpTips){
            tmpTips.forEach((element,i)=>{
                if(tagArr[element.name]==undefined){
                    var tmpArr = [];
                    tagArr[element.name] = tmpArr;
                }
                tagArr[element.name].push(item);
            });
        }
    });
    return tagArr;
}

export function tagMapToList(tagMap){
    var arr = [];
    for(var key in tagMap){
        var sum = tagMap[key].length;
        var item = {
            name : key,
            sum : sum,
            data : tagMap[key]
        }
        arr.push(item);
    }
    return arr;
}

export function copyValue(obj){
    let result = JSON.parse(JSON.stringify(obj));
    return result;
}