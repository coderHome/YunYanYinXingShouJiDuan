/**
 * Created by wangdi on 1/12/16.
 */
'use strict';

import {Share} from 'react-native';
import * as WeChat from 'react-native-wechat';
let resolveAssetSource = require('resolveAssetSource');
import {Toast} from  'antd-mobile/lib'
export default class ShareUtil {

    share(content, url) {
        Share.share({
            message: content,
            url: url,
            title: content
        }).then(this._showResult).catch((error)=> {
            // Toast.show('分享失败', {position: -80});
            console.log('分享失败');
        });
    }

    _showResult(result) {
        if (result.action === Share.sharedAction) {
            //Toast.show('分享成功',{position: px2dp(-80)});
        }
    }

    shareWeChatDefaultImage() {
        WeChat.isWXAppInstalled()
            .then((isInstalled) => {
                if (isInstalled) {
                    try {
                        let imageResource = require('../assets/Image/login/shareImage.jpeg');
                        let result = WeChat.shareToSession({
                            type: 'imageUrl',
                            title: 'web image',
                            description: 'share web image to time line',
                            mediaTagName: 'email signature',
                            messageAction: 'messageAction',
                            messageExt: 'messageExt',
                            // imageUrl: 'http://www.csrcbank.com/images/3.png'
                            imageUrl: resolveAssetSource(imageResource).uri,
                            thumbImage: resolveAssetSource(imageResource).uri,
                        });
                        console.log('share image url to time line successful:', result);
                    } catch (e) {
                        if (e instanceof WeChat.WechatError) {
                            console.error(e.stack);
                        } else {
                            throw e;
                        }
                    }
                } else {
                    Toast.info("没有安装微信软件，请您安装微信之后再试",1);
                }
            });
    }

    shareWeChatImage(imageUrl) {
        WeChat.isWXAppInstalled()
            .then((isInstalled) => {
                if (isInstalled) {
                    try {
                        WeChat.shareToSession({
                            type: 'imageUrl',
                            title: '分享',
                            description: 'share web image to time line',
                            mediaTagName: 'email signature',
                            messageAction: 'messageAction',
                            messageExt: 'messageExt',
                            imageUrl: imageUrl,
                            thumbImage: imageUrl,
                        }).then(res=>{
                            console.log('share image url to time line successful:', res);
                        }).catch(e=>{
                            if (e instanceof WeChat.WechatError) {
                                console.error(e.stack);
                            } else {
                                Toast.fail("图片生成失败，请重试",1);
                                throw e;
                            }
                        })

                    } catch (e) {
                        console.error("wechat error:",e);
                        if (e instanceof WeChat.WechatError) {
                            console.error(e.stack);
                        } else {
                            throw e;
                        }
                    }
                } else {
                    Toast.info("没有安装微信软件，请您安装微信之后再试",1);
                }
            });
    }
}