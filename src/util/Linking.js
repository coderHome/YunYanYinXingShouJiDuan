/**
 * Created by crcb on 2017/12/5.
 * 访问原生，比如打电话，访问其他网页等等
 */
import {Linking} from 'react-native'

export default function _openUrl(url){
    Linking.canOpenURL(url).then(supported => {
        if (supported) {
            Linking.openURL(url);
        }else{
            console.log('Can\'t handle url: ' + url);
        }
    }).catch(err => console.error('An error occurred', err));
}