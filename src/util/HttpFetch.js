/**
 * Created by crcb on 2017/12/5.
 */
import Param from '../constant/HttpParam'

import {Toast} from "antd-mobile/lib"
import Crypto from 'crypto-js'
import {store} from '../redux/store/index'
import api from '../constant/APIParam'
import {Modal} from 'antd-mobile/lib'
import React from 'react'
import {AlertIOS} from 'react-native'
import Config from '../constant/globalParam'


const key = Crypto.enc.Utf8.parse(Param.aes_key);
const timeout = Param.timeout;

/**
 * 网络请求入口,根据传进来的参数,封装网络接口。
 * @param url       必传
 * @param data      必传
 * @param loading   非必传
 * @returns {Promise<*>}
 */
var mIsShowDialog;
export default function (url, data, loading,isShowDialog = true) {
    mIsShowDialog = isShowDialog;
    // 非登陆接口，加入token
    const loginToken = global.loginToken;
    if(url!=api.loginApi){
        data.loginToken = loginToken ? loginToken:'';
    }
    // console.log("store = ", store);
    let _loading = loading ? loading : "正在加载";
    if(isShowDialog){
        Toast.loading(_loading, 0);
    }
    let httpUrl = Param.serverUrl + "/" + Param.projectName + "/" + url;
    if(global.loginToken){

    }
    // data.loginToken = global.loginToken?global.loginToken:
    console.log("请求地址 = ", httpUrl);
    console.log("请求参数 = ",data);
    let jsondata = JSON.stringify(data);                        //转成json字符串
    let encptodata = encrypt(jsondata, key);                    //加密json字符串
    // console.log("加密的字符 = ",encptodata)
    let bodys = "request=" + encodeURIComponent(encptodata);    //拼接body格式
    // console.log("body = ",bodys);
    return _fetch(fetch_promise(httpUrl, bodys,isShowDialog), timeout);
}


/**
 * 设置超时
 * @param fetch_promise
 * @param timeout
 * @returns {Promise.<*>}
 * @private
 */
function _fetch(fetch_promise, timeout) {
    let abort_fn = null;
    let abort_promise = new Promise((resolve, reject) => {
        abort_fn = function () {
            reject('连接超时,请检查网络');
        };
    });
    let abortable_promise = Promise.race([
        fetch_promise,
        abort_promise
    ]);
    setTimeout(function () {
        abort_fn();
    }, timeout);

    return abortable_promise;
}

/**
 * 发送请求接口
 * @param url
 * @param bodys
 * @returns {Promise<any>}
 */
function fetch_promise(url, bodys,isShowDialog = true) {
    return new Promise((resolve, reject) => {
        fetch(url, {
            method: Param.method,
            headers: Param.headers,
            body: bodys,
        }).then((response) => {
            console.log("response = ",response)
            let body = decrypt(response._bodyInit, key);
            response._bodyInit = body;
            response._bodyText = body;
            return response.json();
        }).then((json) => {
            // Toast.info("return msg=",json);

            if (json.state === "000000") {
                if(isShowDialog) {
                    Toast.hide();
                }
                resolve(json.data);
            } else if(json.state === '200002'){
                reject("登录超时，请重新登录");
            } else {
                reject(json.message);
            }
        }).catch((err) => {
            reject(err);    //这里可以使用resolve(err),将错误信息传回去
        })
    })
}

/**
 * 加密
 * @param str
 * @returns {string}
 */
function encrypt(str) {
    const cipher = Crypto.AES.encrypt(str, key, {
        mode: Crypto.mode.ECB,
        padding: Crypto.pad.Pkcs7,
    });
    return cipher.toString();
}

/**
 * 解密
 * @param str
 * @returns {*|WordArray|PromiseLike<ArrayBuffer>}
 */
function decrypt(str) {
    // console.log("str = ",str);
    // Toast.fail("0000"+str,12);
    if(!Config.isRelease){
        str = str.replace(/[\r\n\↵]/g, "");         //base64一行不能超过76个字符，超过会默认添加换行符
    }

    let decrypted = Crypto.AES.decrypt(str, key, {
        mode: Crypto.mode.ECB,
        padding: Crypto.pad.Pkcs7,
    });
    decrypted = decrypted.toString(Crypto.enc.Utf8);     //把wordArray对象转utf8字符串
    // decrypted = Crypto.enc.Utf8.stringify(decrypted);
    // Toast.fail("22222"+decrypted,12);
    return decrypted;
}
