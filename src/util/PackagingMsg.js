/**
 * Created by crcb on 2017/12/8.
 * 封装聊天消息
 */
import HttpParam from '../constant/HttpParam'
import getFile from './getFile'
import Fetch from './HttpFetch'
import ApiParam from '../constant/APIParam'
import globalParam from "../constant/globalParam";

/**
 * 封装一条聊天记录
 * @param msg
 * @param callback
 */
export function packagingOneMsg(msg, callback) {
    // console.log("封装了一条聊天记录",msg);
    let _msg = text_go_View(msg);
    callback([_msg]);
}

/**
 * 封装多条聊天记录
 * @param msgs
 * @param callback
 */
export function packagingMoreMsg(msgs, callback) {
    // console.log("封装了多条聊天记录", msgs);
    let _msgs = msgs.map((item, index) => {
        return text_go_View(item);
    });
    // _msgs[_msgs.length-1].lastFlag = true;
    // console.log("——msgs = ",_msgs);
    callback(_msgs);
}

/**
 * 将聊天记录的转为视图模型
 * type:-1系统消息，0文字/表情消息，1图片，2语音，
 * @param msg
 */
function text_go_View(msg) {
    let that =this;
    let _msg = {};
    if (msg.type === -1) {
        _msg.content = msg.content;
    } else if (msg.type === 0) {
        _msg.content = msg0(msg.content);
    } else if (msg.type === 1) {
        _msg.urlSmall = getFile(msg.picId,400);
        _msg.urlBig = getFile(msg.picId);
        // console.log("图片 = ",msg,"大图 = ",_msg.urlBig,"小图 = ",_msg.urlSmall);
    }
    else if (msg.type === 2) {
        // console.log("语音消息 = ",msg);
        _msg.content = getFile(msg.soundId);
        _msg.len = msg.length;
    }else if(msg.type === 5){
        _msg.busId = msg.busId;
        _msg.busType = msg.busType;
        _msg.ctm = msg.ctm;
        _msg.content = "推荐消息";
        global.storageUtil.read_Id("PRD",msg.busId,(info)=>{
            if(info){
                // console.log("有推荐缓存",info)
                _msg.prdName = info.prdName;
                _msg.prdRate = info.prdRate;
                _msg.prdLastTime = info.prdLastTime;
                _msg.prdStartMoney = info.prdStartMoney;
            }else{
                // console.log("没有推荐缓存")
                //发送营销接口
                fetchPrdInfo(msg,(info)=>{
                    // console.log("获取到info = ",info);
                    _msg.prdName = info.prdName;
                    _msg.prdRate = info.prdRate;
                    _msg.prdLastTime = info.prdLastTime;
                    _msg.prdStartMoney = info.prdStartMoney;
                });

            }
        })
    }
    else {
        _msg.content = "未知的消息类型"
    }
    _msg.id = msg.id;
    _msg.type = msg.type;
    _msg.creator = msg.creator;
    _msg.groupId = msg.groupId;
    _msg.time = msg.time;
    // _msg.lastFlag = false;
    return _msg;
}

/**
 * 封装表情文字消息
 * @param _msg
 * @returns {*}
 */
function msg0(_msg) {
    if (_msg != null) {
        let c1 = _msg.split(/(\[bq:[0-9]*\])/);
        let arr = c1.filter((item, index) => {
            if (item !== "") {
                return true
            }
        });
        let arr2 = arr.map((item, index) => {
            let obj = {};
            let beforeIndex = item.indexOf("[bq:");
            let afterIndex = item.indexOf("]");
            if (beforeIndex >= 0 && afterIndex >= 0) {
                obj.bo = true;
                obj.content = item.substring(beforeIndex + 4, afterIndex);
            } else {
                obj.bo = false;
                obj.content = item;
            }
            return obj;
        })
        return arr2;
    } else {
        return "无记录"
    }
}

function fetchPrdInfo(msg,callback) {
    if(msg.busType === "FBSLC"){
        Fetch(ApiParam.getCLWYPrdInfo,{
            id:msg.busId
        }).then((info)=>{
            // console.log("获取封闭式理财产品成功 = ",info);
            info.prdRate = info.prdExpectedyieldtext;
            info.prdLastTime = info.prdMatruetime;
            info.prdStartMoney = info.prdMinprchunit;
            //获取成功，存入缓存
            global.storageUtil.save_Id("PRD",msg.busId,info);
            // return info;
            callback(info);
        }).catch((err)=>{
            // console.log("获取封闭式理财产品失败 = ",err)
            let obj = {
                prdName:"产品已过期",
                prdRate:"0",
                prdLastTime:"0",
                prdStartMoney:"0",
            }
        })
    }else if(msg.busType === "DECD"){
        Fetch(ApiParam.getDECDPrdInfo,{
            id:msg.busId
        }).then((info)=>{
            // console.log("获取大额存单产品成功 = ",info);
            info.prdName = info.prdname;
            info.prdRate = info.exrate;
            info.prdLastTime = info.appenddate;
            info.prdStartMoney = info.minlowamt;
            //获取成功，存入缓存
            global.storageUtil.save_Id("PRD",msg.busId,info);
            callback(info);
        }).catch((err)=>{
            // console.log("获取大额存单产品失败 = ",err)
        })
    }
}