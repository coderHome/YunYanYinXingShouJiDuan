/**
 * Created by qianyf on 2018/1/2.
 */
import Param from '../constant/HttpParam'
export function getPushId(){
    let pushId = Param.deviceId;
    return pushId.replace(/-/g,'');
}


/**
 * 生成获取服务端分享图片流
 * @param prdId
 * @param prdType
 * @param userno
 * @param rec
 * @returns {string}
 */
export function generatorShareUrl(prdId,prdType,userno,rec) {
    let linkUrl = generatorSmartWechatQRUrl(prdId,prdType,userno,rec);
    let shareUrl = Param.serverUrl+'/yan/api/share/getpic.do?id='+prdId+'&prdType='+prdType+'&linkcontent='+encodeURIComponent(linkUrl);
    console.log("shareUrl++++++:",shareUrl);
    return shareUrl;
}

/**
 * 生成小程序二维码
 * @param prdId
 * @param prdType
 * @param userno
 * @param rec
 * @returns {string}
 */
export function generatorSmartWechatQRUrl(prdId,prdType,userno,rec){
    let linkUrl = Param.shareLinkUrl+'?ctm='+userno+'&prdType='+prdType+'&prdId='+prdId+'&rec='+rec;
    console.log("linkUrl++++++:",linkUrl);
    return linkUrl;
}