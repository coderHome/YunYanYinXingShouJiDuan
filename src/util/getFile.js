/**
 * Created by crcb on 2017/11/23.
 */
import Param from '../constant/HttpParam'
import SHA256 from 'crypto-js/sha256'

const _key = Param.fileKey;

export default function getServerfile(id,zoom) {
    if(id===null){
        return null
    }
    let _time = new Date().getTime();
    let _salt = parseInt(Math.random()*1000000000);
    let str = "id=" + id + "&salt=" + _salt + "&time=" + _time + _key;
    let _sign = SHA256(str).toString();
    let url = Param.serverPicUrl+'/file/download/'+id + "?sign="+_sign+"&time="+_time+"&salt="+_salt;
    // let url = "http://170.101.103.157/file/download/"+id + "?sign="+_sign+"&time="+_time+"&salt="+_salt;
    if(zoom){
        url = url+"&zoom="+zoom
    }
    return url;
}
