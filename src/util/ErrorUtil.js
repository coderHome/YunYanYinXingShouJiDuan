/**
 * Created by crcb on 2017/12/8.
 * 错误提示func
 */
import {Toast} from  'antd-mobile/lib'
import {Alert} from 'react-native'

export default function (errorMsg) {

    errorMsg = errorMsg+"";
    // Alert.alert("errormsg = ",errorMsg);
    // console.log("errormsg = ",errorMsg);
    if(errorMsg.indexOf("JSON")>=0){
        console.log("服务器正在维护");
        Toast.fail("服务器正在维护",1);
    }else if(errorMsg.indexOf("Network request failed")>=0){
        Toast.fail("网络不通,请检查网络",1);
    }else if(errorMsg.indexOf("NavigationAction")>=0){
        Toast.fail("客户端损坏",1);
    }else if(errorMsg.indexOf("UTF")>=0){
        Toast.fail("请求参数有误",1);
    }else{
        Toast.fail(errorMsg,1);
    }
}