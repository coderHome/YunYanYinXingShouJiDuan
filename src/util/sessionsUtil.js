import * as dateUtil from './dateUtil';
import {store} from '../redux/store/index';
import * as sessionsActions from '../redux/action/sessionsAction';
// 会话member 字段
const SESSION_MEMBERS=['yanID','name','role'];

function deepCopy(source){

    var result = {};

    for (var key in source) {

        result[key] = typeof source[key] ==='object'? deepCopy(source[key]) : source[key];

    }
    return result;
}

function formatSessions(sessions=[]){
    let newSessions,preMap;
    preMap = deepCopy(sessions);
    preMap.length = sessions.length;
    newSessions = Array.from(preMap);
    newSessions.forEach(item=>{
        transSession(item);
    });

    return newSessions
}

// 检测本地缺少的成员(原方法)
function checkLocalMember(sessions,callback) {
    let mbs=[],localmembers;
    global.storage.load({
        key:'orignMembers',
        autoSync:false,
    }).then(data=>{
        localmembers = data.list;
        sessions.forEach(item=>{
            let mb = localmembers.find(obj=>(item.cusId == obj.yanId));

            if(mb){
                let tempMb={};
                for(let i=0;i<SESSION_MEMBERS.length;i++){
                    SESSION_MEMBERS[i]!='role'?tempMb[SESSION_MEMBERS[i]] = mb[SESSION_MEMBERS[i]]: tempMb[SESSION_MEMBERS[i]] =2;
                }
                mbs.push(mb);
            }
        });
    if(typeof callback=='function'){
        callback(mbs);
    }

    }).catch(e=>{
        console.log(e);
        if(typeof callback=='function'){
            callback(mbs);
        }
    })

}

// 检测本地缺少的成员(新方法)
function getMissMbs(sessions) {
    let ids = [];
    let {sessionsReducer} = store.getState();
    sessions.forEach((group) => {
        let val = group.cusId;
        if(val){
            let m = sessionsReducer.members[val];
            if (m == null && ids.every((id) => (id != val))) {
                ids.push(val);
            }
        }
    })
    return ids
}

// 检测是否已读
function checkMsgread(msg) {
    let {chatReducer} = store.getState();
    let curGroupId = chatReducer.groupId;
    return msg.groupId == curGroupId
}

function getLocalMember(yanId){
    let {members} = store.getState().sessionsReducer;
    let mb = null;
    // let mb = members.find(item=>(item&&item.yanId==yanId));
    yanId&&(mb = members[yanId]);
    return mb
}

function getCusId(session) {
    let cusId = null;

    if ((typeof session == 'object') && session.groupInfo.type == 0 && session.members.length == 2) {
        let {userInfo} = store.getState().userReducer;
        let userId = userInfo.yanId;
        cusId = session.members.find(val => val != userId);
    }

    return cusId
}

function memberName(mb) {
    if (mb) {
        return mb.name?mb.name:mb.nickName;
    } else {
        return '未知';
    }
}

function memberHead(mb) {
    if (mb) {
        if (mb.headIcon) {
            if (mb.headIcon.indexOf('http:') >= 0) {
                return mb.headIcon.replace('http:', 'https:')
            } else if (mb.headIcon.indexOf('https:') >= 0) {
                return mb.headIcon;
            } else if (mb.head == '-1') {
                return '../../assets/Image/customer/head.jpg';
            } else {
                return '../../assets/Image/customer/head.jpg';
            }

        } else {
            return '../../assets/Image/customer/head.jpg';
        }
    } else {
        return '../../assets/Image/customer/head.jpg';
    }
}

//得到具体消息信息的时间展示字符串
function msgTime(time) {
    if(!time){
        return '';
    }
    const today = new Date().setHours(0, 0, 0, 0)
    const hourSpan = (today - time) / 1000 / 60 / 60
    if (hourSpan < 24) {
        return dateUtil.formatDate(new Date(time), "HH:mm")
    } else if (hourSpan < 48) {
        return '昨天'
    } else if (hourSpan < 24 * 7) {
        return dateUtil.formatDate(new Date(time), "www")
    } else {
        return parseInt(hourSpan / 24) + '天前'
    }
}

// 封装sessions数据
function transGroups(session) {
    let {sessionsReducer, userReducer} = store.getState();
    // 多人
    if (session.groupInfo.type == 1) {
        let icons = [], names = [];
        session.members.length > 4 ? session.iconsType = 4 : session.iconsType = session.members.length;
        for (let i = 0; i < session.iconsType; i++) {
            let mb = sessionsReducer.members[i];
            names.push(memberName(mb));
            icons.push(memberHead(mb));
        }
        session.icons = icons;
        session.name = session.groupInfo.name ? session.groupInfo.name : names.join('、');

    }// 专属客服
    else if (session.groupInfo.type == 2) {
        session.iconsType = 1;
        let mb = getLocalMember(session.groupInfo.creator);
        session.icons = [memberHead(mb)]
        session.name = memberName(mb) + '的专属客户经理'

    } // 点对点
    else {
        session.iconsType = 1;
        for (let i = 0; i < session.members.length; i++) {
            if (session.members[i] != userReducer.userInfo.yanId) {
                let mb = getLocalMember(session.members[i]);
                session.icons = memberHead(mb);
                session.name = memberName(mb);
            }
        }
    }
    // 封装最后一条信息
    if (session.lastMsg) {
        transMsg(session.lastMsg);
    }

    return session;
}

//
function transSession(session={}){
    let mb = getLocalMember(session.cusId);
    let headIcon = memberHead(mb);
    session.icon = headIcon;
    session.name = memberName(mb);
    // 封装最后一条信息
    if (!session.lastMsg) {
        session.lastMsg={};
    }
    transMsg(session.lastMsg);
    return session;
}

//封装消息
function transMsg(msg, callback) {
    let {userInfo} = store.getState().userReducer;
    let mb;
    if(msg.creator == userInfo.yanId){
        mb = {...userInfo};
    }else{
        mb = getLocalMember(msg.creator);
    }

   console.log('transMsg',mb)
    msg.timeStr = msgTime(msg.time);
    msg.creatorStr = memberName(mb);
    msg.headIcon = memberHead(mb);
    switch (msg.type) {
        case -1: {
            msg.contentSession = '[系统消息]';
            break;
        }
        case 0: {

            msg.contentSession = msg.content;
            break;
        }
        case 1: {

            msg.contentSession = '[图片]';
            break;
        }
        case 2: {
            msg.content = "[" + msg.length + "秒的语音消息]"
            msg.contentSession = '[语音]';
            break;
        }
        case 3: {

            msg.contentSession = '[视频]';
            break;
        }
        case 4: {

            msg.contentSession = '[链接]';
            break;
        }
        case 5: {

            msg.contentSession = '[业务推荐]';
            break;
        }
        default: {
            msg.contentSession = '[未知的消息]';
        }
    }

    if (typeof callback == "function") {
        callback(msg)
    }

}


export {
    formatSessions,
    getCusId,
    deepCopy,
    transSession,
    getMissMbs,
    checkLocalMember,
    getLocalMember,
    memberName,
    memberHead,
    msgTime,
    transGroups,
    transMsg,
}


