import {AsyncStorage} from 'react-native'

const _time = 1000*60*60*24*7;      //默认时间一个礼拜

class storageUtil{
    /**
     * 存数据,基本key-value结构
     * @param k
     * @param v
     * @param time
     */
    save(k,v,time){
        global.storage.save({
            key:k,
            data:v,
            expires:time?time:_time,
        })
        // console.log("----------数据缓存成功----------");
    }

    /**
     * 读数据,基本key-value结构
     * @param k
     * @param callback
     */
    read(k,callback){
        global.storage.load({
            key:k,
        }).then((data)=>{
            // console.log("----------缓存数据读取成功----------");
            callback(data);
        }).catch((error)=>{
            // console.log("----------缓存数据读取失败----------");
            callback()
        })
    }

    /**
     * 存数据，key-id-value结构
     * @param k
     * @param id
     * @param v
     * @param time
     */
    save_Id(k,id,v,time){
        global.storage.save({
            key:k,
            id:id,
            data:v,
            expires:time?time:_time,
        })
        // console.log("----------数据缓存成功----------");
    }

    /**
     * 读数据,基本key-id-value结构
     * @param k
     * @param id
     * @param callback
     */
    read_Id(k,id,callback){
        global.storage.load({
            key:k,
            id:id,
        }).then((data)=>{
            // console.log("----------缓存数据读取成功----------");
            callback(data);
        }).catch((error)=>{
            // console.log("----------缓存数据读取失败----------");
            callback()
        })
    }

    /**
     * 删除所有数据
     */
    clearAllData(){
        AsyncStorage.clear();
        global.storage.clearMap();      //清除所有key-Id
    }

    /**
     * 清除单个key-id的聊天记录
     * @param key
     * @param id
     */
    clearKeyOneId(key,id) {
        global.storage.clearMapForKey(key)
    }

    /**
     * 清除所有key-id的数据
     */
    clearAllKeyId(){
        global.storage.clearMap();
    }

    /**
     * 批量读取key，返回数组
     * @param keys
     * @param callback
     */
    readMoreKey(keys,callback,errorCallback){
        let ks = [];
        keys.forEach(function (_k) {
            // console.log("_k = ",_k)
            ks.push({
                key:_k
            })
        });
        global.storage.getBatchData(ks).then(result=>{
            // console.log("----------缓存数据读取成功----------",result);
            callback(result);
        }).catch((error)=>{
            // console.log("----------缓存数据读取失败----------");
            errorCallback();
        })
    }

    /**
     * 批量读取key-ids的数据，返回数组
     * @param key
     * @param ids
     * @param callback
     */
    readMoreKey_id(key,ids,callback){
        global.storage.getBatchDataWithIds({
            key:key,
            ids:ids
        }).then((datas)=>{
            callback(datas);
        })
    }
}


global.storageUtil = new storageUtil();