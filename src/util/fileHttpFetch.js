import Param from '../constant/HttpParam'

import RNFetchBlob from 'react-native-fetch-blob'

import {Toast} from "antd-mobile/lib"
import Crypto from 'crypto-js'

import Sha256 from 'crypto-js/sha256'

const key = Param.fileKey;
const timeout = Param.timeout;
// const timeout = Param.timeout;

/**
 * 文件上传接口。
 * @param url       必传
 * @param data      必传
 * @param loading   非必传
 * @returns {Promise<*>}
 */
export default function (url, data, loading) {
    Toast.loading("正在上传", 0);
    // let httpUrl = Param.uploadUrl;
    let httpUrl = Param.serverPicUrl + "/" + url;

    let formData = new FormData();
    let _auth = 0;
    let _mime = data.mime;
    let _time = new Date().getTime();
    let _salt = parseInt(Math.random() * 10000000);
    let str = 'auth=' + _auth + '&mime=' + _mime + '&salt=' + _salt + '&time=' + _time + key;
    let _sign = Sha256(str).toString();
    let file = {
        uri: data.uri,
        type: "multipart/form-data",
        name: data.name,
    };
    formData.append("file", file);
    formData.append("fileName",'file');
    formData.append("auth", _auth);
    formData.append("mime", _mime);
    formData.append("time", _time);
    formData.append("salt", _salt);
    formData.append("sign", _sign);

    console.log("上传的路径 = ", httpUrl);
    console.log("上传的file = ", formData);
    return _fetch(new fetch_promise(httpUrl, formData), timeout);
}


/**
 * 设置超时
 * @param fetch_promise
 * @param timeout
 * @returns {Promise.<*>}
 * @private
 */
function _fetch(fetch_promise, timeout) {
    let abort_fn = null;
    let abort_promise = new Promise((resolve, reject) => {
        abort_fn = function () {
            reject('连接超时,请检查网络');
        };
    });
    let abortable_promise = Promise.race([
        fetch_promise,
        abort_promise
    ]);
    setTimeout(function () {
        abort_fn();
    }, timeout);

    return abortable_promise;
}

/**
 * 发送请求接口
 * @param url
 * @param bodys
 * @returns {Promise<any>}
 */
function fetch_promise(url, bodys) {
    return new Promise((resolve, reject) => {
        fetch(url, {
            method: Param.method,
            headers: Param.picHeaders,
            body: bodys,
        }).then((response) => {
            console.log("response = ", response)
            return response.json();
        }).then((json) => {
            if (json.state === "000000") {
                Toast.hide();
                resolve(json.data);
            } else {
                reject(json.message);
            }
        }).catch((err) => {
            console.log("err")
            reject(err);    //这里可以使用resolve(err),将错误信息传回去
        })
    })
}
