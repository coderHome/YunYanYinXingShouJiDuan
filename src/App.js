/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {View, Text, TextInput,modal,Image, StyleSheet,Platform,NativeAppEventEmitter} from 'react-native'
import { Provider } from 'react-redux';
import AppConfig from './constant/globalParam'
import * as WeChat from 'react-native-wechat';
import {store} from './redux/store/index';
import AppWithNavigationState from './AppNavigator/AppNavigators';
import JPushModule from 'jpush-react-native';
import {Toast} from  'antd-mobile/lib'
import httpParam from './constant/HttpParam'
import PageComponent from './component/backComponent'
import * as Io from './socketIO/index'
export default class App extends PageComponent {
    componentWillMount() {
        super.componentWillMount();
        //微信分享注册
        WeChat.registerApp(httpParam.wechatAppKey);

    }
    componentDidMount() {
        if(Platform.OS === 'ios'){
            JPushModule.addOpenNotificationLaunchAppListener((notification)=>{
                console.log("jpush 添加事件回调，应用没有启动的情况下，点击推送会执行该回调.",notification);
            });
            JPushModule.getBadge((bage)=>{
                console.log("jpush 返回应用 icon badge.",bage);
            });
            JPushModule.addnetworkDidLoginListener(()=>{
                console.log("jpush 网络已登录事件回调");
            })
        }
        else {
            JPushModule.notifyJSDidLoad((resultCode) => {
                console.log("jpush notifyJSDidLoad",resultCode);
            });
            JPushModule.addGetRegistrationIdListener((resultCode) => {
                console.log("jpush registrationId",resultCode);
            });
            //开启上报崩溃日志
            JPushModule.crashLogON();
        }
        //该事件在ios10后才会触发
        JPushModule.addReceiveOpenNotificationListener((map)=>{
            console.log("jpush 点击推送事件: ",map);
        });

        JPushModule.addReceiveNotificationListener((event)=>{
            console.log("jpush 接收推送事件: " + JSON.stringify(event));
        });
        JPushModule.addReceiveCustomMsgListener((message) => {
            console.log('jpush 接收到自定义消息',message);
        });
        JPushModule.getRegistrationID((id) => {
            console.log('jpush getRegistrationID',id);
        });
    }


    componentWillUnMount() {
        super.componentWillUnmount();
        console.log("componentWillUnMount");
        netWorkTool.removeEventListener(netWorkTool.TAG_NETWORK_CHANGE, ()=>{
            console.log("卸载网络监听");
        });
    }



    render() {
        return (
            <Provider store={store}>
              <AppWithNavigationState />
            </Provider>
        );
    }
}
