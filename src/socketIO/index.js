/**
 * Created by crcb on 2017/12/9.
 */
import {AppState} from 'react-native'
import io from 'socket.io-client';
import Param from '../constant/HttpParam';
import Config from '../constant/globalParam'
import {store} from '../redux/store/index';
import Error from '../util/ErrorUtil'

import * as socketFun from '../redux/action/socketAction';
import * as userFun from '../redux/action/userAction';
import * as chatFun from '../redux/action/chatAction'
import * as addressAction from '../redux/action/addressBookAction'
import * as sesssionActions from '../redux/action/sessionsAction';
import mock from '../assets/mock/group.json';
import * as Type from '../constant/Types';

import netWorkTool from '../util/netWorkTool'
import {Toast} from 'antd-mobile/lib'
var heartInterval;
var reconnectionInterval;
var reconnectionAttempts = 10;//重连次数
var reconnectionTimes = 0;//已经重连次数
var reconnectionDelay = 5000;//重连延时
var enableReconnection = true;//允许重连标志

const socket = global.socket = io.connect(Param.serverSocketUrl, {
    transports: ["websocket"],
    autoConnect: false,
    reconnection: false,
    reconnectionDelay:200,
    reconnectionAttempts:10
});

socket.on("connect", ()=> {
    console.log("socketIO连接成功");
    // Toast.success("socketIO连接成功")
});

socket.on("account_valid_success", (...msg)=> {
    global.socketConnectionState = true;
    connectStore(sesssionActions.updateTitle('云燕客服'));
    if(reconnectionInterval){
        console.log("socket 重连成功，停止重连，重置连接次数");
        clearInterval(reconnectionInterval);
        reconnectionTimes = 0;
    }
    Toast.hide();
    // Toast.success("服务器连接成功");
    console.log("服务器连接成功", ...msg);
    //保存用户信息
    connectStore(userFun.fetchUserInfo(msg[0]));
    connectStore(socketFun.onConnect());
    socket.emit('client_sessions_unread',{},function(sessions){
        // console.log('client_sessions_unread',sessions);
        connectStore(sesssionActions.fetchSessions(sessions));
    });

    //如果存在循环，则删除
    if(heartInterval){
        clearInterval(heartInterval);
    }
    //socket连接成功发送心跳包
    heartInterval = setInterval(()=>{
        socketOnlineBeat();
    },Config.HEART_BEAT_TIME);

    //跳转页面
    // connectStore(socketFun.navPage('HomePage'));
    if(global.loginType == '1'){//如果是token登录
        connectStore(socketFun.navPage('GesturePasswordLogin'));
    }else if(global.loginType == '2'){
        connectStore(socketFun.navPage('BindDevice'));
    }
    global.loginType = '3';//由于重连机制，需要走该方法，所以将loginType设为3
});

socket.on("server_source_unknow", (msg)=> {
    console.log("账号来源非法");
    Error("账号来源非法")
});

socket.on("server_account_error", (msg)=> {
    console.log("账号验证失败")
    Error("账号验证失败")
});


socket.on("event", (data)=> {
    console.log("收到消息 = ", data);
});

socket.on("disconnect", (reason)=> {
    console.log("服务器或客户端断开了连接 = ", reason);
    global.socketConnectionState = false;
    connectStore(sesssionActions.updateTitle('未连接'));
    doReconnect();
});

socket.on("error", (error)=> {
    console.log("socketIO出错");
    Error("socketIO出错")
});

socket.on("connect_error", (data)=> {
    console.log("遇到一个连接错误",global.loginType);
    Error("遇到一个连接错误")
});

socket.on("connect_timeout", (data)=> {
    console.log("连接超时");
    Error("连接超时")
});

socket.close = ()=> {
    console.log("连接断开");
    Error("连接断开")
};

//监听系统消息
socket.on("server_msg_system", (data)=> {
    console.log("监听到一条系统消息", data);
    connectStore(chatFun.server_msg(data));
});
//监听普通消息
socket.on("server_msg_normal", (data)=> {
    console.log("监听到发一条普通消息", data);
    connectStore(chatFun.server_msg(data));
});
//监听语音消息
socket.on("server_msg_sound", (data)=> {
    console.log("监听到一条语音消息", data);
    connectStore(chatFun.server_msg(data));
});
//监听图片消息
socket.on("server_msg_picture", (data)=> {
    console.log("监听到一条图片消息", data);
    connectStore(chatFun.server_msg(data));
});
//监听推荐消息
socket.on("server_msg_recommend", (data)=> {
    console.log("监听到一条推荐消息", data);
    connectStore(chatFun.server_msg(data));
});

//监听联系人变更推送
socket.on("server_evt_cus_ver_change", (data)=> {
    console.log("监听到一条联系人变更推送消息", data);
    connectStore(addressAction.addressVersionChange(data))
});


/**
 * 断开连接
 */
export function closeConnection(){
    //断开连接标识
    connectStore(socketFun.disConnent());
    socket.disconnect();
}

function errornav() {
    connectStore(socketFun.navPage('BindDevice'));
}

/**
 * 连接socket
 * @param userName
 * @param password
 */
export function socketConnect(userName, password) {
    // console.log("手动连接socketIO");

    if (userName && password) {
        socket.io.opts.query = {
            "username": userName,
            "password": password,
            "source": "cusmanager_local",
            "secret": "RSA",
            "device": Param.deviceType,
        }
        socket.open();
    } else {
        callback("账号有误");
    }
}

/**
 * 连接socket
 * @param userName
 * @param password
 */
export function socketConnectToken(tmpToken) {
    // console.log("手动连接socketIO");
    if (tmpToken) {
        socket.io.opts.query = {
            "token": tmpToken,
            "source": "token",
            "secret": "RSA",
            "device": Param.deviceType,
        }
        socket.open();
    } else {
        callback("token有误");
    }
}

/**
 * 心跳
 * @param tmpToken
 */
export function socketOnlineBeat() {
    console.log("发送心跳包");
    socket.emit('client_online_beat',{device:Param.deviceType},(data)=>{
        console.log("heart beat",data);
    })
}

/**
 * socket.emit
 * @param event
 * @param obj
 * @param callback
 */
export function socketEmit(event, obj, callback) {
    socket.emit(event, obj, function (data) {
        callback(data);
    });
}

/**
 * 绑定数据到store
 * @param func
 */
function connectStore(func) {
    // console.log("绑定数据到store")
    store.dispatch(func)
}
/**
 * 重连动作
 */
function reconnectAction(){
    userFun.createSingleToken(global.loginToken,(tmpToken)=>{
        socketConnectToken(tmpToken);
    },()=>{
        console.log("临时聊天token获取失败");
    });
}

/**
 * 重连
 */
export function doReconnect (){
    if(reconnectionInterval){
        clearInterval(reconnectionInterval);
    }
    reconnectionTimes = 0;
    if(store.getState().socketReducer.connectFlag) {
        // Toast.loading("正在重连",0);
        connectStore(sesssionActions.updateTitle('正在重连...'));
        reconnectionInterval = setInterval(()=> {
            checkReconnectEnable((flag)=>{
                enableReconnection = flag;
                console.log("socket 连接断开，开始重连,是否允许重连表示",flag);
                if(enableReconnection){//先判断是否需要重连
                    if (reconnectionTimes <= reconnectionAttempts) {
                        console.log("socket 尝试重连，当前重连次数 == ",reconnectionTimes);
                        reconnectionTimes++;
                        reconnectAction();
                    } else {
                        Toast.hide();
                        console.log("socket 重连已经达到最大次数，停止重连，重置连接次数");
                        clearInterval(reconnectionInterval);
                        reconnectionTimes = 0;
                        connectStore(sesssionActions.updateTitle('未连接'));
                    }
                }else{
                    // Toast.info(netWorkTool.NOT_NETWORK);
                }
            });
        }, reconnectionDelay);
    }
}

function checkReconnectEnable(callback){
    //判断网络状态
    netWorkTool.checkNetworkState((isConnected)=>{
        if(!isConnected){
            callback(false);
        }else{
            //判断是否在前台
            if(AppState.currentState == 'active'){
                callback(true);
            }else{
                callback(false);
            }
        }
    })
}