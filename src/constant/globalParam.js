/**
 * Created by crcb on 2017/11/22.
 */
/**
 * 通用全局参数
 */
export default {
    isRelease:true,//是否为发布版本
    NavBarHeight: 60,          //导航栏高度

    defaultPage: "session",    //默认的页面

    chatRefreshNum:10,
    TOAST_LENGTH:2,//消息提示时间
    DEBUG:false,
    LAUNCH_SPLASH_TIME:1500,//启动页显示时间
    HEART_BEAT_TIME:30000,//心跳包间隔时间
}
global.loginToken = '';
global.loginType;
global.socketConnectionState = false;