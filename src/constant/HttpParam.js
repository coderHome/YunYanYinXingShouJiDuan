/**
 * 通讯通用类
 */

import {Platform, Dimensions} from 'react-native'
import Device from 'react-native-device-info'

export default {
    // serverSocketUrl: "http://localyanchat1617.csebank.com:9092",  //通信地址(socket)
    // serverUrl: "http://localyanchat1620.csebank.com",  //通信地址(http)  wk
    //
    serverSocketUrl: "http://testyanchat.csebank.com:9092",  //通信地址(socket)
    serverUrl: "http://testyanchat.csebank.com",  //通信地址(http)  wk

    serverPicUrl: "http://testyanchat.csebank.com",  //通信地址(http)测试
    // projectName: "yan-server-business",  //项目名
    projectName: "yan",  //项目名
    downloadBase:"http://testyanChat.csebank.com/yan/file/download/{id}",       //图片路径（原图）
    downloadImageThumb:"http://testyanChat.csebank.com/yan/file/download/{id}?thumb=100",       //图片路径（小）
    downloadImageThumbBig:"http://testyanchat.csebank.com/yan/file/download/{id}?thumb=400",    //图片路径（大）
    uploadUrl:"http://170.101.103.157:8080/file/upload.do",             //文件上传地址

    shareLinkUrl:'http://testyanchat.csebank.com/yan/w/',

    aes_key:"12345678abcdefgh",     //AES加密秘钥，必须16个字符
    rsa_pub_key: '-----BEGIN PUBLIC KEY-----' + 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA7ofGP4lBQYvtNt+suYqP' +
    'qbK9P1h0y3rCS9w4ULknh0pERhOGHaLh/LPtx/r6Ys9eWt8n5YMRsuCG7X7XrRPp' +
    'bZrAf5WDeKYNmbgS/frT9UFhY/zvdY3hW7KGA+PFq2gRlu9Cxuc2YAqDEPekEGOv' +
    'AmYt089kHAFCPwBjjjzloVOtjRPRtxBR1NQlduUYLL+HYsdEww6xZta5SQtgOZBd' +
    '/leBIAmFfe85QLcrhigZ6aQhR5LeczGMSWzz3lFjh+Di8J5oUUdEifHjUWTv0xR5' +
    '1CfUSAsF9Ru6pvMGqWjAi6HaudGx1g8CP/Llzu8docq+z2f93625cglsSL+ez3Mp' +
    'jwIDAQAB' +
    '-----END PUBLIC KEY-----',         //RSA加密密钥

    // http 部分请求参数
    method: "POST",
    headers: {'content-type': 'application/x-www-form-urlencoded'},
    picHeaders: {'content-type': 'multipart/form-data'},
    deviceType: Platform.OS,
    deviceVersion: Device.getSystemVersion(),
    deviceId: Device.getUniqueID(),
    secretType: "RSA",   //加密方式

    timeout: 6000,  //请求超时 单位毫秒

    fileKey:"asdfghjkl",        //文件上传的key

    wechatAppKey:'wx2950a5bf75ca82a3',


}