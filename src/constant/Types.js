/**
 * Created by crcb on 2017/11/30.
 * reducers type集合
 */

export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';//用户登录成功
export const LOGIN_FAIL = 'LOGIN_FAIL';//用户登录失败
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';//用户退出登录

//会话列表信息
export const FETCH_SESSIONS_DATA_REQUEST = 'FETCH_SESSIONS_DATA_REQUEST';   //请求获取会话列表
export const FETCH_SESSIONS_DATA_SUCCESS = 'FETCH_SESSIONS_DATA_SUCCESS';   //获取会话列表成功
export const FETCH_SESSIONS_DATA_FAILURE = 'FETCH_SESSIONS_DATA_FAILURE';   //获取会话列表失败
export const UPDATE_SESSIONS_DATA = 'UPDATE_SESSIONS_DATA';     //更新会话列表

//群组成员信息
export const FETCH_MEMBERS_DATA_REQUEST = 'FETCH_MEMBERS_DATA_REQUEST';   //请求获取会话列表
// export const FETCH_MEMBERS_DATA_SUCCESS = 'FETCH_MEMBERS_DATA_SUCCESS';   //获取会话列表成功
export const FETCH_MEMBERS_DATA_FAILURE = 'FETCH_MEMBERS_DATA_FAILURE';   //获取会话列表失败
export const UPDATE_MEMBERS_DATA = 'UPDATE_MEMBERS_DATA';     //更新会话列表
export const UPDATE_SESSION_TITLE = 'UPDATE_SESSION_TITLE';     //更新会话标题

//通讯录信息
export const FETCH_ADDRESSBOOK_DATA_SUCCESS = 'FETCH_ADDRESSBOOK_DATA_SUCCESS';     //更新会话列表
export const FETCH_GROUP_DATA_SUCCESS = 'FETCH_GROUP_DATA_SUCCESS';     //更新会话列表
export const CREATE_GROUP_DATA_SUCCESS = 'CREATE_GROUP_DATA_SUCCESS';     //更新会话列表
export const DELETE_GROUP_DATA_SUCCESS = 'DELETE_GROUP_DATA_SUCCESS';     //更新会话列表
export const FETCH_ADDRESSBOOK_BY_GROUP = 'FETCH_ADDRESSBOOK_BY_GROUP';     //更新会话列表
export const FETCH_SORT_MEMBERS_DATA = 'FETCH_SORT_MEMBERS_DATA';     //更新会话列表
export const FETCH_CUSTOMER_INFO_DATA = 'FETCH_CUSTOMER_INFO_DATA';     //更新会话列表
export const REFRESH_ORIGIN_MEMBER = 'REFRESH_ORIGIN_MEMBER';     //更新会话列表
export const FETCH_TAG_LIST = 'FETCH_TAG_LIST';     //更新会话列表
export const FETCH_ADDRESSBOOK_BY_TAG = 'FETCH_ADDRESSBOOK_BY_TAG';     //更新会话列表
export const UPDATE_CUSTOMER_INFO = 'UPDATE_CUSTOMER_INFO';     //更新会话列表

//营销信息
export const FETCH_MANAGER_SUCCESS = 'FETCH_MANAGER_SUCCESS';     //
export const FETCH_MANAGER_FAIL = 'FETCH_MANAGER_FAIL';     //
export const FETCH_MARKETINGS_LIST_REQUEST = 'FETCH_MARKETINGS_LIST_REQUEST';     //

//聊天数据
export const GET_CHAT_LIST_SUCCESS = 'GET_CHAT_LIST_SUCCESS';                       //获取聊天记录
export const GET_CHAT_REFRESH_LIST_SUCCESS = 'GET_CHAT_REFRESH_LIST_SUCCESS';       //获取刷新记录

// 当前会话id
export const CHANGE_GROUPID_DATA = 'CHANGE_GROUPID_DATA';

// 当前会话底部是否弹起
export const CHANGE_BOTHIDE_FLAG = 'CHANGE_BOTHIDE_FLAG';

// 用户信息
export const FETCH_USERINFO_DATA_SUCCESS = 'FETCH_USERINFO_DATA_SUCCESS';
export const SET_SESSIONID_SUCCESS = 'SET_SESSIONID_SUCCESS';

//群组groupId
export const RESET_GROUPID = 'RESET_GROUPID'; //重置groupId


//监听消息
export const SERVER_MSG = "SERVER_MSG";

export const DISCONNECT = "DISCONNECT";
export const CONNECT_SUCCESS = "CONNECT_SUCCESS";
export const GETUSERINFO = "GETUSERINFO";


/**
 * PubSub类型集合
 */
export const P_LOGIN_SUCCESS = 'P_LOGIN_SUCCESS';       //登录成功
export const P_NAVIGATOR = 'P_NAVIGATOR';       //页面跳转
export const SESSIONS_CHANGE = "SESSIONS_CHANGE";  //会话改变
export const P_NORMAL_MSG = 'P_NORMAL_MSG';     //监听普通消息