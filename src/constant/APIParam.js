/**
 * Created by crcb on 2017/12/5.
 * 接口地址
 */
export default {
    loginApi:"api/manager/pwdlogin.do",        //登录接口
    loginToken:"api/manager/tokenlogin.do",        //token登录接口
    testApi:"api/test.do",                  //测试接口
    mbApi:"api/addressbook/getCustomerById.do",                  //测试接口
    getSMSApi:"api/sendmsg.do",                  //获取验证码
    checkMsgCode:"api/manager/checkmsgcode.do",                  //短信校验
    getCustomers:"api/addressbook/getCustomers.do",                   //获取通讯录信息
    getCustomerById:"api/addressbook/getCustomerById.do",                   //根据id获取当前客户基本信息
    getCustomerDetailsById:"api/addressbook/getSingle.do",                   //根据id获取客户详细信息
    getCustomersById:"api/addressbook/getCustomersById.do",  //根据yanid获取当前客服多个客户基本信息
    getCustVersion:"api/addressbook/getCustVersion.do",                  //获取通讯录版本号
    uploadPic:"file/uploadSimple.do",  //文件上传接口
    uploadPic2:"file/upload.do",  //文件上传接口

    createSingleToken:"api/createSingleToken.do",  //实用logintoken换取socket用token
    logout:"api/manager/logout.do",  //实用logintoken换取socket用token
    getGroups:"api/addressbook/getGroups.do",  //获取用户分组
    createGroup:"api/addressbook/createGroup.do",  //创建分组
    deleteGroup:"api/addressbook/deleteGroup.do",  //删除分组
    joinGroup:"api/addressbook/joinGroup.do",  //用户加入分组
    leaveGroup:"api/addressbook/leaveGroup.do",  //用户移出分组
    setRemark:"api/addressbook/setRemark.do",  //修改备注
    getMarketings:"api/manager/getMarketings.do",  //获取营销信息

    getCLWYPrdInfo:"api/prdFinancepro/detail.do",      //获取常乐稳盈产品数据
    getDECDPrdInfo:"api/depositPlus/findProduceById.do",      //通过ID获取大额存单产品数据
}