/**
 * Created by crcb on 2017/11/22.
 */
/**
 * 通用样式参数
 */
import {Dimensions,Platform} from 'react-native'
import px2dp from '../util/px2dp';
export default {
    navigatorBgColor:"#483627",
    bgColor: this.white,   //默认背景色调
    btnColor: "#a67c52", //按钮颜色
    labelColor: "#dcd9e0",  //标签颜色
    orangeRed: '#ff4500',
    orange: '#ff8500',
    yellow: '#ffd500',
    yellowGreen: '#9acd32',
    lightGreen: '#70dc70',
    limeGreen: '#32cd32',
    seaGreen: '#20b2aa',
    skyBlue: '#87ceeb',
    lightBlue: '#1ec8ff',
    dodgerBlue: '#1e90ff',
    helpBlue:'#09B6F2',
    thistle: '#d8bfd8',
    purple: '#7e83e3',
    slateBlue: '#6a5acd',
    black: '#000',
    white: '#fff',
    lightGray:'#E6E6E6',
    lineGray:'#666666',
    arrowColor:'#ccc',
    normalGray:'#6D6D6D',
    fontBlack:'#2A2A2A',
    screenHeight: Dimensions.get('window').height,
    screenWidth: Dimensions.get('window').width,
    mainMargin:10,
    btnMargin:40,
    borderRadius:5,
    //填充父级全部
    _flex1:{
        flex:1,
        display:"flex",
    },
    //横向布局
    _flexrow:{
        display:"flex",
        flexDirection:"row"
    },
    //纵向布局
    _flexColumn:{
        display:"flex",
        flexDirection:"column"
    },
    //中部居中
    _center:{
        justifyContent:"center",
        alignItems:"center",
    },
    //横向靠右居中
    _rowright:{
        justifyContent:"flex-end",
        alignItems:"center",
    },
    //横向靠左居中
    _rowleft:{
        justifyContent:"flex-start",
        alignItems:"center",
    },
    //横向靠上居中
    _rowtop:{
        justifyContent:"center",
        alignItems:"flex-start",
    },
    //横向靠下居中
    _rowbottom:{
        justifyContent:"center",
        alignItems:"flex-end",
    },
    //纵向靠右居中
    _columnright:{
        justifyContent:"flex-end",
        alignItems:"center",
    },
    //纵向靠左居中
    _columnleft:{
        justifyContent:"flex-start",
        alignItems:"center",
    },
    //纵向靠上居中
    _columntop:{
        justifyContent:"center",
        alignItems:"flex-start",
    },
    //纵向靠下居中
    _columnbottom:{
        justifyContent:"center",
        alignItems:"flex-end",
    },
    _globalContainer:{
        flex: 1,
    },


    //正常输入框样式
    _normalInputStyle:{
        height:px2dp(36),
        backgroundColor:'white',
        borderRadius:3,
        paddingVertical: (Platform.OS === 'ios') ? 7 : 0,
        paddingHorizontal: 7,
        marginBottom:0,
        borderWidth:0,
        fontSize:16,
    },
    _normalBtnStyle:{
        marginLeft:10,
        marginRight:10,
    },
    _normalBtnTextStyle:{
        color:'white',
    }
}
