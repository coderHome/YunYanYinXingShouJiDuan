/**
 * Created by crcb on 2017/11/29.
 */
import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { addNavigationHelpers, StackNavigator } from 'react-navigation'


import Login from "../pages/login/index"
import Chat from '../pages/chat/index'
import Sessions from '../pages/sessions/index'
import HomePage from '../pages/Home/HomePage'
import userDetail from '../pages/chat/userDetail'
import Address from '../pages/address/index'
import GesturePasswordLogin from '../pages/login/GesturePasswordLogin'
import GesturePasswordSet from '../pages/login/GesturePasswordSet'
import BindDevice from '../pages/login/bindDevice'
import ContacterPick from '../pages/address/contacterPick'
import ContacterInfoPage from '../pages/address/contacterInfoPage'
import GroupListPage from '../pages/address/groupListPage'
import GroupContacterListPage from '../pages/address/groupContacterListPage'
import LoginPage from '../pages/login/loginPage'
import TagListPage from '../pages/address/tagListPage'
import AddTagPage from '../pages/address/addTagPage'
import TagContacterListPage from '../pages/address/tagContacterListPage'
import SetRemarkPage from '../pages/address/setRemarkPage'
import QRCodePage from '../pages/mine/qrCodePage'
import YingXiao2 from '../pages/chat/yingxiao'

export const AppNavigator = StackNavigator({
    Login:{
        screen:Login,
    },
    Chat:{
        screen:Chat,
    },
    HomePage:{
        screen:HomePage,
        navigationOptions:{
            gesturesEnabled:false
        }
    },
    UserDetail:{
        screen:userDetail,
    },
    Sessions:{
        screen:Sessions,
    },
    Address:{
        screen:Address,
    },
    GesturePasswordLogin:{
        screen:GesturePasswordLogin,
    },
    GesturePasswordSet:{
        screen:GesturePasswordSet,
    },
    BindDevice:{
        screen:BindDevice,
    },
    ContacterPick:{
        screen:ContacterPick,
    },
    ContacterInfoPage:{
        screen:ContacterInfoPage,
    },
    GroupListPage:{
        screen:GroupListPage,
    },
    GroupContacterListPage:{
        screen:GroupContacterListPage,
    },
    LoginPage:{
        screen:LoginPage,
    },
    TagListPage:{
        screen:TagListPage,
    },
    AddTagPage:{
        screen:AddTagPage,
    },
    TagContacterListPage:{
        screen:TagContacterListPage,
    },
    SetRemarkPage:{
        screen:SetRemarkPage,
    },
    QRCodePage:{
        screen:QRCodePage,
    },
    YingXiao2:{
        screen:YingXiao2
    }
},{
    navigationOptions:{
        gesturesEnabled:true,
        header:null,
    }
})

const AppWithNavigationState = ({ dispatch, nav }) => (
    <AppNavigator navigation={addNavigationHelpers({ dispatch, state: nav })} />
);

AppWithNavigationState.propTypes = {
    dispatch: PropTypes.func.isRequired,
    nav: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    nav: state.nav,
});

export default connect(mapStateToProps)(AppWithNavigationState);