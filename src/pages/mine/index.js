/**
 * Created by crcb on 2017/12/18.
 */
import React from 'react'
import {View,Text,Image, ScrollView, StyleSheet} from 'react-native'
import Nav from '../../component/NavigationBar'
import { List} from 'antd-mobile/lib'
import theme from '../../constant/styleParam'
import pstyles from '../../constant/styleParam'
import * as userActions from '../../redux/action/userAction'
import Button from 'apsl-react-native-button'
import {Toast} from "antd-mobile/lib"

const Item = List.Item;
const Brief = Item.Brief;
class mine extends React.Component{
    render() {
        let {userInfo} = this.props.userInfo;
        return (
            <View  style={pstyles._globalContainer} >
                <Nav
                    title="我的"
                />
                <ScrollView
                    automaticallyAdjustContentInsets={false}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                >
                    <List>
                        <Item
                            extra={<Image
                                source={require('../../assets/Image/addressbook/head.png')}
                                style={{ width: 70, height: 70 }}
                            />}
                            arrow="horizontal"
                        >头像</Item>
                        <Item disabled extra={this.props.userInfo.name?this.props.userInfo.name:''}  onClick={() => {}}>真实姓名</Item>
                        <Item disabled extra="客户经理" onClick={() => {}}>角色</Item>
                        <Item extra="点击查看" arrow="horizontal" onClick={() => {this._gotoQRCodePage()}}>我的二维码</Item>
                        {/*<Item extra="空间清理" arrow="horizontal" onClick={() => {*/}
                            {/*// global.storageUtil.clearAllKeyId();*/}
                        {/*}}>删除聊天记录</Item>*/}
                    </List>
                    {/*<Button type="primary" style={styles.button} onPressIn={()=>{this._logout()}}>退出登录</Button>*/}
                    <Button
                        style={styles.button}
                        textStyle={{color:'white'}}
                        onPress={() => {this._logout()}}>
                        退出登录
                    </Button>
                </ScrollView>
            </View>
        );
    }

    _logout(){
        // console.log("退出登录");
        this.props.userAction.logout(this.props.userInfo,()=>{
            this.props.navigation.dispatch({type: "Reset", url: "LoginPage"})
        });
    }

    _gotoQRCodePage(){
        this.props.navigation.dispatch({type: "Navigation", url: "QRCodePage"})
    }

}

const styles = StyleSheet.create({
    listViewStyle:{
        paddingTop:80
    },
    button:{
        marginLeft:pstyles.mainMargin,
        marginRight:pstyles.mainMargin,
        marginTop:100,
        backgroundColor:theme.btnColor,
        borderRadius:5,
        height:50,
        borderWidth:0
    }
});
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

const mapStateToProps = state => ({
    userInfo: state.userReducer.userInfo,
});
const mapDispatchToProps = dispatch => ({
    userAction: bindActionCreators(userActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(mine);