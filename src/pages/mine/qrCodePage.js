/**
 * Created by crcb on 2017/12/18.
 */
import React from 'react'
import {View, Text, Image, TextInput, ScrollView, StyleSheet} from 'react-native'
import Nav from '../../component/NavigationBar'
import pstyles from '../../constant/styleParam'
import * as userActions from '../../redux/action/userAction'
import QRCode from 'react-native-qrcode'
import * as commonUtils from '../../util/commonUtils'
import PageComponent from '../../component/backComponent'
const qrCodeUrl = 'http://testyanchat.csebank.com/file/wechat/';
class qrCodePage extends PageComponent {

    constructor(props) {
        super(props);
        this.state = {
            text: 'http://facebook.github.io/react-native/',
        }
    }

    render() {
        // let url = qrCodeUrl+this.props.userInfo.jobNo;
        let url = commonUtils.generatorSmartWechatQRUrl('','',this.props.userInfo.userno,'');
        return (
            <View style={styles.globalContainer}>
                <Nav
                    leftBtnIcon="angle-left"
                    leftBtnPress={() => this.props.navigation.dispatch({type: "Back"})}
                    title="我的二维码"
                />
                <View style={styles.mainContainer}>
                    <QRCode
                        value={url}
                        size={200}
                        bgColor='black'
                        fgColor='white'/>
                </View>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    listViewStyle: {
        paddingTop: 80
    },
    button: {
        marginLeft: pstyles.mainMargin,
        marginRight: pstyles.mainMargin,
        marginTop: 100,
    },
    input: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        margin: 10,
        borderRadius: 5,
        padding: 5,
        width:200
    },
    globalContainer: {
        flex: 1,
    },
    mainContainer:{
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    }
});
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

const mapStateToProps = state => ({
    userInfo: state.userReducer.userInfo,
});
const mapDispatchToProps = dispatch => ({
    userAction: bindActionCreators(userActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(qrCodePage);