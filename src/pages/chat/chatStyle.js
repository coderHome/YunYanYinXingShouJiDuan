/**
 * Created by crcb on 2017/12/15.
 */
import Style from '../../constant/styleParam'

export default {
    _content: {
        flex: 1,
        display: "flex",
        flexDirection: "column",

    },
    _top: {
        backgroundColor: "white"
    },
    _center: {
        backgroundColor: "#f5f5f5",
        display: "flex",
        flexDirection: "column",
        justifyContent: "flex-end",
    },
    _bottom: {
        backgroundColor: "#f5f5f7",
        display: "flex",
        flexDirection: "row",
        maxHeight: 100,
        borderTopWidth: 1,
        borderTopColor: "#e7e7e8",
        borderBottomWidth: 1,
        borderBottomColor: "#e7e7e8",
    },
    _tubiao: {
        width: 40,
    },
    _tbImg: {
        width: 32,
        height: 32,
    },
    _inputTxt: {
        flex: 1
    },
    _TextInputV: {
        margin: 5,
        backgroundColor: "white",
        fontSize: 20,
        padding: 5,
        paddingLeft: 10,
        paddingRight: 10,
        borderWidth: 1,
        borderColor: "#e7e7e8",
        borderRadius: 5,
    },
    bqView: {
        flexWrap: "wrap",
        paddingTop: 10,
        paddingLeft: 20,
        paddingRight: 20,
    },
    bqImgView: {
        height: 40,
    },
    moreView: {
        flexWrap: "wrap",
        paddingTop: 10,
        paddingLeft: 30,
        paddingRight: 30,
        paddingBottom: 10,
    },
    _mView: {
        // width: 58,
        height: 70,
        marginLeft:10,
        marginRight:10,
        marginTop:10,
    },
    _moreImage:{
        borderWidth:1,
        flex:3,
        borderColor:"#d8d8d8",
        borderRadius:10,
        backgroundColor:"white"
    },
    _moreTxt:{
        color:"#6e7378",
        // paddingTop:5,
        fontSize:12,
    },
    _positionView:{
        backgroundColor:"red",
        // backgroundColor:"rgba(0,0,0,0.8)",
        width:200,
        height:200,
        // position:"absolute",
        // left:10,
        // top:100,
    },
    _yybtn:{
        width:120,
        height:120,
        borderRadius:120,
    },
    _yyTxtStyle:{
        padding:20,
        fontSize:20,
    },
    lajiView:{
        width:64,
        height:64,
        borderRadius:64,
    },
    bottomMenu: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        borderTopWidth: 1,
        borderColor: '#eaeaea',
        backgroundColor: 'white'
    },
    _bqView:{
        borderTopWidth:1,
        borderTopColor:"white",

    }
}