/**
 * Created by crcb on 2017/12/14.
 */
import React from 'react'
import {
    View,
    Text,
    FlatList,
    Image,
    TextInput,
    TouchableOpacity,
    ScrollView,
    KeyboardAvoidingView,
    Keyboard,
    Platform,
    BackHandler,
    PanResponder,
    PermissionsAndroid, Alert
} from 'react-native'
import Nav from '../../component/NavigationBar'
import {Carousel} from 'antd-mobile/lib'
import pstyles from '../../constant/styleParam'
import styles from './chatStyle'
import News from '../../component/chat/news'
import * as chatActions from '../../redux/action/chatAction'
import BqlistJson from './chatMoreData'
import ImagePickers from 'react-native-image-crop-picker'
import {Toast} from 'antd-mobile/lib'
import {AudioRecorder, AudioUtils} from 'react-native-audio'
import RNFetchBlob from "react-native-fetch-blob";
import Linking from '../../util/Linking'
import InputScrollView from 'react-native-input-scroll-view';

const _top = 120;             //顶部默认高度
const _bottomH = 50;          //底部默认高度
const _bottomH2 = 200;        //底部增加高度(不算默认的底部高度)
const _bottomH3 = Platform.OS==="ios"?100:0;        //底部导航栏和底部的距离（有键盘的时候）


const _bqWidth = (pstyles.screenWidth - 50) / 8; //表情View的宽度
const _bqViewH = 160;         //表情View的高度
const _bqFont = 15;           //表情文字的大小

const _moreViewWidth = (pstyles.screenWidth - 140) / 4; //更多栏单个View的宽度

const bqlist = BqlistJson.bqRequireUrl;   //表情图片路径
const morelist = BqlistJson.moreRequireUrl; //更多图片路径
const cancelBtnName = "取消发送";
const sendBtnName = "松开发送";
const yuyinBtnName = "按住说话";


import BackPage from '../../component/backComponent'

class chatIndex extends BackPage {

    constructor(props) {
        super(props);
        this.state = {
            topH: _top,                 //头部高度
            bottomH: _bottomH,          //底部高度
            bottomType: 'keyboard',              //底部更多栏类型 yy
            keyHeight: 0,               //键盘高度
            inputTxt: "",              //文本框输入内容
            focusFlag: false,            //是否获取焦点
            refershFlag: false,          //下拉刷新标志
            groupId: this.props.navigation.state.params.groupId,     //groupID
            yyTxt: yuyinBtnName,     //图片状态
            _second: 0,              //录音时间
            currentTime: 0.0,                                                   //开始录音到现在的持续时间
            recording: false,                                                   //是否正在录音
            stoppedRecording: false,                                            //是否停止了录音
            finished: false,                                                    //是否完成录音
            audioPath: AudioUtils.DocumentDirectoryPath + '/test.aac',          //路径下的文件名
            hasPermission: false,                                               //是否获取权限
        }
    }

    componentWillMount() {
        this._panResponder = PanResponder.create({
            onStartShouldSetPanResponder: (evt, gestureState) => true,         //是否申请成为事件响应者
            onStartShouldSetPanResponderCapture: (evt, gestureState) => true,  //父组件劫持子组件
            onMoveShouldSetPanResponder: (evt, gestureState) => true,          //是否申请成为事件响应者
            onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,   //父组件劫持子组件
            onPanResponderGrant: (evt, gestureState) => {
                // console.log("开始录音");
                let that = this;
                that.setState({yyTxt: sendBtnName});
                //录音
                that.record();

                let s = 0;
                this._timer = setInterval(function () {
                    that.setState({
                        _second: ++s
                    })
                }, 1000);
            },
            onPanResponderMove: (evt) => {
                let _event = evt.nativeEvent;
                if (Math.abs(pstyles.screenWidth - _event.pageX) < 64) {
                    if (this.state.yyTxt !== cancelBtnName) {
                        this.setState({yyTxt: cancelBtnName})
                    }
                } else {
                    if (this.state.yyTxt !== sendBtnName) {
                        this.setState({yyTxt: sendBtnName})
                    }
                }
            },
            onPanResponderRelease: (evt, gestureState) => {
                this.stop();
            },
            onPanResponderTerminate: (evt, gestureState) => {
                this.stop();
            },

        });
        if(!this.props.originMember){

        }
        //获取聊天记录
        this.props.chatAction.getChatRecords(this.state.groupId, 10);
        //获取群组信息
        this.props.chatAction.getGroupInfo(this.state.groupId);
    }

    componentDidMount() {
        let that = this;
        this.checkPermission().then((hasPermission) => {
            this.setState({hasPermission});
            //如果未授权, 则执行下面的代码
            if (!hasPermission) return;
            this.prepareRecordingPath(this.state.audioPath);
            AudioRecorder.onProgress = (data) => {
                this.setState({currentTime: Math.floor(data.currentTime)});
            };
            AudioRecorder.onFinished = (data) => {
                if (Platform.OS === 'ios') {
                    this.finishRecording(data.status === "OK", data.audioFileURL);
                }

            };
        });
    }

    //卸载页面
    componentWillUnmount() {
        this.props.chatAction.resetGroupId(this.state.groupId);
    }

    prepareRecordingPath(audioPath) {
        AudioRecorder.prepareRecordingAtPath(audioPath, {
            SampleRate: 22050,
            Channels: 1,
            AudioQuality: "Low",            //录音质量
            AudioEncoding: "aac",           //录音格式
            AudioEncodingBitRate: 32000     //比特率
        });
    }

    // 检测录音权限
    checkPermission() {
        if (Platform.OS !== 'android') {
            return Promise.resolve(true);
        }
        return PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.RECORD_AUDIO, {
            'title': '获取录音权限',
            'message': '正请求获取麦克风权限用于录音,是否准许'
        }).then((result) => {
            return (result === true || PermissionsAndroid.RESULTS.GRANTED);
        })
    }

    // 录音
    async record() {
        // 如果正在录音
        if (this.state.recording) {
            return Alert.alert('正在录音中!');
        }

        //如果没有获取权限
        if (!this.state.hasPermission) {
            return Alert.alert('没有获取录音权限!');
        }

        //如果暂停获取停止了录音
        if (this.state.stoppedRecording) {
            this.prepareRecordingPath(this.state.audioPath);
        }

        this.setState({recording: true});

        try {
            const filePath = await AudioRecorder.startRecording();
        } catch (error) {
            // console.log(error);
        }
    }

    // 停止录音
    async stop() {
        clearInterval(this._timer);
        // 没有录音, 无需停止!
        if (!this.state.recording) {
            return;
        }
        this.setState({
            stoppedRecording: true,
            recording: false
        });
        try {
            const filePath = await AudioRecorder.stopRecording();
            if (Platform.OS === 'android') {
                this.finishRecording(true, filePath);
            }
            return filePath;
        } catch (err) {
            // console.log(err);
        }
    }

    finishRecording(didSucceed, filePath) {
        let that = this;
        if (didSucceed && that.state.yyTxt === sendBtnName) {
            this.props.chatAction.sendyuyinMsg(filePath, that.state._second, that.state.groupId);
        }
        that.setState({
            finished: didSucceed, yyTxt: yuyinBtnName, _second: 0
        });
    }

    componentWillReceiveProps(nextProps) {
        // console.log("组件更新了", nextProps);
        !this.state.refershFlag ? this._setTimeOut(500) : null;
    }

    render() {
        const {members, originMember,otherUserInfo} = this.props;
        // console.log("otherUserInfo = ",otherUserInfo);
        let groupName = otherUserInfo ? otherUserInfo.name : "正在加载";
        return (
            this._content(groupName)
        )
    }

    //主要内容
    _content(groupName) {
        const {focusFlag, bottomType} = this.state;
        const {userInfo, imgs, members, sessionId,otherUserInfo} = this.props;
        let chatlist = this.props.chatRecords;
        return <View style={[pstyles._flex1, {backgroundColor: "#f5f5f7"}]}>
            <Nav
                title={groupName}
                leftBtnIcon="angle-left"
                leftBtnPress={() => this.props.navigation.dispatch({type: "Back"})}
                rightBtnIcon="phone"
                // rightBtnPress={() => this.props.navigation.dispatch({type: "Navigation", url: "UserDetail"})}
                rightBtnPress={()=>{
                    let _phone = "tel:"+otherUserInfo.phone;
                    Linking(_phone);
                }}
            />
            <View style={[styles._content]}>
                <View style={[styles._center, {flex: 1}]}>
                    {
                        chatlist ?
                            <FlatList
                                data={chatlist}
                                ref={(flatList) => this._flatList = flatList}
                                keyExtractor={(item) => item.id}
                                renderItem={({item, index}) => <News key={item.id} dataSource={item}
                                                                     userInfo={this.props.userInfo}
                                                                     imgs={imgs}
                                                                     _index={index}/>}
                                refreshing={false}
                                onRefresh={this._refsh.bind(this)}
                            />
                            : null
                    }
                </View>
                <View
                    style={{height: focusFlag ? ( _bottomH3 + _bottomH) : (bottomType === "keyboard" ? _bottomH : _bottomH2 + _bottomH)}}>
                    <Text></Text></View>

                {
                    this._bottomContent()
                }
            </View>
            {/*</ScrollView>*/}
        </View>
    }


    _bottomContainer(){
        const {bottomType, keyHeight, inputTxt, focusFlag} = this.state;
        return(
            <View style={[styles._bottom]}>
                <View style={[styles._tubiao, pstyles._center]}>
                    <TouchableOpacity onPress={() => this.btmType('more')} activeOpacity={0.7}>
                        {
                            bottomType === "more" ? <Image style={styles._tbImg} cache={"force-cache"}
                                                           source={require('../../assets/Image/chatImg/down.png')}/> :
                                <Image style={styles._tbImg} source={require('../../assets/Image/chatImg/add.png')}/>
                        }
                    </TouchableOpacity>
                </View>
                <View style={[styles._tubiao, pstyles._center]}>
                    <TouchableOpacity onPress={() => this.btmType('bq')} activeOpacity={0.7}>
                        {
                            bottomType === "bq" ? <Image style={styles._tbImg} cache={"force-cache"}
                                                         source={require('../../assets/Image/chatImg/down.png')}/> :
                                <Image style={styles._tbImg} source={require('../../assets/Image/chatImg/bq.png')}/>
                        }
                    </TouchableOpacity>
                </View>
                {/**/}
                {/*<View style={[styles._tubiao, pstyles._center]}>*/}
                {/*<TouchableOpacity onPress={() => this.btmType('yy')} activeOpacity={0.7}>*/}
                {/*{*/}
                {/*bottomType === "yy" ? <Image style={styles._tbImg} cache={"force-cache"}*/}
                {/*source={require('../../assets/Image/chatImg/down.png')}/> :*/}
                {/*<Image style={styles._tbImg} source={require('../../assets/Image/chatImg/yuyin.png')}/>*/}
                {/*}*/}
                {/*</TouchableOpacity>*/}
                {/*</View>*/}

                <View style={[pstyles._flex1]} contenteditable={true}>
                    <TextInput
                        multiline={true}
                        ref="input_content"
                        autoCorrect={false}
                        placeholder={""}
                        value={inputTxt}
                        autoCapitalize={"none"}
                        keyboardType={"default"}
                        clearButtonMode={"always"}
                        underlineColorAndroid={"transparent"}
                        textAlignVertical={"top"}
                        blurOnSubmit={true}
                        maxLength={200}
                        onFocus={() => {
                            this.setState({
                                focusFlag: true,
                                bottomType: "keyboard"
                            });
                            this._setTimeOut(100);
                        }}
                        onBlur={() => {
                            this.setState({
                                focusFlag: false,
                            })
                        }}
                        onChangeText={(v) => this.setState({inputTxt: v})}
                        style={styles._TextInputV}
                    />
                </View>
                <View style={[pstyles._center, {width: 70}]}>
                    <TouchableOpacity onPress={() => this._submit()} activeOpacity={0.7}>
                        <Text>发送</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    //底部内容
    _bottomContent() {
        const {bottomType, keyHeight, inputTxt, focusFlag} = this.state;
        return(
        Platform.OS==="ios"?
            <KeyboardAvoidingView
            behavior={"padding"}
            // contentContainerStyle={ this.props.contentContainerStyle }
            style={[styles.bottomMenu]}>
                {this._bottomContainer()}
            {
                focusFlag ? <View style={{height: _bottomH3}}><Text>{null}</Text></View> : null
            }
            {
                //底部更多栏
                bottomType === "keyboard" ? null: this._bottomView()
            }
            </KeyboardAvoidingView>
            :
            <View
                behavior={"padding"}
                // contentContainerStyle={ this.props.contentContainerStyle }
                style={[styles.bottomMenu]}>
                {this._bottomContainer()}
                {
                    focusFlag ? <View style={{height: _bottomH3}}><Text>{null}</Text></View> : null
                }
                {
                    //底部更多栏
                    bottomType === "keyboard" ? null: this._bottomView()
                }
            </View>
        )
    }

    //刷新数据
    _refsh() {
        this.props.chatAction.refreshChatRecords(this.state.groupId, this.props.chatRecords[0].id);
        this.setState({refershFlag: true})
    }

    //下面三个按钮的点击事件，延迟滚动条滚到底部，隐藏键盘
    btmType(index) {
        this.setState({bottomType: this.state.bottomType === index ? "keyboard" : index});
        Keyboard.dismiss();
        let time = 100;
        if (index === 'bq') {  //如果是表情，延迟150ms
            time = 200;
        } else if (index === 'yy') {  //如果是表情，延迟150ms
            time = 150;
        } else if (index === 'more') {   //如果是更多，延迟100ms
            time = 150
        }
        this._setTimeOut(time);
    }

    //滚动条滚到底部
    _setTimeOut(time) {
        // console.log("chatRecords = ", this.props.chatRecords)
        if (this.props.chatRecords.length > 10) {
            setTimeout(() => {
                this._flatList.scrollToEnd();
                this.setState({refershFlag: false})
            }, time)
        }
    }

    //发送
    _submit() {
        let msg = this.state.inputTxt;
        if (msg) {
            msg = msg.replace(/\s/g, '');
            // console.log("发送的内容 = ", msg);
            if (msg.length > 0) {
                this.props.chatAction.sendMsg(this.state.groupId, msg);
            }
            this._setTimeOut(20);
            this.setState({
                inputTxt: null
            })
        }

    }

    //底部更多栏
    _bottomView() {
        const {bottomType, focusFlag} = this.state;
        if (focusFlag) {
            return null
        }
        if (bottomType === 'bq') {
            return <View style={[{height: _bottomH2}]}>{this._bq()}</View>
        } else if (bottomType === 'yy') {
            return <View style={[{height: _bottomH2}]}>{this._yuyin()}</View>
        } else if (bottomType === 'more') {
            return <View style={[{height: _bottomH2}]}>{this._MoreView()}</View>
        }
    }

    //底部语音栏
    _yuyin() {
        return <View style={[pstyles._flex1, pstyles._flexrow, {width: pstyles.screenWidth}]}>
            <View style={[{width: 64}, pstyles._flexrow, pstyles._center]}>
                <Text style={{fontSize: 30}}>{this.state._second}</Text>
            </View>
            <View style={[pstyles._flex1, pstyles._flexColumn, pstyles._columnleft]}>
                <View>
                    {
                        this.state.yyTxt === yuyinBtnName ?
                            <Text style={[styles._yyTxtStyle, {color: "#7f8393"}]}>{this.state.yyTxt}</Text> :
                            <Text
                                style={[styles._yyTxtStyle, {color: this.state.yyTxt === cancelBtnName ? "red" : "green"}]}>{this.state.yyTxt}</Text>
                    }

                </View>
                <View
                    style={[styles._yybtn, pstyles._center, {backgroundColor: this.state.yyTxt !== yuyinBtnName ? "#1093D7" : "#17b5ed",}]}
                    {...this._panResponder.panHandlers}>
                    <Image source={require('../../assets/Image/chatImg/luyin.png')} cache={"force-cache"}/>
                </View>
            </View>
            <View style={[pstyles._center]}>
                {
                    this.state.yyTxt === cancelBtnName ?
                        <View style={[styles.lajiView, pstyles._center, {backgroundColor: "#666"}]}>
                            <Image source={require('../../assets/Image/chatImg/trash.png')} cache={"force-cache"}/>
                        </View> : <View style={[styles.lajiView, pstyles._center]}>
                            <Image source={require('../../assets/Image/chatImg/trash.png')} cache={"force-cache"}
                                   style={{tintColor: "#666", width: 48, height: 48}}/>
                        </View>
                }
            </View>
        </View>
    }

    //底部表情栏
    _bq() {

        return <View style={{width: pstyles.screenWidth, height: _bottomH2}}>
            <Carousel>
                {
                    bqlist.map((item, index) =>
                        <View key={index} style={[{height: _bqViewH}, styles.bqView, pstyles._flexrow]}>
                            {
                                item.map((i, j) =>
                                    <TouchableOpacity activeOpacity={0.9} onPress={() => this._clickBq(i)} key={j}
                                                      style={[styles.bqImgView, pstyles._flexrow, pstyles._center, {width: _bqWidth}]}>
                                        <Image source={i} style={{width:_bqWidth-7,height:_bqWidth-7}} cache={"force-cache"}/>
                                    </TouchableOpacity>)
                            }
                        </View>
                    )
                }
            </Carousel>
            <View style={[{height: _bottomH2 - _bqViewH}, pstyles._flexrow,styles._bqView]}>
                <View style={[pstyles._flexrow]}>
                    <View style={[pstyles._flexrow, pstyles._center, {width: 80}]}>
                        <Text style={{fontSize: _bqFont}}>表情</Text>
                    </View>
                </View>
                <View style={{flex: 1, backgroundColor: "white"}}><Text>{null}</Text></View>
            </View>
        </View>
    }

    //底部更多栏
    _MoreView() {
        return <View style={{width: pstyles.screenWidth, height: _bottomH2}}>
            <Carousel dots={morelist.length < 2 ? false : true}>
                {
                    morelist.map((item, index) => {
                        return <View style={[pstyles._flexrow, {height: _bottomH2}, styles.moreView]} key={index}>
                            {
                                item.map((i, j) =>
                                    <View style={[pstyles._flexColumn, styles._mView, {width: _moreViewWidth}]} key={j}>
                                        <TouchableOpacity activeOpacity={0.7}
                                                          style={[styles._moreImage, pstyles._center]}
                                                          onPress={() => this._MoreClickEvent(i.name)}
                                        >
                                            <Image source={i.url} cache={"force-cache"}
                                                   style={{width: 32, height: 32}}/>
                                        </TouchableOpacity>
                                        <View style={[pstyles._rowbottom, pstyles._flex1, pstyles._flexrow]}><Text
                                            style={styles._moreTxt}>{i.name}</Text></View>
                                    </View>)
                            }
                        </View>
                    })
                }
            </Carousel>
        </View>
    }

    //点击表情事件
    _clickBq(e) {
        let _txt = this.state.inputTxt;
        this.setState({
            inputTxt: _txt ? _txt + "[bq:" + e + "]" : "[bq:" + e + "]"
        })
    }

    //更多按钮的点击事件
    _MoreClickEvent(name) {
        switch (name) {
            case "拍摄":
                this._shooting();
                break;
            case "相册":
                this._photo();
                break;
            case "营销":
                this._yingxiao();
                break;
            default:
                this._photoTest();
                break;
        }
    }

    //拍摄
    _shooting() {
        ImagePickers.openCamera({
            compressImageQuality: 0.6,
        }).then(image => {
            this.props.chatAction.sendPic(image.path, this.state.groupId);
        });
    }

    //相册
    _photo() {
        ImagePickers.openPicker({
            cropping: false,
            // width: pstyles.screenWidth,
            // height: pstyles.screenHeight,
            width: 300,
            height: 400,
        }).then(image => {
            this.props.chatAction.sendPic(image.path, this.state.groupId);
        });
    }

    //营销
    _yingxiao(){
        // console.log("营销咯");
        this.props.navigation.dispatch({type: "Navigation",url:"YingXiao2"});
    }

    _photoTest() {
        // console.log("测试图片");
    }
}


import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import addressBookReducer from "../../redux/reducer/addressBookReducer";

const mapStateToProps = state => ({
    chatRecords: state.chatReducer.chatRecords,
    imgs: state.chatReducer.imgs,
    sessionId: state.chatReducer.sessionId,
    userLogin: state.userReducer.userLogin,
    userInfo: state.userReducer.userInfo,
    members: state.sessionsReducer.members,
    originMember: state.addressBookReducer.originMember,
    addressBookMembersByTag: state.addressBookReducer.addressBookMembersByTag,
    otherUserInfo:state.userReducer.otherUserInfo,
});
const mapDispatchToProps = dispatch => ({
    chatAction: bindActionCreators(chatActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(chatIndex);