/**
 * Created by crcb on 2017/12/19.
 */
import React from 'react'
import {View,Text} from 'react-native'
import Nav from '../../component/NavigationBar'

export default class loginSuccess extends React.Component{
    render(){
        return (
            <View>
                <Nav
                    title="XX的详细信息"
                    leftBtnIcon="angle-left"
                    leftBtnPress={()=>{
                        this.props.navigation.dispatch({type:"Back"})
                    }}
                />
                <Text>
                    详细信息
                </Text>
            </View>
        )
    }
}