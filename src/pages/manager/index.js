/**
 * Created by crcb on 2017/12/18.
 */
import React from 'react'
import {AppRegistry, View, Text, Image, ScrollView, StyleSheet, FlatList, TouchableHighlight} from 'react-native'
import Nav from '../../component/NavigationBar'
import pstyles from '../../constant/styleParam'
import Swipeout from 'react-native-swipeout';
import * as managerAction from '../../redux/action/managerAction'
import ShareUtil from '../../util/ShareUtil'
import * as commonUtils from '../../util/commonUtils'
import * as userActions from '../../redux/action/userAction'
const shareUrl = 'http://testyanchat.csebank.com/file/wechat/000003';
let resolveAssetSource = require('resolveAssetSource');
class index extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            close:true,
        }
    }
    componentWillMount() {
        this._fetchMarketingsData();
    }

    //初始化
    componentDidMount() {

    }

    render() {
        return (
            <View style={pstyles._globalContainer}>
                <Nav
                    title="营销"
                />
                <FlatList
                    renderItem={this._renderItem.bind(this)}
                    getItemLayout={(data, index) => (
                    {length: 70, offset: (70) * index, index}
                    )}
                    initialNumToRender={10}
                    data={this.props.manageActivities}
                    keyExtractor={this._keyExtractor.bind(this)}
                    onRefresh={()=>{
                        this._fetchMarketingsData()
                    }}
                    refreshing={this.props.isRefresh}
                >
                </FlatList>
            </View>
        );
    }

    _fetchMarketingsData(){
        this.props.managerActions.getManageList(()=>{
            console.log('获取营销产品成功');
        });
    }

    _renderItem(data) {
        let {item} = data;
        // console.log(item);
        return (
            <Swipeout backgroundColor="#ffffff" right={[{
                text: '分享',
                type: 'delete',
                backgroundColor: '#009245',
                onPress: () => {
                    this._share(item.prdType, item.prdId);
                },
            }]} autoClose={true}
                close={this.state.close}
                onOpen={()=>{
                }}
            >
                <TouchableHighlight underlayColor="rgba(0,0,0,0.2)" onPress={()=> {
                    this._handlerClick()
                }}>
                    <View style={styles.item}>
                        <Text numberOfLines={1} style={styles.titleText}>
                            {item.title}({item.subtitle})
                        </Text>
                    </View>
                </TouchableHighlight>
            </Swipeout>
        )
    }

    _handlerClick(){
        console.log('click');
        this.setState({
            close:true
        })
    }

    _keyExtractor(item) {
        return item.prdId;
    }

    _share(prdType,prdId) {
        let share = new ShareUtil();
        let shareUrl = commonUtils.generatorShareUrl(prdId,prdType,this.props.userInfo.userno,'');
        share.shareWeChatImage(shareUrl);
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    line: {
        flex: 1,
        height: 1,
        flexDirection: 'row',
        backgroundColor: "#eeeeee",
    },
    item: {
        flex: 1,
        height: 60,
        overflow: 'hidden',
        position: 'relative',
        paddingVertical: 5,
        paddingHorizontal: 15,
        borderBottomWidth: 1,
        borderStyle: 'solid',
        borderBottomColor: '#cccccc',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleText: {
        flex: 1,
        fontSize: 16,
    },
    colorGreen: {
        color: 'green'
    }
})
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

const mapStateToProps = state => ({
    manageActivities: state.managerReducer.manageActivities,
    isRefresh: state.managerReducer.isRefresh,
    routers: state.nav.routes,
    userInfo: state.userReducer.userInfo,
});
const mapDispatchToProps = dispatch => ({
    managerActions: bindActionCreators(managerAction, dispatch),
    userAction: bindActionCreators(userActions, dispatch),
});
export default connect(mapStateToProps, mapDispatchToProps)(index);