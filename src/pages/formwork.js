/**
 * Created by crcb on 2017/12/9.
 */

import React from 'react'
import {View,Text} from 'react-native'
import Nav from '../component/NavigationBar'

export default class loginSuccess extends React.Component{
    render(){
        return (
            <View>
                <Nav
                    title="模板"
                    leftBtnIcon="angle-left"
                    leftBtnPress={()=>{
                        this.props.navigation.dispatch({type:"Back"})
                    }}
                />
                <Text>
                  这是一张模板页面
                </Text>
            </View>
        )
    }
}