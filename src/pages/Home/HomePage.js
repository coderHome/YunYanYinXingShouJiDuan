/**
 * Created by crcb on 2017/12/15.
 */

import React from 'react'
import {View,Text,FlatList,Image,StyleSheet,AppState} from 'react-native'
import Nav from '../../component/NavigationBar'
import pstyles from '../../constant/styleParam'

import TabNav from 'react-native-tab-navigator'
import * as userActions from '../../redux/action/userAction'
import * as chatActions from '../../redux/action/chatAction'
import * as addressBookActions from '../../redux/action/addressBookAction'
import * as socketActions from '../../redux/action/socketAction'
import Session from '../sessions/index'
import Address from '../address/index'
import Mine from '../mine/index'
import Manager from '../manager/index'
import PageComponent from '../../component/backComponent'
import netWorkTool from '../../util/netWorkTool'

const tabBarSize = 13;            //底部tabBar的字体大小
const tabBarIconWidth = 30;       //底部tabBar图标的宽度
const tabBarIconHeight = 30;      //底部tabBar图标的高度
const selectedColor = "green";    //底部选中的字体/图标颜色
const defaultColor = "#A4A4A4";   //底部默认的字体颜色

class HomePage extends PageComponent{
    constructor(props){
        super(props);
        this.state = {
            hidden:false,
            fullScreen:false,
            selectedTab: 'session'
        }

    }


    componentWillMount() {
        super.componentWillMount();
        netWorkTool.addEventListener(netWorkTool.TAG_NETWORK_CHANGE,this.handleNetStateChangeMethod.bind(this));
        //监听状态改变事件
        AppState.addEventListener('change', this.handleAppStateChange.bind(this));
    }

    componentWillUnMount() {
        super.componentWillUnmount();
        netWorkTool.removeEventListener(netWorkTool.TAG_NETWORK_CHANGE, ()=>{
            console.log("卸载网络监听");
        });
        //删除状态改变事件监听
        AppState.removeEventListener('change', this.handleAppStateChange.bind(this));
    }

    componentDidMount() {
        this.props.addressBookAction.getAddressBookVersion((version)=>{
            // console.log("通讯录版本号获取成功");
            this.props.addressBookAction.getAddressBook(version,()=>{
                // console.log("通讯录获取成功");
            },()=>{
                // console.log("通讯录获取失败");
            });
        },()=>{
            // console.log("通讯录版本号获取失败");
        });
    }

    handleNetStateChangeMethod(){
        console.log("网络状态发生变化");
        this.props.socketAction.reconnect();
    }

    handleAppStateChange(appState){
        console.log("AppState发生变化");
        if(appState == 'active' && !global.socketConnectionState){
            this.props.socketAction.reconnect();
        }
    }

    _ItemPage(_page,title,tabName,icon){
        return <TabNav.Item
            selected={this.state.selectedTab === title}
            title={tabName}
            titleStyle={styles.tabText}
            selectedTitleStyle={styles.selectedTabText}
            renderIcon={() => <Image style={styles.icon} source={icon} />}
            renderSelectedIcon={() =>  <Image style={[styles.icon,{tintColor:selectedColor}]} source={icon} />}
            onPress={() => this.setState({ selectedTab: title })}>
            <_page navigation={this.props.navigation} />
        </TabNav.Item>
    }

    render(){
        // console.log("this.props",this.props.navigation);
        return(
            <View style={pstyles._flex1}>
                <TabNav tabBarStyle={styles._tabStyle} sceneStyle={styles._sceneStyle}>
                    {this._ItemPage(Session,"session","会话",require("../../assets/Image/tab/session.png"))}
                    {this._ItemPage(Address,"address","通讯录",require("../../assets/Image/tab/address.png"))}
                    {this._ItemPage(Manager,"manager","营销",require("../../assets/Image/tab/manage.png"))}
                    {this._ItemPage(Mine,"me","个人中心",require("../../assets/Image/tab/mine.png"))}
                </TabNav>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    tabText: {
        fontSize: tabBarSize,
        color: defaultColor,
    },
    selectedTabText: {
        fontSize: tabBarSize,
        color: selectedColor,
    },
    icon: {
        width: tabBarIconWidth,
        height: tabBarIconHeight,
    },
    _tabStyle:{
        overflow:"hidden",
        height:"auto",
        paddingTop:5,
    },
    _sceneStyle:{
    }
});


import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({
    userAction: bindActionCreators(userActions, dispatch),
    chatAction: bindActionCreators(chatActions, dispatch),
    addressBookAction: bindActionCreators(addressBookActions, dispatch),
    socketAction: bindActionCreators(socketActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);