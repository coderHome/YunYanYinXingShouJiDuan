/**
 * 客户详情页
 * Created by crcb on 2017/12/18.
 */
import React from 'react'
import {View,Text,Image, ScrollView, StyleSheet} from 'react-native'
import Nav from '../../component/NavigationBar'
import { List,Button,Tag,TextareaItem} from 'antd-mobile/lib'
import pstyles from '../../constant/styleParam'
import * as addressBookActions from '../../redux/action/addressBookAction'
import  {ActionSheet} from 'antd-mobile/lib'
import PageComponent from '../../component/backComponent'
const Item = List.Item;
const Brief = Item.Brief;
class setRemarkPage extends PageComponent{
    constructor(props){
        super(props);
        this.state = {
            customerInfo:this.props.navigation.state.params.customerInfo,
            val :this.props.navigation.state.params.customerInfo.remark
        }
    }


    //初始化
    componentDidMount() {

    }

    onChange = (val)=>{
        this.setState({
            val
        })
    }


    render() {
        const value = this.state.customerInfo.remark;
        return (
            <View  style={pstyles._globalContainer} >
                <Nav
                    leftBtnIcon="angle-left"
                    leftBtnPress={() => this.props.navigation.dispatch({type: "Back"})}
                    title='设置备注'
                    rightBtnText="确定"
                    rightBtnPress = {()=>{
                        this._handlerClick()
                    }}
                />
                <ScrollView
                    automaticallyAdjustContentInsets={false}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                >
                    <List renderHeader={()=>'备注'}>
                       <TextareaItem
                            rows={1}
                            value={this.state.val}
                            onChange = {this.onChange}
                       />
                    </List>
                </ScrollView>
            </View>
        );
    }
    _handlerClick(){
        this.props.addressBookAction.doSetRemark(this.state.customerInfo.yanId,this.state.val,()=>{
            console.log("修改成功") ;
            this.props.navigation.dispatch({type: "Back"});
        });
    }


}

const styles = StyleSheet.create({
    listViewStyle:{
        paddingTop:80
    },
    button:{
        marginLeft:pstyles.mainMargin,
        marginRight:pstyles.mainMargin,
        marginTop:100,
    }
});

import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

const mapStateToProps = state => ({
    customerInfo: state.addressBookReducer.customerInfo,
    groupDatas:state.addressBookReducer.groupDatas,
});
const mapDispatchToProps = dispatch => ({
    addressBookAction: bindActionCreators(addressBookActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(setRemarkPage);