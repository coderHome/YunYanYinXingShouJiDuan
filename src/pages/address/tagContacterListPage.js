/**
 * Created by crcb on 2017/12/18.
 */
import React from 'react'
import {View,Text,SectionList,TouchableHighlight,Image,CustomSeparatorComponent, StyleSheet} from 'react-native'
import Nav from '../../component/NavigationBar'
import addressJson from '../../assets/mock/addressBook.json'
import Swipeout from 'react-native-swipeout';
import AddressItem from '../../component/address/addressItem'
import SectionHeaderItem from '../../component/address/sectionHeaderItem'
import * as addressBookActions from '../../redux/action/addressBookAction';
import PropTypes from 'prop-types';
// var StyleSheet = require('react-native-debug-stylesheet');
import PageComponent from '../../component/backComponent'
var sections = addressJson.addressBook;

const ITEM_HEIGHT = 100;
const HEADER_HEIGHT = 30;
const SEPARAtOR_HEIGHT = 0;
class tagContacterListPage extends PageComponent{
    constructor(props){
        super(props);
        this.state = {
            dataSource:sections,
            name:this.props.navigation.state.params.name,
        }
    }

    //初始化
    componentDidMount() {
        this.props.addressBookActions.getMembersByTag(this.state.name);
    }
    render(){
        return (
            <View style={styles.container}>
                <Nav
                    leftBtnIcon="angle-left"
                    leftBtnPress={() => this.props.navigation.dispatch({type: "Back"})}
                    title={this.state.name}
                />
                <View style={styles.container}>
                    <SectionList
                        ref='list'
                        enableEmptySections
                        renderSectionHeader={this._sectionComp}
                        renderItem={this._renderItem}
                        sections={this.props.addressBookMembersByTag}
                        refreshing={false}
                        getItemLayout={this._getItemLayout}
                        stickySectionHeadersEnabled={true}
                        initialNumToRender={10}
                    />
                </View>
            </View>
        )
    }
    _renderItem = (item) => {
        const data = item.item;
        return(
            this.state.groupId!='0'?
            <Swipeout backgroundColor="#ffffff" right={[{
                text: '删除',
                type: 'delete',
                onPress:() => {
                    this._deleteMember(data.yanId,this.state.groupId);
                }
            }]}>
                <AddressItem
                    userInfo={data}
                    headImg={data.head}
                    text={data.name}
                    onClickItem={()=>this.navChat(data.yanId,data.name)}
                    isShowCheckBox={false}
                />
            </Swipeout>
                :
            <AddressItem
                userInfo={data}
                headImg={data.head}
                text={data.name}
                onClickItem={()=>this.navChat(data.yanId)}
                isShowCheckBox={false}
            />
        )
    }

    _sectionComp = (info) => {
        var txt = info.section.key;
        return (
            <SectionHeaderItem
                text={txt}
            />

        )
    }

    _getItemLayout(data,index){
        let[length,separator,header] = [ITEM_HEIGHT,SEPARAtOR_HEIGHT,HEADER_HEIGHT];
        return {length,offset:(length+separator)*index+header,index};
    }
    navChat(yanId,name){
        this.props.navigation.dispatch({type:"Navigation",url:"ContacterInfoPage",params:{
            yanId,name,
        }})
    }
    _deleteMember(yanId,groupId){
        this.props.addressBookActions.doLeaveGroup(yanId,groupId,()=>{
            //刷新页面
            this.props.addressBookActions.getMembersByGroupId(this.state.groupId);
        });
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    line: {
        flex: 1,
        height: 1,
        flexDirection: 'row',
        backgroundColor: "#cccccc",
    },
    item: {
        flex: 1,
        height: 60,
        overflow: 'hidden',
        position: 'relative',
        paddingVertical: 5,
        paddingHorizontal:5,
        borderBottomWidth:1,
        borderStyle:'solid',
        borderBottomColor:'#cccccc',
    },
    unread: {
        position: 'absolute',
        zIndex:10,
        top: 2,
        left: 52,
        width: 6,
        height: 6,
        borderRadius: 3,
        backgroundColor: 'red',
    },
    menu: {
        width: 50,
        height: 50,
        flexDirection: 'row',
        position: 'absolute',
        right: -50,
        top: 0,
    },
    delete: {
        flex: 1,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'red'
    },
    menuText: {
        color: '#fff',
        fontSize: 16,
    },
    msg: {
        flexDirection: 'row',
    },
    picBox: {
        width: 50,
        height: 50,
    },
    pic: {
        flex: 1,
        width: 50,
    },
    block: {
        paddingLeft:5,
        flex: 1,
        height: 50,

    },
    blockTop: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    title:{
        flex:1,
        height:20,
        flexDirection:'row',

    },
    date: {
        width: 60,
        paddingLeft:10,
    },
    blockBot: {
        flex: 1,
        paddingTop:10,
    },
    detail: {
        flexDirection: 'row',
    },
    titleText: {
        flex:1,
        fontSize: 16,

    },
    dateText: {
        fontSize: 12,
    },
    detailLeft: {
        fontSize: 12,
    },
    detailRight: {
        flex:1,
        fontSize: 12,
    },
    sectionHeadertText:{
        fontSize:15,
    },
    sectionHeaderContainer:{
        height:30,
        paddingLeft:5,
        justifyContent: 'center',
        backgroundColor:'#eee'
    }
});
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

const mapStateToProps = state => ({
    addressBookMembersByTag: state.addressBookReducer.addressBookMembersByTag,
});
const mapDispatchToProps = dispatch => ({
    addressBookActions: bindActionCreators(addressBookActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(tagContacterListPage);