/**
 * 客户详情页
 * Created by crcb on 2017/12/18.
 */
import React from 'react'
import {View, Text, Image, ScrollView, StyleSheet} from 'react-native'
import Nav from '../../component/NavigationBar'
import {List, Button, Tag} from 'antd-mobile/lib'
import pstyles from '../../constant/styleParam'
import * as addressBookActions from '../../redux/action/addressBookAction'
import  {ActionSheet} from 'antd-mobile/lib'
import PageComponent from '../../component/backComponent'
const Item = List.Item;
const Brief = Item.Brief;
class contacterInfoPage extends PageComponent {
    constructor(props) {
        super(props);
        this.state = {
            yanId: this.props.navigation.state.params.yanId,
            name: this.props.navigation.state.params.name,
            flag: this.props.navigation.state.params.flag
        }
    }


    //初始化
    componentDidMount() {
        this.props.addressBookAction.getCustomerDetailsById(this.state.yanId, ()=> {
            // this.props.addressBookAction.testAction(this.props.customerInfo);
        }, ()=> {

        });
    }


    render() {
        const {yanId, name} = this.state;
        console.log('info--------------', this.props.customerInfo);
        return (
            <View style={pstyles._globalContainer}>
                <Nav
                    leftBtnIcon="angle-left"
                    leftBtnPress={() => this.props.navigation.dispatch({type: "Back"})}
                    title={this.props.customerInfo.name}
                />
                <ScrollView
                    automaticallyAdjustContentInsets={false}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                >
                    <List>
                        <Item
                            extra={<Image
                                source={require('../../assets/Image/addressbook/head.png')}
                                style={{width: 70, height: 70}}
                            />}
                        >头像</Item>
                        <Item disabled extra="客户" onClick={() => {
                        }}>角色</Item>
                    </List>
                    <List
                        style={{marginTop: 20}}
                    >
                        <Item disabled extra={name} onClick={() => {
                        }}>真实姓名</Item>
                        <Item extra={this.props.customerInfo.remark} arrow="horizontal" onClick={() => {
                            this._goSetRemark(this.props.customerInfo)
                        }}>备注</Item>
                    </List>
                    <List
                        style={{marginTop: 20}}
                    >
                        <Item extra={this.props.customerInfo.groupName} arrow="horizontal" onClick={() => {
                            this._showGroupsActionSheet(this.props.customerInfo.groupId)
                        }}>群组</Item>
                        <Item extra={this._returnTag(this.props.customerInfo.tips)} arrow="horizontal" onClick={() => {
                        }}>标签</Item>
                    </List>
                    {
                        this.state.flag ? null :
                            <Button type="primary" style={styles.button} onPressIn={()=> {
                                this._doChat()
                            }}>
                                发送消息
                            </Button>
                    }
                </ScrollView>
            </View>
        );
    }

    _doChat() {
        this.props.addressBookAction.doChat(this.state.yanId, (data)=> {
            if (data) {
                this.props.navigation.dispatch({type: "Navigation", url: "Chat", params: {
                    groupId: data.sessionId
                }
                })
            } else {
                console.log("dochat data获取失败");
            }
        });
    }

    _returnTag(tips) {
        console.log("tips----------", tips);
        if (tips != undefined) {
            let str = '';
            tips.forEach((item, index)=> {
                if (index == 0) {
                    str = item.name;
                } else {
                    str = str + ',' + item.name;
                }
            });
            return (
                <View>
                    <Text>{str}</Text>
                </View>
            )
        }

    }

    _showGroupsActionSheet(groupId) {
        console.log('customerInfo==', groupId);
        const groupDatas = this.props.groupDatas;
        let groupNames = [];
        let groupIds = [];
        let currentIndex = 0;
        groupDatas.forEach((element, index) => {
            groupNames.push(element.name);
            groupIds.push(element.id);
            if (element.id == groupId) {
                currentIndex = index;
            }
        })
        groupNames.push("取消");
        groupIds.push('-1');
        ActionSheet.showActionSheetWithOptions({
            title: '移动分组',
            message: '请选择',
            options: groupNames,
            cancelButtonIndex: groupNames.length - 1,
            destructiveButtonIndex: currentIndex,
        }, (buttonIndex)=> {
            console.log('buttion index', buttonIndex);
            if (buttonIndex == currentIndex || buttonIndex == groupNames.length - 1) {
                console.log('点击了删除或者原分组');
            } else {
                console.log('分组请求');
                this._doMove(groupIds[buttonIndex], groupId, this.state.yanId);
            }
        });
    }

    _doMove(targetGroupId, originGroupId, yanId) {
        if (targetGroupId == '0' || targetGroupId == '-1') {
            this.props.addressBookAction.doLeaveGroup(yanId, originGroupId, ()=> {

            });
        } else {
            this.props.addressBookAction.doJoinGroup(yanId, targetGroupId);
        }
    }

    _goSetRemark(customerInfo) {
        this.props.navigation.dispatch({
            type: "Navigation", url: "SetRemarkPage", params: {
                customerInfo: customerInfo
            }
        })
    }
}

const styles = StyleSheet.create({
    listViewStyle: {
        paddingTop: 80
    },
    button: {
        marginLeft: pstyles.mainMargin,
        marginRight: pstyles.mainMargin,
        marginTop: 100,
    }
});

import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

const mapStateToProps = state => ({
    customerInfo: state.addressBookReducer.customerInfo,
    groupDatas: state.addressBookReducer.groupDatas,
});
const mapDispatchToProps = dispatch => ({
    addressBookAction: bindActionCreators(addressBookActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(contacterInfoPage);