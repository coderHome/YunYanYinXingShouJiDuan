/**
 * Created by crcb on 2017/12/18.
 */
import React from 'react'
import {View,Text,SectionList,TouchableHighlight,Image,CustomSeparatorComponent, StyleSheet} from 'react-native'
import Nav from '../../component/NavigationBar'
import addressJson from '../../assets/mock/addressBook.json'
import Swipeout from 'react-native-swipeout';
import AddressItem from '../../component/address/addressItem'
import SectionHeaderItem from '../../component/address/sectionHeaderItem'
import * as addressBookActions from '../../redux/action/addressBookAction'
import PropTypes from 'prop-types';
// var StyleSheet = require('react-native-debug-stylesheet');
var sections = addressJson.addressBook;

const ITEM_HEIGHT = 100;
const HEADER_HEIGHT = 30;
const SEPARAtOR_HEIGHT = 0;
class address extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            // dataSource:this.props.addressBookMembers,
        }
    }
    componentWillMount() {
    }

    //初始化
    componentDidMount() {
        // this.props.addressBookAction.getAddressBook(()=>{
        //     console.log("通讯录获取成功！");
        // });
        this.props.addressBookAction.getGroupInfo(()=>{
            console.log("分组获取成功");
        },()=>{
            console.log("分组获取失败");
        });


    }


    render(){
        return (
            <View style={styles.container}>
                <Nav
                    title="通讯录"
                />
                <View style={styles.container}>
                    <SectionList
                        ref='list'
                        enableEmptySections
                        renderSectionHeader={this._sectionComp}
                        renderItem={this._renderItem}
                        sections={this.props.addressBookMembers}
                        refreshing={false}
                        getItemLayout={this._getItemLayout}
                        stickySectionHeadersEnabled={true}
                        initialNumToRender={10}
                        ListHeaderComponent={this._renderHeader()}
                    />
                </View>
            </View>
        )
    }
    _renderItem = (item) => {
        const data = item.item;
        return(
            <Swipeout backgroundColor="#ffffff" right={[{
                text: '删除',
                type: 'delete',
                onPress:() => {
                    this._deleteSession(data.id);
                }
            }]}>
                <AddressItem
                    userInfo={data}
                    headImg={data.head}
                    text={data.name}
                    onClickItem={()=>this.navChat(data.yanId,data.name)}
                    isShowCheckBox={false}
                    keyExtractor={this._keyExtractor.bind(this)}
                />
            </Swipeout>
        )
    }

    _keyExtractor(item){
        return item.yanId
    }
    _sectionComp = (info) => {
        var txt = info.section.key;
        return (
            <SectionHeaderItem
                text={txt}
            />
        )
    }

    _renderHeader(){
        return(
            <View>
                <AddressItem
                    text='群组'
                    imageType="1"
                    onClickItem={()=>{this.props.navigation.dispatch({type:"Navigation",url:"GroupListPage"})}}
                />
                <AddressItem
                    text='标签'
                    imageType="2"
                    onClickItem={()=>{this.props.navigation.dispatch({type:"Navigation",url:"TagListPage"})}}
                />
            </View>
        )
    }


    _getItemLayout(data,index){
        let[length,separator,header] = [ITEM_HEIGHT,SEPARAtOR_HEIGHT,HEADER_HEIGHT];
        return {length,offset:(length+separator)*index+header,index};
    }
    navChat(yanId,name){
        console.log("address yanId="+yanId);
        this.props.navigation.dispatch({type:"Navigation",url:"ContacterInfoPage",params:{
            yanId,name
        }})
    }
    _deleteSession(groupId){
        console.log(groupId);
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

const mapStateToProps = state => ({
    chatRecords: state.chatReducer.chatRecords,
    userLogin: state.userReducer.userLogin,
    userInfo: state.userReducer.userInfo,
    addressBookMembers: state.addressBookReducer.addressBookMembers,
});
const mapDispatchToProps = dispatch => ({
    addressBookAction: bindActionCreators(addressBookActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(address);