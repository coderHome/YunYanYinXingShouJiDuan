/**
 * Created by crcb on 2017/12/18.
 */
import React from 'react'
import {View,Text,SectionList,TouchableHighlight,Image,CustomSeparatorComponent, StyleSheet,TextInput} from 'react-native'
import Nav from '../../component/NavigationBar'
import addressJson from '../../assets/mock/addressBook.json'
import Swipeout from 'react-native-swipeout';
import AddressItem from '../../component/address/addressItem'
import SectionHeaderItem from '../../component/address/sectionHeaderItem'
import PropTypes from 'prop-types';
import theme from '../../constant/styleParam'
import PageComponent from '../../component/backComponent'
// var StyleSheet = require('react-native-debug-stylesheet');

var sections = addressJson.addressBookPicker;

const ITEM_HEIGHT = 100;
const HEADER_HEIGHT = 30;
const SEPARAtOR_HEIGHT = 0;
export default class addTagPage extends PageComponent{
    constructor(props){
        super(props);
        this.state = {
            dataSource:sections,
            inputTxt:"a",
            tagMemberNum:0
        }
    }
    render(){
        return (
            <View style={styles.container}>
                <Nav
                    leftBtnIcon="angle-left"
                    leftBtnPress={() => this.props.navigation.dispatch({type: "Back"})}
                    title="标签设置"
                />
                <View style={styles.container}>
                    <SectionList
                        ref='list'
                        enableEmptySections
                        renderSectionHeader={this._sectionComp}
                        renderItem={this._renderItem}
                        sections={this.state.dataSource}
                        refreshing={false}
                        getItemLayout={this._getItemLayout}
                        stickySectionHeadersEnabled={true}
                        initialNumToRender={10}
                        ListHeaderComponent={this._renderHeader.bind(this)}
                    />
                </View>
            </View>
        )
    }

    _renderHeader(){
        return(
            <View style={styles.headerContainer}>
                <View>
                    <Text style={styles.tagTitleContainer}>标签名字</Text>
                    <TextInput
                        style={[theme._normalInputStyle,styles._margin]}
                        placeholder='未设置标签名字'
                        onChangeText={(v) => this.setState({inputTxt: v})}
                    />
                </View>
                <View>
                    <Text style={styles.tagTitleContainer}>标签成员({this.state.tagMemberNum})</Text>
                    <View style={styles.item} >
                        <Text numberOfLines={1} style={[styles.titleText,styles.colorGreen]}>
                            + 添加成员
                        </Text>
                    </View>
                </View>
            </View>
        )
    }

    _renderItem = (item) => {
        let data = item.item;
        return(
            <AddressItem
                userInfo={data}
                headImg={data.head}
                text={data.userName}
                onClickItem={(isChecked)=>{this._onChange(isChecked,data.id)}}
                isShowCheckBox={false}
            />
        )
    }

    _onChange(isChecked,id){
        console.log('checkbox  ischeck:'+isChecked+'   id:'+id);
    }
    _sectionComp = (info) => {
        var txt = info.section.key;
        return (
            <SectionHeaderItem
                text={txt}
            />

        )
    }



    _getItemLayout(data,index){
        let[length,separator,header] = [ITEM_HEIGHT,SEPARAtOR_HEIGHT,HEADER_HEIGHT];
        return {length,offset:(length+separator)*index+header,index};
    }
    navChat(groupId){
        console.log(groupId);
        this.props.navigation.dispatch({type:"Navigation",url:"Chat",params:{
            groupId,
        }})
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    tagTitleContainer:{
        marginHorizontal:theme.mainMargin,
        paddingTop:20,
        paddingBottom:10
    },
    headerContainer:{
        backgroundColor:'#EEEEEE',
    },
    item: {
        flex: 1,
        height: 60,
        overflow: 'hidden',
        position: 'relative',
        paddingVertical: 5,
        paddingHorizontal:15,
        borderBottomWidth:1,
        borderStyle:'solid',
        borderBottomColor:'#cccccc',
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'white'
    },
    titleText: {
        flex:1,
        fontSize: 16,
    },
    colorGreen:{
        color:'green'
    }
});