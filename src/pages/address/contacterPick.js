/**
 * Created by crcb on 2017/12/18.
 */
import React from 'react'
import {View,Text,SectionList,TouchableHighlight,Image,CustomSeparatorComponent, StyleSheet,TextInput} from 'react-native'
import Nav from '../../component/NavigationBar'
import addressJson from '../../assets/mock/addressBook.json'
import Swipeout from 'react-native-swipeout';
import AddressItem from '../../component/address/addressItem'
import SectionHeaderItem from '../../component/address/sectionHeaderItem'
import PropTypes from 'prop-types';
import theme from '../../constant/styleParam'
import * as addreBookUtils from '../../util/addressBookUtil'
import PageComponent from '../../component/backComponent'
var sections = addressJson.addressBookPicker;

const ITEM_HEIGHT = 100;
const HEADER_HEIGHT = 30;
const SEPARAtOR_HEIGHT = 0;
class contacterPick extends PageComponent{
    constructor(props){
        super(props);
        this.state = {
            dataSource:sections,
            tag:'',
        }
        // this.props.navigation.dispatch({type: "Back"});
    }
    render(){
        return (
            <View style={styles.container}>
                <Nav
                    leftBtnIcon="angle-left"
                    leftBtnPress={() =>{
                        const routers = this.props.routers;
                        console.log("routers==========",routers);
                        const router = routers.filter((value)=>{
                            return value.routeName == 'GesturePasswordLogin'
                        });
                        console.log("key==========",router[0].key);
                        const key = addreBookUtils.getNavigationKeyByRouterName(routers,'Login');
                        this.props.navigation.dispatch({type: "Back",key:key});
                        }
                    }
                    title="请选择"
                />
                <View style={styles.container}>
                    <SectionList
                        ref='list'
                        enableEmptySections
                        renderSectionHeader={this._sectionComp}
                        renderItem={this._renderItem}
                        sections={this.state.dataSource}
                        refreshing={false}
                        getItemLayout={this._getItemLayout}
                        stickySectionHeadersEnabled={true}
                        initialNumToRender={10}
                    />
                </View>
            </View>
        )
    }

    _renderHeader(){
        return(
            <View>
                <View>
                    <Text>标签名字</Text>
                    <TextInput
                        style={[theme._normalInputStyle,styles._margin]}
                        placeholder='未设置标签名字'
                        onChangeText={(text) => {
                            this.setState({
                                tag: text
                            })
                        }}
                    />
                </View>
            </View>
        )
    }

    _renderItem = (item) => {
        let data = item.item;
        return(
            <AddressItem
                userInfo={data}
                headImg={data.head}
                text={data.userName}
                onClickItem={(isChecked)=>{this._onChange(isChecked,data.id)}}
                isShowCheckBox={true}
                isCheck={false}
            />
        )
    }

    _onChange(isChecked,id){
        console.log('checkbox  ischeck:'+isChecked+'   id:'+id);
    }
    _sectionComp = (info) => {
        var txt = info.section.key;
        return (
            <SectionHeaderItem
                text={txt}
            />

        )
    }



    _getItemLayout(data,index){
        let[length,separator,header] = [ITEM_HEIGHT,SEPARAtOR_HEIGHT,HEADER_HEIGHT];
        return {length,offset:(length+separator)*index+header,index};
    }
    navChat(groupId){
        console.log(groupId);
        this.props.navigation.dispatch({type:"Navigation",url:"Chat",params:{
            groupId,
        }})
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});



import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
const mapStateToProps = state => ({
    routers:state.nav.routes
});
const mapDispatchToProps = dispatch => ({
});
export default connect(mapStateToProps, mapDispatchToProps)(contacterPick);

