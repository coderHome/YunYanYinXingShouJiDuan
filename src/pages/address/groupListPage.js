import React, {Component} from 'react';
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {StyleSheet, ListView,Text,Image, View, TouchableHighlight,FlatList} from 'react-native';
import Nav from '../../component/NavigationBar';
import Swipeout from 'react-native-swipeout';
import * as addressBookActions from '../../redux/action/addressBookAction';
import GroupData from '../../assets/mock/groupData.json'
import { Modal} from 'antd-mobile/lib'
import PageComponent from '../../component/backComponent'
class groupListPage extends PageComponent{
    constructor(props) {
        super(props);
        this.state={
            dataSource:GroupData.groupList
        }
        console.log("router------------",this.props.routers) ;
    }


    //初始化
    componentDidMount() {

    }

    render() {
        return (
            <View style={styles.container}>
                <Nav
                    leftBtnIcon="angle-left"
                    leftBtnPress={() => this.props.navigation.dispatch({type: "Back"})}
                    title="群组"
                />
                <FlatList
                    renderItem={this._renderItem.bind(this)}
                    getItemLayout={(data, index) => (
                        {length: 70, offset: (70) * index, index}
                    )}
                    initialNumToRender={10}
                    data={this.props.groupDatas}
                    keyExtractor={this._keyExtractor.bind(this)}
                    ListHeaderComponent={this._header.bind(this)}
                >
                </FlatList>
            </View>

        )
    }

    _header(){
        return (
            <TouchableHighlight underlayColor="rgba(0,0,0,0.2)" onPress={this._showCreateGroupDialog.bind(this)}>
                <View style={styles.item} >
                    <Text numberOfLines={1} style={[styles.titleText,styles.colorGreen]}>
                        + 新建群组
                    </Text>
                </View>
            </TouchableHighlight>
        )
    }

    _showCreateGroupDialog(){
        console.log("clicke");
        Modal.prompt(
            '新建群组',
            null,
            name => {this._createGroupsAction(name)},
            'default',
            null,
            ['请输入群组名称'],
        );
    }

    _createGroupsAction(name){
        this.props.addressBookActions.createGroup(name,()=>{
            console.log("新建群组成功");
        },()=>{
            console.log("新建群组失败");
        });
    }

    _keyExtractor(item){
        return item.id
    }
    _deleteSession(groupId){
        console.log(groupId);
        this.props.addressBookActions.deleteGroup(groupId);
    }
    _renderItem(data){
        let {item} = data;
        console.log(item);
        return (
            item.id=='0'?
            <TouchableHighlight underlayColor="rgba(0,0,0,0.2)" onPress={()=>{this._handlerClick(item.id,item.name)}}>
                <View style={styles.item}>
                    <Text numberOfLines={1} style={styles.titleText}>
                        {item.name}
                    </Text>
                </View>
            </TouchableHighlight>
                :
            <Swipeout backgroundColor="#ffffff" right={[{
                text: '删除',
                type: 'delete',
                onPress:() => {
                    this._deleteSession(item.id);
                }
            }]}>
                <TouchableHighlight underlayColor="rgba(0,0,0,0.2)" onPress={()=>{this._handlerClick(item.id,item.name)}}>
                    <View style={styles.item}>
                        <Text numberOfLines={1} style={styles.titleText}>
                            {item.name}
                        </Text>
                    </View>
                </TouchableHighlight>
            </Swipeout>
        )
    }

    _handlerClick(groupId,groupName){
        console.log(groupId);
        this.props.navigation.dispatch({type: "Navigation",url:'GroupContacterListPage',params:{
            groupId:groupId,
            groupName:groupName
        }})
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    line: {
        flex: 1,
        height: 1,
        flexDirection: 'row',
        backgroundColor: "#eeeeee",
    },
    item: {
        flex: 1,
        height: 60,
        overflow: 'hidden',
        position: 'relative',
        paddingVertical: 5,
        paddingHorizontal:15,
        borderBottomWidth:1,
        borderStyle:'solid',
        borderBottomColor:'#cccccc',
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center'
    },
    titleText: {
        flex:1,
        fontSize: 16,
    },
    colorGreen:{
        color:'green'
    }
})

const mapStateToProps = state => ({
    groupDatas: state.addressBookReducer.groupDatas,
    routers:state.nav.routes
});
const mapDispatchToProps = dispatch => ({
    addressBookActions: bindActionCreators(addressBookActions, dispatch),
});
export default connect(mapStateToProps, mapDispatchToProps)(groupListPage);



