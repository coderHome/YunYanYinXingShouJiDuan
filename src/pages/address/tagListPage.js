import React, {Component} from 'react';
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {StyleSheet, ListView,Text,Image, View, TouchableHighlight,FlatList} from 'react-native';
import Nav from '../../component/NavigationBar';
import Swipeout from 'react-native-swipeout';
import * as SessionsActions from '../../redux/action/sessionsAction';
import GroupData from '../../assets/mock/groupData.json'
import * as addressBookActions from '../../redux/action/addressBookAction';
import { Modal} from 'antd-mobile/lib'
import PageComponent from '../../component/backComponent'
class tagListPage extends PageComponent{
    constructor(props) {
        super(props);
        this.state={
            dataSource:GroupData.groupList
        }
    }


    //初始化
    componentDidMount() {
        this.props.addressBookAction.getTagList();
    }

    render() {
        return (
            <View style={styles.container}>
                <Nav
                    leftBtnIcon="angle-left"
                    leftBtnPress={() => this.props.navigation.dispatch({type: "Back"})}
                    title="标签"
                />
                <FlatList
                    renderItem={this._renderItem.bind(this)}
                    getItemLayout={(data, index) => (
                        {length: 70, offset: (70) * index, index}
                    )}
                    initialNumToRender={10}
                    data={this.props.tagList}
                    keyExtractor={this._keyExtractor.bind(this)}
                    ListHeaderComponent={this._header.bind(this)}
                >
                </FlatList>
            </View>

        )
    }

    _header(){
        return (
            <TouchableHighlight underlayColor="rgba(0,0,0,0.2)" onPress={this._showCreateGroupDialog.bind(this)}>
                <View style={styles.item} >
                    <Text numberOfLines={1} style={[styles.titleText,styles.colorGreen]}>
                        + 新建标签
                    </Text>
                </View>
            </TouchableHighlight>
        )
    }

    _showCreateGroupDialog(){
        console.log("clicke");
        this.props.navigation.dispatch({type: "Navigation",url:'AddTagPage',params:{
        }})
    }

    _keyExtractor(item){
        return item.name
    }
    _deleteSession(groupId){
        console.log(groupId);
    }
    _renderItem(data){
        let {item} = data;
        console.log(item);
        return (
            <Swipeout backgroundColor="#ffffff" right={[{
                text: '删除',
                type: 'delete',
                onPress:() => {
                    this._deleteSession(item.name);
                }
            }]}>
                <TouchableHighlight underlayColor="rgba(0,0,0,0.2)" onPress={()=>{this._handlerClick(item.name)}}>
                    <View style={styles.item}>
                        <Text numberOfLines={1} style={styles.titleText}>
                            {item.name} ({item.sum})
                        </Text>
                    </View>
                </TouchableHighlight>
            </Swipeout>
        )
    }

    _handlerClick(tagName){
        console.log(tagName);
        this.props.navigation.dispatch({type: "Navigation",url:'TagContacterListPage',params:{
            name:tagName
        }})
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    line: {
        flex: 1,
        height: 1,
        flexDirection: 'row',
        backgroundColor: "#eeeeee",
    },
    item: {
        flex: 1,
        height: 60,
        overflow: 'hidden',
        position: 'relative',
        paddingVertical: 5,
        paddingHorizontal:15,
        borderBottomWidth:1,
        borderStyle:'solid',
        borderBottomColor:'#cccccc',
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center'
    },
    titleText: {
        flex:1,
        fontSize: 16,
    },
    colorGreen:{
        color:'green'
    }
})

const mapStateToProps = state => ({
    tagList: state.addressBookReducer.tagList
});
const mapDispatchToProps = dispatch => ({
    addressBookAction: bindActionCreators(addressBookActions, dispatch),
});
export default connect(mapStateToProps, mapDispatchToProps)(tagListPage);



