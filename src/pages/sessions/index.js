import React, {Component} from 'react';
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {StyleSheet, ListView,Text,Image, View, TouchableHighlight,FlatList} from 'react-native';
import Nav from '../../component/NavigationBar';
import HttpFetch from '../../util/HttpFetch';
import Api from '../../constant/APIParam';
import Swipeout from 'react-native-swipeout';
import ListItem from '../../component/sessions/listItem';
import * as SessionsActions from '../../redux/action/sessionsAction';
import * as TYPES from '../../constant/Types';
import * as $U from '../../util/sessionsUtil';


class sessions extends Component{
    constructor(props) {
        super(props);
    }


    //初始化
    componentDidMount() {
       global.PubSub.on(TYPES.SESSIONS_CHANGE,this.changeSessions.bind(this));
    }

    //销毁
    componentWillUnmount() {
        global.PubSub.off(TYPES.SESSIONS_CHANGE,this.changeSessions.bind(this))
    }
    changeSessions(msg){
        console.log(msg);
        this.props.sessionsActions.changeSessions(msg);
    }

    render() {
        let sessions = $U.formatSessions(this.props.sessions);
        return (
            <View style={styles.container}>
                <Nav
                    title={this.props.title}
                />
                <FlatList
                    renderItem={this._renderItem.bind(this)}
                    getItemLayout={(data, index) => (
                        {length: 70, offset: (70) * index, index}
                    )}
                    initialNumToRender={10}
                    data={sessions}
                    keyExtractor={this._keyExtractor.bind(this)}
                >
                </FlatList>
            </View>

        )
    }

    _separator(){
        return <View style={styles.line}></View>
    }
    _keyExtractor(item){
        return item.sessionId
    }
    _deleteSession(groupId){
        console.log(groupId);
        this.props.sessionsActions.deleteSession(groupId);
    }
    navChat(groupId){
        console.log(groupId);
        this.props.sessionsActions.changeSessionUnread(groupId);
        this.props.navigation.dispatch({type:"Navigation",url:"Chat",params:{
            groupId,
        }})
    }

    _renderItem(data){
        let {item} = data;
        //console.log(item);
        return (
            <Swipeout backgroundColor="#ffffff" right={[{
                text: '删除',
                type: 'delete',
                onPress:() => {
                    this._deleteSession(item.sessionId);
                }
            }]}>
                <ListItem groupInfo={item} routerChat={this.navChat.bind(this)}></ListItem>
            </Swipeout>
        )


    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    line: {
        flex: 1,
        height: 1,
        flexDirection: 'row',
        backgroundColor: "#eeeeee",
    },


})

const mapStateToProps = state => ({
    sessions: state.sessionsReducer.sessions,
    title:state.sessionsReducer.title
});
const mapDispatchToProps = dispatch => ({
    sessionsActions: bindActionCreators(SessionsActions, dispatch),
});
export default connect(mapStateToProps, mapDispatchToProps)(sessions);



