/**
 * Created by crcb on 2017/12/9.
 */
import system from '../../constant/globalParam'
import theme from '../../constant/styleParam'
import {Dimensions,Platform} from 'react-native'
export default {
    content:{
        flex:1,
        backgroundColor:'white'
    },
    _main:{
        flex:1,
        flexDirection:"column",
    },
    _V1:{
        flex:1,
        flexDirection:"column",
        justifyContent:"center",
        alignItems:"center",
        // backgroundColor:"red",
    },
    _V2:{
        flex:1,
    },
    _txtIpt:{
        // flex:1,
        display:"flex",
        flexDirection:"row",
        justifyContent:"center",
        alignItems:"center",
        padding:10,
        paddingLeft:30,
        paddingRight:30,

    },
    _ipt:{
        flex:1,
        backgroundColor:"white",
        borderWidth:1,
        height:50,
        borderRadius:10,
        padding:5,
        paddingLeft:20,
    },
    _topImg:{
        // resizeMode:'stretch',
        height:230,
        width:theme.screenWidth,
        // display:'none'
    },
    _contacterImg:{
        resizeMode:'contain',
        height:100,
        width:100,
        position: 'absolute',
        top:115-50,
        left:theme.screenWidth/2-50
    },
    _inputContainer:{
        marginHorizontal:theme.btnMargin,
        borderRadius:5,
        flexDirection:'row',
        backgroundColor:'#f9f9f9',
        borderColor:'#666666',
        paddingHorizontal:20,
        paddingVertical:8,
        borderWidth:0.25
    },
    _iconStyle:{
        height:30,
        width:30
    },
    _inputStyle:{
        height:30,
        backgroundColor:'transparent',
        // backgroundColor:'red',
        paddingVertical: (Platform.OS === 'ios') ? 7 : 0,
        paddingHorizontal: 7,
        marginBottom:0,
        borderWidth:0,
        // fontSize:16,
        flex:1,
        // width:100
    },
    _viewContainer:{
        // backgroundColor:'red',
        marginTop:30,
    },
    _btn:{
        backgroundColor:theme.btnColor,
        marginHorizontal:theme.btnMargin,
        borderRadius:5,
        borderWidth:0,
        // color:'white'
    },
    _backgroundImage:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        width:null,
        height:null
    },
    container: {
        flex: 1,
        backgroundColor:'red',
        justifyContent: 'center',//
        paddingHorizontal: 20,
        paddingTop: 20,
    },
}