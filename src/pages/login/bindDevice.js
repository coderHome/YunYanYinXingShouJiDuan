/**
 * 设备绑定
 * Created by crcb on 2017/12/9.
 */
import React from 'react'
import {View, Text,TextInput, StyleSheet} from 'react-native'
import Nav from '../../component/NavigationBar'
import SmsButton from '../../component/SmsButton'
import Button from 'apsl-react-native-button'
import theme from '../../constant/styleParam'
import PageComponent from '../../component/backComponent'
import * as userActions from '../../redux/action/userAction'

class bindDevice extends PageComponent {

    constructor(props){
        super(props);
        this.state = {
            serno:null,//
            code:''
        };
    }
    componentDidMount() {
    }

    render() {
        return (
            <View style={styles.content}>
                <Nav
                    leftBtnIcon="angle-left"
                    leftBtnPress={() => this.props.navigation.dispatch({type: "Back"})}
                    title="设备绑定"
                />
                <View style={styles._main}>
                    {/*<TextInput*/}
                        {/*style={[theme._normalInputStyle,styles._margin]}*/}
                        {/*placeholder='手机号码'*/}
                        {/*keyboardType="numeric"*/}
                        {/*maxLength={11}*/}
                        {/*onChangeText={(text) => {*/}
                            {/*this.setState({*/}
                                {/*phone: text*/}
                            {/*})*/}
                        {/*}}*/}
                    {/*/>*/}
                    <SmsButton
                        enable={true}
                        ref={"t_smsCode"}
                        label={"验证码"}
                        timerCount={30}
                        onClick={(shouldStartCountting)=>{
                            shouldStartCountting(true);
                            this.props.userAction.getSMS(this.props.userInfo.phone);
                        }}
                        textOnChange={(text)=>{
                            this.setState({
                                code:text
                            })
                        }}
                    />
                    <Button
                            style={[{marginTop:30,backgroundColor:((this.state.code.length==6)?"#a67c52":"#cccccc")},styles.btn]}
                            onPress={()=>{
                                this._doBind()
                                }
                            }
                            textStyle={theme._normalBtnTextStyle}
                            disabled={this.state.code.length==6?false:true}
                    >
                        确认
                    </Button>
                </View>
            </View>
        )
    }

    _doBind() {
        console.log('doBind');
        const code = this.refs['t_smsCode'].getValue();
        const phone = this.props.userInfo.phone;
        this.props.userAction.bindDevice(phone,code,()=>{
            this.props.navigation.dispatch({type: "Navigation", url: "GesturePasswordSet"})
        });

        // const {serno,code} = this.state;
        // let phone  = this.refs["phone"].getValue();
        // let code1  = this.refs["t_smsCode"].getValue();
        // if(code!=code1||code==null||code1==null){
        //     AlertUtil.toast("验证码不正确");
        //     return ;
        // }
        // this.props.navigator.push({
        //     component:Pwd,
        //     args:{
        //         phone:phone,
        //     }
        // })
    }
}


const styles = StyleSheet.create({
    content:{
        backgroundColor:theme.white,
        flex:1
    },
    _main:{
        marginLeft:theme.btnMargin,
        marginRight:theme.btnMargin,
        paddingTop:40,
    },
    _margin:{
        marginLeft:theme.btnMargin-10,
        marginRight:theme.btnMargin-10,
    },
    btn:{
        // backgroundColor:,
        borderWidth:0,
        borderRadius:theme.borderRadius
    },
});


import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
const mapStateToProps = state => ({
    userInfo:state.userReducer.userInfo
});
const mapDispatchToProps = dispatch => ({
    userAction: bindActionCreators(userActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(bindDevice);