/**
 * Created by crcb on 2017/12/9.
 */
import React from 'react'
import {View, Text,TextInput, StyleSheet} from 'react-native'
import Nav from '../../component/NavigationBar'
import loginStyles from './loginStyle'
import {Button} from 'antd-mobile/lib'
import Param from '../../constant/HttpParam'

import * as userActions from '../../redux/action/userAction'
import * as addressBookActions from '../../redux/action/addressBookAction'
import * as chatActions from '../../redux/action/chatAction'
import * as testActions from '../../redux/action/testAction'
import PasswordGesture from 'react-native-gesture-password'
import * as userDao from '../../dao/userDao'
import PageComponent from '../../component/backComponent'

var Password1 = '123';

class GesturePasswordLogin extends PageComponent {

    constructor(props){
        super(props);
        this.state = {
            message: '请输入手势密码',
            status: 'normal'
        }
    }
    componentWillMount() {
        super.componentWillMount();
        Password1 = global.gesturePassword;
    }

    onEnd(password) {
        console.log(password);
        if (password == Password1) {
            this.setState({
                status: 'right',
                message: '解锁成功'
            },()=>{
                //数据缓存
                userDao.saveUserInfo(global.userInfo);
                // this.props.addressBookAction.getAddressBookVersion(()=>{
                    this.props.navigation.dispatch({type: "Reset", url: "HomePage"});
                // });
            });

            // your codes to close this view
        } else {
            this.setState({
                status: 'wrong',
                message: '手势密码错误，请重新输入！'
            });
        }
    }

    onStart() {
        console.log('onStart');
        this.setState({
            status: 'normal',
            message: '请输入手势密码'
        });
    }

    onReset() {
        this.setState({
            status: 'normal',
            message: '请重新输入手势密码'
        });
    }

    render() {
        return (
            <View style={loginStyles.content}>
                <Nav
                    title="手势解锁"
                />
                <PasswordGesture
                    ref='pg'
                    status={this.state.status}
                    message={this.state.message}
                    onStart={() => this.onStart()}
                    onEnd={(password) => this.onEnd(password)}
                    onReset={() => this.onReset()}
                    innerCircle={true}
                    outerCircle={true}
                    interval = {1000}
                    style={styles.gesturePasswordStyle}
                    normalColor='#ccc'
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    gesturePasswordStyle:{
        backgroundColor:'white',
    }
})

import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({
    userAction: bindActionCreators(userActions, dispatch),
    addressBookAction: bindActionCreators(addressBookActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(GesturePasswordLogin);
