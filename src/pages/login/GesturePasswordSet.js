/**
 * Created by crcb on 2017/12/9.
 */
import React from 'react'
import {View, Text,TextInput, StyleSheet} from 'react-native'
import Nav from '../../component/NavigationBar'
import loginStyles from './loginStyle'
import {Button} from 'antd-mobile/lib'
import Param from '../../constant/HttpParam'

import * as userActions from '../../redux/action/userAction'
import * as chatActions from '../../redux/action/chatAction'
import * as testActions from '../../redux/action/testAction'
import PasswordGesture from 'react-native-gesture-password'
import * as userDao from '../../dao/userDao'
import PageComponent from '../../component/backComponent'
import { NavigationActions } from 'react-navigation';
// var StyleSheet = require('react-native-debug-stylesheet');
var Password1 = '';

class GesturePasswordSet extends PageComponent{

    constructor(props){
        super(props);
        this.state = {
            message: '请绘制解锁图案',
            status: 'normal',
            isShowResetBtn:false
        }
    }

    onEnd(password) {
        if ( Password1 === '' ) {
            // The first password
            Password1 = password;
            this.setState({
                status: 'normal',
                message: '请再次绘制解锁图案',
                isShowResetBtn:true
            });
        } else {
            // The second password
            if ( password === Password1 ) {
                this.setState({
                    status: 'right',
                    message: '手势解锁设置成功'
                },()=>{
                    userDao.saveGesturePassword(password);
                    userDao.saveUserInfo(global.userInfo);
                    userDao.saveLoginToken(global.loginToken);
                    global.gesturePassword = password;
                    this.props.navigation.dispatch({type: "Reset", url: "HomePage"});
                });
                Password1 = '';
                // your codes to close this view
            } else {
                this.setState({
                    status: 'wrong',
                    message:  '手势图案不一致，请重新绘制'
                });
            }
        }
    }

    onStart() {
        if ( Password1 === '') {
            this.setState({
                message: '请绘制解锁图案'
            });
        } else {
            this.setState({
                status: 'normal',
                message: '请再次绘制解锁图案'
            });
        }
    }

    _resetPassword(){
        Password1 = '';
        this.setState({
            status: 'normal',
            message: '请绘制解锁图案',
            isShowResetBtn:false
        });
    }

    render() {
        return (
            <View style={loginStyles.content}>
                <Nav
                    leftBtnIcon="angle-left"
                    leftBtnPress={() => this.props.navigation.dispatch({type: "Back"})}
                    title="手势密码设置"
                />
                <PasswordGesture
                    ref='pg'
                    status={this.state.status}
                    message={this.state.message}
                    onStart={() => this.onStart()}
                    onEnd={(password) => this.onEnd(password)}
                    innerCircle={true}
                    outerCircle={true}
                    interval = {500}
                    style={styles.gesturePasswordStyle}
                    normalColor='#ccc'
                />
                <View style={styles.textContainer}>
                    {this.state.isShowResetBtn?
                    <Text style={styles.tipText} onPress={()=>this._resetPassword()}>
                        重新绘制 >>
                    </Text>
                        : null}
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    gesturePasswordStyle:{
        backgroundColor:'white',
    },
    tipText:{
        color:'#ccc',
        textAlign:'center'
    },
    textContainer:{
        backgroundColor:'white',
        height:100,
        justifyContent:'center',
    }

})

import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({
    userAction: bindActionCreators(userActions, dispatch),
    chatAction: bindActionCreators(chatActions, dispatch),
    testAction: bindActionCreators(testActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(GesturePasswordSet);
