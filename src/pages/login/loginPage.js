/**
 * Created by crcb on 2017/12/9.
 */
import React from 'react'
import {View, Text, TextInput,modal,Image, StyleSheet,Platform,NativeAppEventEmitter} from 'react-native'
import Nav from '../../component/NavigationBar'
import styles from './loginStyle'
import Config from '../../constant/globalParam'
import * as userActions from '../../redux/action/userAction'
import * as chatActions from '../../redux/action/chatAction'
import * as testActions from '../../redux/action/testAction'
import RSA from '../../assets/Js/wx_rsa'
import Param from '../../constant/HttpParam'
import Button from 'apsl-react-native-button'
import theme from '../../constant/styleParam'
import PageComponent from '../../component/backComponent'
import Tts from 'react-native-tts';
import InputScrollView from 'react-native-input-scroll-view';
var inputScrollView;
class login extends PageComponent {

    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
        }
    }

    componentWillMount() {
        super.componentWillMount();
    }

    componentDidMount() {
        // Tts.setDefaultLanguage('en-US');
        // Tts.speak('您好 124432 abfhvh');
        // Tts.addEventListener('tts-start', (event) => console.log("start", event));
        // Tts.addEventListener('tts-finish', (event) => console.log("finish", event));
        // Tts.addEventListener('tts-cancel', (event) => console.log("cancel", event));
    }

    componentWillUnmount() {
        super.componentWillUnmount();
        console.log('销毁');
        this.inputScrollView._removeListener();
    }

    render() {
        return (
            <View style={styles.content}>
                <Nav
                    title="登录"
                />
                <InputScrollView style={styles._main} ref={(ref) => { this.inputScrollView = ref }}>
                    {/*<Image source={require('../../assets/Image/login/bg.png')} style={styles._topImg}/>*/}
                    <View  style={styles._topImg}/>
                    <Image
                        source={require('../../assets/Image/login/headIconWithBorder.png')}
                        style={styles._contacterImg}
                    />
                    <View style={styles._viewContainer}>

                        <View style={styles._inputContainer}>
                            <Image
                                style={styles._iconStyle}
                                source={require('../../assets/Image/login/accountIcon.png')}/>
                            <TextInput
                                onChangeText={(v) => this.setState({username: v})}
                                underlineColorAndroid="transparent"
                                placeholder='工号'
                                style={styles._inputStyle}
                            />
                        </View>
                        <View style={[styles._inputContainer,{marginTop:15}]}>
                            <Image
                                style={styles._iconStyle}
                                source={require('../../assets/Image/login/passwordIcon.png')}/>
                            <TextInput
                                placeholder='密码'
                                underlineColorAndroid="transparent"
                                onChangeText={(v) => this.setState({password: v})}
                                style={styles._inputStyle}
                                secureTextEntry = {true}
                            />
                        </View>
                    </View>
                    <Button
                        style={[styles._btn,{marginTop:75}]}
                        textStyle={theme._normalBtnTextStyle}
                        onPress={() => {
                            //console.log('username = ',this.state.username,' password = ',this.state.password);
                            let password = this.state.password;
                            let username = this.state.username;
                            if(Config.DEBUG){
                                password = "123456";
                                username = "000001";
                            }

                            // RSA加密
                            let encrypt_rsa = new RSA.RSAKey();
                            encrypt_rsa = RSA.KEYUTIL.getKey(Param.rsa_pub_key);
                            let encStr = encrypt_rsa.encrypt(password)
                            encStr = RSA.hex2b64(encStr);
                            var obj = {
                                username: username,
                                password: encStr
                            }
                            this.props.userAction.loginuser(obj, () => {
                                this.inputScrollView._removeListener();
                                {/*this.props.navigation.dispatch({type: "Navigation", url: "BindDevice"})*/}
                            })
                        }}
                    >登录</Button>
                </InputScrollView>
            </View>
        )

    }
}


import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({
    userAction: bindActionCreators(userActions, dispatch),
    chatAction: bindActionCreators(chatActions, dispatch),
    testAction: bindActionCreators(testActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(login);