/**
 * Created by crcb on 2017/12/9.
 */
import React from 'react'
import {AppRegistry,View, Text, TextInput, modal, Platform, BackHandler,Image} from 'react-native'
import Nav from '../../component/NavigationBar'
import styles from './loginStyle'
import {Button} from 'antd-mobile/lib'
import Param from '../../constant/HttpParam'

import * as userActions from '../../redux/action/userAction'
import * as chatActions from '../../redux/action/chatAction'
import * as testActions from '../../redux/action/testAction'
import * as storageDao from '../../util/storageUtil'
import * as userDao from '../../dao/userDao'

import * as pSub from '../../PubSub/index'
import Storage from '../../storage/index'

import * as addressBookUtil from '../../util/addressBookUtil';

import RSA from '../../assets/Js/wx_rsa'
import AddressBookJson from '../../assets/mock/addressBook.json'
import * as AddressBookDao from '../../dao/addressBookDao'

import BackPage from '../../component/backComponent'
import {CachedImage,ImageCache} from 'react-native-img-cache'
import config from '../../constant/globalParam'
// import {ImageCache} from 'react-native-fetch-blob'

import SplashScreen from "../../native_modules/SplashScreen";
class login extends BackPage {

    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
        }
    }
    _initData(){
        userDao.getLocalParams(["loginToken","userInfo","gesturePassword"],(data)=>{
            const loginToken = data[0];
            const userInfo = data[1];
            const gesturePassword = data[2];
            // const test = data[3];
            if(loginToken&&userInfo&&gesturePassword){
                global.loginToken = loginToken;
                global.userInfo = userInfo;
                global.gesturePassword = gesturePassword;
                this.props.userAction.tokenLogin(()=>{
                    this.props.navigation.dispatch({type: "Reset", url: "GesturePasswordLogin"})
                },()=>{
                    this.props.navigation.dispatch({type: "Reset", url: "LoginPage"})
                });
            }else{
                this.props.navigation.dispatch({type: "Reset", url: "LoginPage"})
            }
        },()=>{
            this.props.navigation.dispatch({type: "Reset", url: "LoginPage"})
        })
    }
    componentWillMount() {

        // this.timer = setTimeout(()=>{
        //     this._initData();
        // },config.LAUNCH_SPLASH_TIME);

        //初始化token
        // this.props.userAction.tokenLogin(()=>{
        //     this.props.navigation.dispatch({type: "Navigation", url: "GesturePasswordLogin"})
        // },()=>{
        //     //删除所有缓存数据
        //     // chatActions.clearAllRecords();
        //     this.props.navigation.dispatch({type: "Navigation", url: "LoginPage"})
        // });
    }
    //初始化
    componentDidMount() {

        // userDao.getLoginToken().then(
        //     (value)=>{
        //         global.loginToken = value;
        //         console.log("初始化 token",global.loginToken);
        //     }
        // );
        SplashScreen.hide();
        this._initData();
    }
    render() {
        return (
            config.DEBUG?
            <View style={styles.content}>
                <Nav
                    title="首页"
                />
                <View style={styles._main}>
                    <View style={styles._V1}>
                        <View style={styles._txtIpt}>
                            <TextInput value={this.state.username}
                                       onChangeText={(v) => this.setState({username: v})}
                                       underlineColorAndroid="transparent"
                                       autoCapitalize="none"
                                       autoCorrect={false}
                                       placeholder={"用户名"}
                                       style={styles._ipt}/>
                        </View>
                        <View style={styles._txtIpt}>
                            <TextInput value={this.state.password}
                                       onChangeText={(v) => this.setState({password: v})}
                                       placeholder={"密码"}
                                       style={styles._ipt}
                                       underlineColorAndroid="transparent"
                                       secureTextEntry={true}
                            />
                        </View>
                    </View>
                    <View style={styles._V2}>
                        <Button type="primary" style={styles._btn} onPressIn={() => {
                            //console.log('username = ',this.state.username,' password = ',this.state.password);
                            let password = "123456";
                            // RSA加密
                            let encrypt_rsa = new RSA.RSAKey();
                            encrypt_rsa = RSA.KEYUTIL.getKey(Param.rsa_pub_key);
                            let encStr = encrypt_rsa.encrypt(password)
                            encStr = RSA.hex2b64(encStr);
                            let obj = {
                                username: "000001",
                                password: encStr
                            }
                            this.props.userAction.loginuser(obj, () => {
                                this.props.navigation.dispatch({type: "Navigation", url: "HomePage"})
                            })
                        }}>登录</Button>
                        <Button type="primary" style={styles._btn} onPressIn={() => {
                            chatActions.clearOneRecord();
                        }}>
                            清空所有聊天缓存
                        </Button>
                        <Button type="primary" style={styles._btn} onPressIn={() => {
                            chatActions.clearAllRecords()
                        }}>
                            清空所有缓存
                        </Button>
                        <Button type="primary" style={styles._btn} onPressIn={() => {
                            this.props.navigation.dispatch({type: "Navigation", url: "QRCodePage"})
                        }}>
                            跳转登录页
                        </Button>
                        <Button type="primary" style={styles._btn} onPressIn={() => {
                            global.storageUtil.readMoreKey(["a","b","c","d"],(data)=>{
                                console.log("data = ",data);
                            })
                        }}>
                            传递多个参数度缓存
                        </Button>
                        <Button type="primary" style={styles._btn} onPressIn={() => {
                            ImageCache.get().clear();
                        }}>
                            清空图片缓存
                        </Button>

                    </View>
                </View>
            </View>
                :
                <View style={styles.content}>
                    {/*<Image*/}
                        {/*style={styles._backgroundImage}*/}
                        {/*source={require('../../assets/Image/addressbook/splash.png')}*/}
                    {/*></Image>*/}
                </View>
        )
    }
}


import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({
    userAction: bindActionCreators(userActions, dispatch),
    chatAction: bindActionCreators(chatActions, dispatch),
    testAction: bindActionCreators(testActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(login);