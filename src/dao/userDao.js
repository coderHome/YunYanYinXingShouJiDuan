/**
 * Created by qianyf on 2017/12/29.
 */

/**
 * 保存loginToken
 * @param _key
 * @param value
 */
export function saveLoginToken(value){
    //更新全局token
    global.loginToken = value;
    global.storageUtil.save('loginToken',value);
    // return storageUtils.save('loginToken',value);
}

export function getLoginToken(callback){
    return global.storageUtil.read('loginToken',(data)=>{callback(data)});
}

export function saveUserInfo(value){
    global.userInfo = value;
    global.storageUtil.save('userInfo',value);
    // return storageUtils.save('userInfo',value);
}

export function getUserInfo(callback){
    return global.storageUtil.read('userInfo',(data)=>{callback(data)});
}
export function saveGesturePassword(value){
    global.storageUtil.save('gesturePassword',value);
    // return storageUtils.save('gesturePassword',value);
}
export function getGesturePassword(callback){
    return global.storageUtil.read('gesturePassword',(data)=>{callback(data)});
}
export function getTokenAndUserInfo(succCallback,errorCallback){
    global.storageUtil.readMoreKey(["loginToken","userInfo"],(data)=>{
        if(data[0] && data[1]){
            global.loginToken = data[0];
            succCallback(data);
        }else{
            global.loginToken = '';
            errorCallback();
        }
    },()=>{
        errorCallback();
    })
}

export function getLocalParams(params,succCallback,errorCallback){
    global.storageUtil.readMoreKey(params,(data)=>{
        succCallback(data);
    },
        errorCallback
    )
}

export function clearAllData(){
    global.storageUtil.clearAllData();
}

