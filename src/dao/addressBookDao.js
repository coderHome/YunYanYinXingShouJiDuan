/**
 * Created by qianyf on 2017/12/29.
 */

/**
 * 保存
 * @param _key
 * @param value
 */
export function saveAddressBookMembers(value){
    //更新全局token
    global.storageUtil.save('addressBookMemebers',value);
}

export function getAddressBookMemebers(callback){
    return global.storageUtil.read('addressBookMemebers',(data)=>{callback(data)});
}

export function saveOriginMembers(value){
    //更新全局token
    global.storageUtil.save('orignMembers',value);
}

export function getOriginMembers(callback){
    return global.storageUtil.read('orignMembers',(data)=>{callback(data)});
}

export function saveAddressBookVersion(value){
    //更新全局token
    global.storageUtil.save('addressBookVersion',value);
}

export function getAddressBookVersion(callback){
    return global.storageUtil.read('addressBookVersion',(data)=>{callback(data)});
}