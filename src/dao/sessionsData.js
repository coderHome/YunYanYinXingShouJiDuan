import {AsyncStorage} from 'react-native';
import {store} from '../redux/store/index';

export default class sessionsDao{
    setItem(data){
        return new Promise((resolve,reject)=>{
            let {userInfo} = store.getState().userReducer;
            data['userId'] = userInfo.yanId;
            AsyncStorage.setItem("sessionsData",JSON.stringify(data),(error)=>{
                if(!error){
                    resolve("操作成功");
                }else{
                    reject("操作失败");
                }
            })
        })
    }
    getItem(){
        return new Promise((resolve,reject)=>{
            let {userInfo} = store.getState().userReducer;
            AsyncStorage.getItem("sessionsData",(error,res)=>{
                if(!error){
                    const list = JSON.parse(res);
                    // console.log('storage',list);
                    if(list&&list.userId==userInfo.yanId){
                        resolve(list)
                    }
                    else{
                        reject(null);
                    }
                }else{
                    reject(null)
                }
            })
        })
    }

    remove(){
        return new Promise((resolve,reject)=>{
            AsyncStorage.removeItem("sessionsData",(error,res)=>{
                if(!error){
                   resolve('删除成功');
                }else{
                    reject('删除失败')
                }
            })
        })
    }

}
