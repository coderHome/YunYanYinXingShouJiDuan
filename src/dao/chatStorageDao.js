import * as PackagingMsg from "../util/PackagingMsg";
import RNFetchBlob from "react-native-fetch-blob";
import HttpParam from "../constant/HttpParam";
import ErrorUtil from "../util/ErrorUtil";
import {Toast} from "antd-mobile/lib/index";

const _key = "records";
const _time = 60000 * 60 * 24; //缓存24小时

/**
 * 读取聊天记录缓存,如果没有就发接口获取
 * @param groupId
 * @param number
 * @param callback
 */
export function getRecords(groupId, number, callback) {
    global.storageUtil.read_Id(_key, groupId, (data) => {
        if (data) {
            // console.log("storage ------- 获取数据成功", data.msg);

            Toast.loading("加载聊天记录",0);
            let list = data.msg.filter((item, index) => {
                if (index < number) return true
            });
            // fetchDownyuyin(list);
            callback(list.reverse());
        } else {
            // console.log("storage ------- 获取数据失败");
            global.socket.emit("client_msg_record", {
                groupId: groupId,
                countMsg: number,
            }, function (data) {
                let obj = {
                    msg:data.reverse()
                };
                global.storageUtil.save_Id(_key,groupId,obj,_time)
                callback(data.reverse());
            });
        }
    })
}


/**
 * 接收消息后把消息加到缓存
 * @param msg
 */
export function addOneRecords(msg) {
    let groupId = msg.groupId;
    global.storageUtil.read_Id(_key,groupId,(data)=>{
        if(data){
            let list = data.msg;
            list.unshift(msg);
            global.storageUtil.save_Id(_key,groupId,{msg:list},_time);
            // console.log("新增一条缓存成功");
        }else{
            global.storageUtil.save_Id(_key,groupId,{msg:[msg]},_time);
            // console.log("新建一条缓存成功");
        }
    })
}

/**
 * 下拉刷新，加入缓存
 * @param listdata
 * @param number
 */
export function addMoreRecords(listdata, number) {
    if(listdata.length>0){
        let groupId = listdata[0].groupId;
        global.storageUtil.read_Id(_key,groupId,(data)=>{
            if(data){
                // console.log("获取到的所有缓存 = ", data.msg);
                let oldlist = data.msg.filter((item, index) => {
                    if (index < number) return true
                })
                // console.log("刷新加载的数据 = ", listdata);
                // console.log("原来的数据 = ", oldlist);
                let list = oldlist.concat(listdata.reverse());
                // console.log("刷新后的缓存数据 = ", list);
                global.storageUtil.save_Id(_key,groupId,{msg:list},_time);
                // console.log("下拉新增了缓存")
            }else{
                global.storageUtil.save_Id(_key,groupId,{msg:listdata},_time);
            }
        })
    }else{
        console.log("没有记录缓存");
        ErrorUtil("没有更多记录");
    }

}

/**
 * 获取当前group的聊天记录
 * @param groupId
 * @param callback
 */
export function getNowRecordNum(groupId, callback) {
    global.storageUtil.read_Id(_key,groupId,(data)=>{
        if(data){
            callback(data.msg)
        }else{
            callback([]);
        }
    })
}