/**
 * tform表单控件整体样式
 */
'use strict';

import { Platform, StyleSheet } from 'react-native';
import px2dp from '../../util/px2dp';
import theme from '../../constant/styleParam';

var LABEL_COLOR = '#000000';
var INPUT_COLOR = '#000000';
var ERROR_COLOR = '#a94442';
var HELP_COLOR = '#999999';
var BORDER_COLOR = '#cccccc';
var DISABLED_COLOR = '#777777';
var DISABLED_BACKGROUND_COLOR = '#eeeeee';
var FONT_SIZE = 14;
var FONT_WEIGHT = '500';


var stylesheet = Object.freeze({
  fieldset: {},
  // the style applied to the container of all inputs
  formGroup: {
    normal: {
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        marginTop:0,
        marginBottom:10,
        height:46,
        // backgroundColor:theme.red,
        borderRadius:theme.borderRadius,
        borderWidth:0.25,
    },
      normal2: {
          flexDirection:'row',
          alignItems:'center',
          justifyContent:'center',
          marginTop:5,
          marginBottom:5,
          marginLeft:5,
          marginRight:5,
          height:46,
          backgroundColor:theme.white,
          borderRadius:3,
      },
    error: {
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        marginTop:0,
        marginBottom:10,
        marginLeft:20,
        marginRight:20,
        height:46,
        backgroundColor:theme.white,
        borderRadius:3,
        //额外错误样式
        borderColor:theme.orangeRed,
        borderWidth:1,
        padding:1,
    }
  },
  controlLabel: {
    normal: {
        fontSize:16,
        color:theme.normalGray,
        fontWeight:'normal',
        marginLeft:15,
        width:65,
        marginBottom:0,
        marginRight:5,
    },
      normal2: {
        fontSize:14,
        color:theme.normalGray,
        fontWeight:'normal',
        marginLeft:10,
        width:30,
        marginBottom:0,
        marginRight:5,
    },
    // the style applied when a validation error occours
    error: {
        fontSize:16,
        color:theme.fontBlack,
        fontWeight:'normal',
        marginLeft:15,
        width:65,
        marginBottom:0,
        marginRight:5,
    }
  },
  helpBlock: {
    normal: {
        color:theme.normalGray,
        fontWeight:'normal',
        marginBottom:0,
        marginRight:10,
        fontSize:14,
    },
    // the style applied when a validation error occours
    error: {
        color:theme.normalGray,
        fontWeight:'normal',
        marginBottom:0,
        marginRight:10,
        fontSize:14,
    }
  },
  errorBlock: {
      fontSize: 13,
      marginBottom: 2,
      color: ERROR_COLOR,
      width:65
  },
  textboxView: {
    normal: {
        flex:1,
        marginRight:0,
        marginBottom:0,
    },
    error: {
        flex:1,
        marginRight:0,
        marginBottom:0,
    },
    notEditable: {
        flex:1,
        marginRight:0,
        marginBottom:0,
    }
  },
    checkCodeView: {
        normal: {
            flex:1,
            marginRight:0,
            marginBottom:0,
            backgroundColor:'#f9f9f9',
            height:45,
            justifyContent:'center',
            borderBottomLeftRadius:theme.borderRadius,
            borderTopLeftRadius:theme.borderRadius,
            // borderWidth:1,
            borderColor:theme.lineGray
        },
        error: {
            flex:1,
            marginRight:0,
            marginBottom:0,
            backgroundColor:'#f9f9f9',
            borderRadius:3,
            height:45,
            borderBottomLeftRadius:theme.borderRadius,
            borderTopLeftRadius:theme.borderRadius,
            // borderWidth:1,
            borderColor:theme.lineGray
        },
        notEditable: {
            flex:1,
            marginRight:0,
            marginBottom:0,
            backgroundColor:'#f9f9f9',
            borderRadius:3,
            height:45,
            borderBottomLeftRadius:theme.borderRadius,
            borderTopLeftRadius:theme.borderRadius,
            // borderWidth:1,
            borderColor:theme.lineGray
        }
    },
  textbox: {
    normal: {
        //color: theme.mainThemeColor,
        height: 45,
        paddingVertical: (Platform.OS === 'ios') ? 7 : 0,
        paddingHorizontal: 20,
        borderRadius: 4,
        borderColor: BORDER_COLOR,
        marginBottom:0,
        borderWidth:0,
        fontSize:16,
    },
    // the style applied when a validation error occours
    error: {
        //color: colors.orangeRed,
        height: 45,
        paddingVertical: (Platform.OS === 'ios') ? 7 : 0,
        paddingHorizontal: 7,
        borderRadius: 4,
        borderColor: BORDER_COLOR,
        marginBottom:0,
        borderWidth:0,
        fontSize:16,
    },
    // the style applied when the textbox is not editable
    notEditable: {
        height: 45,
        paddingVertical: (Platform.OS === 'ios') ? 7 : 0,
        paddingHorizontal: 7,
        borderRadius: 4,
        borderColor: BORDER_COLOR,
        marginBottom:0,
        borderWidth:0,
        fontSize:16,
    }
  },
  checkbox: {
    normal: {
      marginBottom: 4
    },
    // the style applied when a validation error occours
    error: {
      marginBottom: 4
    }
  },
  pickerContainer: {
    normal: {
        marginBottom: 4,
        borderRadius: 4,
        borderColor: BORDER_COLOR,
        borderWidth:0,
        flex:1,
        alignSelf:"center",
        flexDirection:"column",
    },
    error: {
        marginBottom: 4,
        borderRadius: 4,
        borderColor: BORDER_COLOR,
        borderWidth:0,
        flex:1,
        alignSelf:"center",
        flexDirection:"column",
    },
    open: {
        alignSelf:"flex-start"
    }
  },
  select: {
    normal: Platform.select({
      android: {
          paddingLeft: 7,
          color: INPUT_COLOR,
          flex:1,
          marginBottom:0,
      },
      ios: {
          flex:1,
          marginBottom:0,
      }
    }),
    // the style applied when a validation error occours
    error: Platform.select({
      android: {
          paddingLeft: 7,
          color: INPUT_COLOR,
          flex:1,
          marginBottom:0,
      },
      ios: {
          flex:1,
          marginBottom:0,
      }
    })
  },
  pickerTouchable: {
    normal: {
      height: 44,
      flexDirection: 'row',
      alignItems: 'center'
    },
    error: {
      height: 44,
      flexDirection: 'row',
      alignItems: 'center'
    },
    active: {
      borderBottomWidth: 0,
      borderColor: BORDER_COLOR
    }
  },
  pickerValue: {
    normal: {
        paddingLeft: 7,
        backgroundColor:'transparent',
        fontSize:16,
        //color:theme.mainThemeColor,
    },
    error: {
        paddingLeft: 7,
        backgroundColor:'transparent',
        fontSize:16,
        //color:theme.mainThemeColor,
    }
  },
  datepicker: {
    normal: {
      marginBottom: 4
    },
    // the style applied when a validation error occours
    error: {
      marginBottom: 4
    }
  },
  dateTouchable: {
    normal: {},
    error: {}
  },
  dateValue: {
    normal: {
      color: INPUT_COLOR,
      fontSize: FONT_SIZE,
      padding: 7,
      marginBottom: 5
    },
    error: {
      color: ERROR_COLOR,
      fontSize: FONT_SIZE,
      padding: 7,
      marginBottom: 5
    }
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
  button: {
    height: 36,
    backgroundColor: '#48BBEC',
    borderColor: '#48BBEC',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
    itemStyle: {
        borderWidth:0,
        borderColor:theme.bgColor,
        backgroundColor:theme.white,
        //fontSize:16
    },
});

module.exports = stylesheet;
