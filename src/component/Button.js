'use strict';

import React, {Component} from 'react';
import {Text,ViewPropTypes, View, StyleSheet, Platform, TouchableOpacity, TouchableNativeFeedback} from 'react-native';
import theme from '../constant/styleParam';
import PropTypes from 'prop-types'
export default class Button extends Component{

    static propTypes = {
        text: PropTypes.string.isRequired,
        onPress: PropTypes.func,
        backgroundColor:PropTypes.string,
        fontColor:PropTypes.string,
        style: ViewPropTypes.style,
        btnEnable:PropTypes.bool
    };

    static defaultProps = {
        backgroundColor:theme.btnColor,
        fontColor:theme.white
    };

    render(){
        if(this.props.btnEnable){
            if(Platform.OS === 'android') {
                return (
                    <TouchableNativeFeedback
                        onPress={this.props.onPress}>
                        {this._renderContent()}
                    </TouchableNativeFeedback>
                );
            }else if(Platform.OS === 'ios'){
                return(
                    <TouchableOpacity
                        onPress={this.props.onPress}>
                        {this._renderContent()}
                    </TouchableOpacity>
                );
            }
        }else{
            if(Platform.OS === 'android') {
                return (
                    this._renderContent()
                );
            }else if(Platform.OS === 'ios'){
                return(
                    this._renderContent()
                );
            }
        }

    }

    _renderContent(){
        return(
            <View style={[styles.container,{backgroundColor: this.props.backgroundColor,},this.props.style]}>
                <Text style={[styles.text,{color:this.props.fontColor}]}>{this.props.text}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        height: 45,
        alignItems:'center',
        justifyContent:'center',
        borderBottomRightRadius:theme.borderRadius,
        borderTopRightRadius:theme.borderRadius
    },
    text:{
        fontSize: 13
    }
});