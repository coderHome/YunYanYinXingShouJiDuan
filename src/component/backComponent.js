import React from 'react';
import {BackHandler, ToastAndroid} from 'react-native';
import {AppNavigator} from '../AppNavigator/AppNavigators';
import {store} from '../redux/store/index';
export default class PageComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        console.log("监听了返回按钮")
        BackHandler.addEventListener('hardwareBackPress', this._handleBack.bind(this));
    }

    componentWillUnmount() {
        console.log("卸载了返回按钮的监听事件")
        BackHandler.removeEventListener('hardwareBackPress', this._handleBack.bind(this));
    }

    _handleBack() {
        // if (this.lastBackPressed && this.lastBackPressed + 2000 >= Date.now()) {
        //     //最近2秒内按过back键，可以退出应用。
        //     return false;
        // }
        // this.lastBackPressed = Date.now();
        // ToastAndroid.show('再按一次退出应用', ToastAndroid.SHORT);
        // console.log("this.props = ",this.props);
        // console.log("this.props.navigation = ",this.props.navigation);
        // let rountName = this.props.navigation.state.routeName;
        // console.log("rountName = ",rountName)
        // this.props.navigation.dispatch({type: "Back"});
        // return true;

        // var routers = this.props.nav.routers;

        const state = store.getState();
        let routers = state.nav.routes;
        if (routers.length > 1 && routers) {
            this.props.navigation.dispatch({type: "Back"});
        }else{
            if (this.lastBackPressed && this.lastBackPressed + 2000 >= Date.now()) {
                //最近2秒内按过back键，可以退出应用。
                BackHandler.exitApp();
            }
            this.lastBackPressed = Date.now();
            ToastAndroid.show('再按一次退出应用', ToastAndroid.SHORT);
        }
        return true;
    }
}

