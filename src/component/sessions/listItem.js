import React, {Component} from 'react';
import {StyleSheet, ListView, Text, Image, View, TouchableHighlight, FlatList} from 'react-native';
import PropTypes from 'prop-types';
import * as $U from '../../util/sessionsUtil';
const PIC_URL=require('../../assets/Image/customer/head.jpg');
export default class ListItem extends Component {
    constructor(props) {
        super(props);
    }

    //初始化
    componentDidMount(){

    }

    //销毁
    componentWillUnmount(){

    }

    render(){

        //let item = $U.transSession($U.deepCopy(this.props.groupInfo));
        let item = this.props.groupInfo;
        // console.log(item);
        return (
            <TouchableHighlight underlayColor="rgba(0,0,0,0.2)" onPress={() => {
                this.props.routerChat(item.sessionId);
            }}>
                <View style={styles.item}>
                    {item.unread? <View style={styles.unread}></View>:null}
                    <View style={styles.msg}>
                        <View style={styles.picBox}>
                            <Image style={styles.pic} source={PIC_URL}/>
                        </View>
                        <View style={styles.block}>
                            <View style={styles.blockTop}>
                                <View style={styles.title}>
                                    <Text numberOfLines={1} style={styles.titleText}>{item.name}</Text>
                                </View>
                                <View style={styles.date}>
                                    <Text style={styles.dateText}>{item.lastMsg.timeStr}</Text>
                                </View>
                            </View>
                            <View style={styles.blockBot}>
                                <View style={styles.detail}>
                                    <Text numberOfLines={1}
                                          style={styles.detailRight}>{item.lastMsg.contentSession}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableHighlight>
        )
    }

}

const styles = StyleSheet.create({
    item: {
        flex: 1,
        height: 70,
        overflow: 'hidden',
        position: 'relative',
        paddingVertical: 5,
        paddingHorizontal: 5,
        borderBottomWidth: 1,
        borderStyle: 'solid',
        borderBottomColor: '#eeeeee',
    },
    unread: {
        position: 'absolute',
        zIndex: 10,
        top: 3,
        left: 62,
        width: 6,
        height: 6,
        borderRadius: 3,
        backgroundColor: 'red',
    },
    msg: {
        flexDirection: 'row',
    },
    picBox: {
        width: 60,
        height: 60,
    },
    pic: {
        flex: 1,
        width: 60,
    },
    block: {
        paddingLeft: 5,
        flex: 1,
        height: 60,

    },
    blockTop: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    title: {
        flex: 1,
        height: 20,
        flexDirection: 'row',

    },
    date: {
        width: 60,
        paddingLeft: 10,
    },
    blockBot: {
        flex: 1,
        justifyContent: 'center',
    },
    detail: {
        flexDirection: 'row',
    },
    titleText: {
        color: '#333333',
        flex: 1,
        fontSize: 18,
    },
    dateText: {
        color: '#bbbbbb',
        fontSize: 14,
    },
    detailLeft: {
        color: '#999999',
        fontSize: 14,
    },
    detailRight: {
        flex: 1,
        fontSize: 14,
        color: '#999999',
    }

})

ListItem.propTypes = {
    groupInfo: PropTypes.object.isRequired,
}