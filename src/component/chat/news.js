/**
 * Created by crcb on 2017/12/15.
 */

import React from 'react'
import {View, Text, StyleSheet, Image, TouchableOpacity, Modal, Alert} from 'react-native'
import {PropTypes} from 'prop-types'

import ImageZoom from 'react-native-image-pan-zoom'
import {CachedImage} from 'react-native-img-cache'

import Param from '../../constant/styleParam'
import ImgJson from '../../pages/chat/chatMoreData';
import {Carousel, Toast} from 'antd-mobile/lib'
import Sound from 'react-native-sound'
import RNFetchBlob from "react-native-fetch-blob";
import HttpParam from "../../constant/HttpParam";

const betWid = 50;
const txtsize = 18;
const bqImglist = ImgJson.bqRequireUrl[0];
const borderColorL = "#d4d4d4";
const borderbgL = "white";
const borderColorR = "#9fcf60";
const borderbgR = "#a2e65a";

export default class news extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            visibleflag: true,
            Img_Width: Param.screenWidth,
            Img_Height: Param.screenHeight,
            Img_index: null,     //当前选中的图片
        }
    }

    static propTypes = {
        dataSource: PropTypes.object,
        userInfo: PropTypes.object,
        _index: PropTypes.number,
        imgs: PropTypes.array,
    }

    render() {
        const {content, creator, type} = this.props.dataSource;
        const {yanId, nickName, realName} = this.props.userInfo;
        const {imgs} = this.props;
        const {visibleflag, Img_Width, Img_Height, Img_index} = this.state;
        return (
            <View>
                {
                    this.props._index === 0 ? <View style={{height: 20}}><Text>{null}</Text></View> : null
                }
                {
                    type === -1 ? this._systemMsg(content) :
                        creator === yanId ? this._rightChat() : this._leftChat()
                }
                {/* 预览图片开始 手指缩放和点击区域关闭不能同时生效，故，用两个Vew*/}
                {
                    visibleflag && Img_index!=null ?
                        <Modal
                            visible={visibleflag}   //是否显示
                            animationType={"slide"}  //动画类型
                            transparent={false}  //是否透明
                            onRequestClose={()=>{
                                this.setState({
                                    visibleflag:!visibleflag
                                })
                            }}
                        >
                            <View style={{
                                backgroundColor: "rgba(0,0,0,0.8)",
                                height: Param.screenHeight,
                                flexDirection: "column"
                            }}>
                                <View style={{flex: 1, alignItems: "center", justifyContent: "center"}}>
                                    <ImageZoom cropWidth={Param.screenWidth}
                                               cropHeight={Param.screenHeight}
                                               imageWidth={Img_Width}
                                               imageHeight={Img_Height}>
                                        <CachedImage source={{uri: Img_index, cache: "force-cache"}}
                                                     resizeMode={"contain"}
                                                     style={{width: Img_Width, height: Img_Height}}/>
                                    </ImageZoom>
                                </View>
                            </View>
                            <TouchableOpacity
                                style={{backgroundColor: "white", position: "absolute", right: 40, top: 40}}
                                onPress={() => this.setState({visibleflag: false, Img_index: null})}>
                                <Text style={{padding: 10}}>关闭</Text>
                            </TouchableOpacity>
                        </Modal> : null
                }

                {/* 预览图片结束 */}
            </View>
        )
    }

    //系统消息
    _systemMsg(content) {
        return <View style={styles._systemStyle}>
            <Text style={{color: "white", fontSize: txtsize - 2}}>{content}</Text>
        </View>
    }

    //左边界面
    _leftChat() {
        const {content, type, urlSmall, urlBig, soundPath,len,prdName,prdRate,prdLastTime,prdStartMoney} = this.props.dataSource;
        // console.log("content L = ",urlSmall,urlBig);
        if (type === 0 && content) {
            return <View style={[styles._conf, styles._flex]}>
                <View style={[styles._fontL, styles._flex]}>
                    <CachedImage source={require("../../assets/Image/addressbook/head.png")} style={styles._HeadImg}/>
                </View>
                <View style={[styles._fontM, styles._contentL, styles._flex]}>
                    <View style={styles._box1L}><Text>{null}</Text></View>
                    <View style={styles._box2L}><Text>{null}</Text></View>
                    <View style={[styles._flex, styles._contentV, styles._boxL, {alignItems: "center",}]}>
                        {this.Msg0()}
                    </View>
                </View>
                <View style={styles._fontR}>

                </View>
            </View>
        } else if (type === 1 && urlSmall && urlBig) {      //图片
            return <View style={[styles._conf, styles._flex]}>
                <View style={[styles._fontL, styles._flex]}>
                    <CachedImage source={require("../../assets/Image/addressbook/head.png")} style={styles._HeadImg}/>
                </View>
                <View style={[styles._fontM, styles._contentL, styles._flex]}>
                    <TouchableOpacity style={[styles._flex, styles._boxL, {alignItems: "center",}]} activeOpacity={0.7}
                                      onPress={() => this.bigPicture(urlBig)}>
                        <CachedImage source={{uri: urlSmall}} style={styles.ImageView}/>
                    </TouchableOpacity>
                </View>
                <View style={styles._fontR}>

                </View>
            </View>
        } else if (type === 2 && content&&len) {      //语音
            return <View style={[styles._conf, styles._flex]}>
                <View style={[styles._fontL, styles._flex]}>
                    <CachedImage source={require("../../assets/Image/addressbook/head.png")} style={styles._HeadImg}/>
                </View>
                <View style={[styles._fontM, styles._contentL, styles._flex]}>
                    <View style={styles._box1L}><Text>{null}</Text></View>
                    <View style={styles._box2L}><Text>{null}</Text></View>
                    <TouchableOpacity onPress={() => this._listen(content)}
                                      style={[styles._flex, styles._contentV, styles._boxL, {alignItems: "center",}]}>
                        <CachedImage source={require("../../assets/Image/chatImg/rightyy3.png")}
                                     style={{width: 16, height: 16,transform: [{rotate: "180deg"}]}}/>
                        <Text>（{len}s）</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles._fontR}>

                </View>
            </View>
        } else if (type === 5 && content){             //推荐产品
            return <View style={[styles._conf, styles._flex]}>
                <View style={[styles._fontL, styles._flex]}>
                    <CachedImage source={require("../../assets/Image/addressbook/head.png")} style={styles._HeadImg}/>
                </View>
                <View style={[styles._fontM, styles._contentL, styles._flex]}>
                    <View style={[styles._boxL,styles._tjPrd]}>
                        <View style={[styles._flex]}>
                            <View style={[{padding:10,alignItems:"center"},styles._flex]}>
                                <CachedImage source={require("../../assets/Image/chatImg/TJLogo.png")} style={styles._HeadImg} />
                            </View>
                            <View style={{flex:1,padding:10}}>
                                <Text style={{fontSize:22}}>{prdName}</Text>
                                <Text>利率:  <Text style={{fontSize:30,color:"red"}}>{prdRate}</Text></Text>
                                <Text>到期时间: {prdLastTime}</Text>
                                <Text><Text style={{fontSize:30,color:"red"}}>{prdStartMoney}</Text>元起投</Text>
                            </View>
                        </View>
                        <View style={{borderTopWidth:1,borderTopColor:borderColorL,padding:10,}}>
                            <Text style={{color:"#777"}}>{content}</Text>
                        </View>
                    </View>
                </View>
                <View style={styles._fontR}>

                </View>
            </View>
        }
        else {
            return <View style={[styles._conf, styles._flex]}>
                <View style={[styles._fontL, styles._flex]}>
                    <CachedImage source={require("../../assets/Image/addressbook/head.png")} style={styles._HeadImg}/>
                </View>
                <View style={[styles._fontM, styles._contentL, styles._flex]}>
                    <View style={styles._box1L}><Text>{null}</Text></View>
                    <View style={styles._box2L}><Text>{null}</Text></View>
                    <View style={[styles._flex, styles._contentV, styles._boxL, {alignItems: "center",}]}>
                        <Text style={{color: "red"}}>{content}</Text>
                    </View>
                </View>
                <View style={styles._fontR}>

                </View>
            </View>
        }
    }

    //右边界面
    _rightChat() {
        const {content, type, urlSmall, urlBig,len,prdName,prdRate,prdLastTime,prdStartMoney} = this.props.dataSource;
        // console.log("content R = ", urlSmall, urlBig);
        if (type === 0 && content) {
            return <View style={[styles._conf, styles._flex]}>
                <View style={styles._fontL}>

                </View>
                <View style={[styles._fontM, styles._contentR, styles._flex]}>
                    <View style={[styles._flex, styles._contentV, styles._boxR, {alignItems: "center",}]}>
                        {this.Msg0()}
                    </View>
                    <View style={styles._box1R}><Text>{null}</Text></View>
                    <View style={styles._box2R}><Text>{null}</Text></View>
                </View>
                <View style={[styles._fontR, styles._flex]}>
                    <CachedImage source={require("../../assets/Image/customer/head.jpg")} style={styles._HeadImg}/>
                </View>
            </View>
        } else if (type === 1 && urlSmall && urlBig) {      //图片消息
            return <View style={[styles._conf, styles._flex]}>
                <View style={styles._fontL}>

                </View>
                <View style={[styles._fontM, styles._contentR, styles._flex]}>
                    <TouchableOpacity style={[styles._flex, styles._boxR, {alignItems: "center",}]} activeOpacity={0.7}
                                      onPress={() => this.bigPicture(urlBig)}>
                        <CachedImage source={{uri: urlSmall}} style={styles.ImageView}/>
                    </TouchableOpacity>
                </View>
                <View style={[styles._fontR, styles._flex]}>
                    <CachedImage source={require("../../assets/Image/customer/head.jpg")} style={styles._HeadImg}/>
                </View>
            </View>
        } else if (type === 2 && content&&len) {      //语音消息，content是语音的路径
            return <View style={[styles._conf, styles._flex]}>
                <View style={styles._fontL}>

                </View>
                <View style={[styles._fontM, styles._contentR, styles._flex]}>
                    <TouchableOpacity
                        style={[styles._flex, styles._boxR, {alignItems: "center", paddingLeft: 10, paddingRight: 10}]}
                        activeOpacity={0.7}
                        onPress={() => this._listen(content)}>
                        <Text>（{len}s）</Text>
                        <CachedImage source={require("../../assets/Image/chatImg/rightyy3.png")}
                                     style={{width: 16, height: 16}}/>
                    </TouchableOpacity>
                </View>
                <View style={[styles._fontR, styles._flex]}>
                    <CachedImage source={require("../../assets/Image/customer/head.jpg")} style={styles._HeadImg}/>
                </View>
            </View>
        } else if (type === 5 && content) {      //推荐消息
            return <View style={[styles._conf, styles._flex]}>
                <View style={styles._fontL}>

                </View>
                <View style={[styles._fontM, styles._contentR, styles._flex]}>
                    <View style={[styles._boxL,styles._tjPrd]}>
                        <View style={[styles._flex]}>
                            <View style={[{padding:10,alignItems:"center"},styles._flex]}>
                                <CachedImage source={require("../../assets/Image/chatImg/TJLogo.png")} style={styles._HeadImg} />
                            </View>
                            <View style={{flex:1,padding:10}}>
                                <Text style={{fontSize:22}}>{prdName}</Text>
                                <Text>利率:  <Text style={{fontSize:30,color:"red"}}>{prdRate}</Text></Text>
                                <Text>到期时间: {prdLastTime}</Text>
                                <Text><Text style={{fontSize:30,color:"red"}}>{prdStartMoney}</Text>元起投</Text>
                            </View>
                        </View>
                        <View style={{borderTopWidth:1,borderTopColor:borderColorL,padding:10,}}>
                            <Text style={{color:"#777"}}>{content}</Text>
                        </View>
                    </View>
                </View>
                <View style={[styles._fontR, styles._flex]}>
                    <CachedImage source={require("../../assets/Image/customer/head.jpg")} style={styles._HeadImg}/>
                </View>
            </View>
        }
        else {            //其他消息类型
            return <View style={[styles._conf, styles._flex]}>
                <View style={styles._fontL}>

                </View>
                <View style={[styles._fontM, styles._contentR, styles._flex]}>
                    <View style={[styles._flex, styles._contentV, styles._boxR, {alignItems: "center"}]}>
                        <Text style={{color: "red"}}>{content}</Text>
                    </View>
                    <View style={styles._box1R}><Text>{null}</Text></View>
                    <View style={styles._box2R}><Text>{null}</Text></View>
                </View>
                <View style={[styles._fontR, styles._flex]}>
                    <CachedImage source={require("../../assets/Image/customer/head.jpg")} style={styles._HeadImg}/>
                </View>
            </View>
        }
    }

    //表情文字消息视图
    Msg0() {
        const {content, type} = this.props.dataSource;
        // console.log("content = ",content," type = ",type)
        return (
            content ?
                content.map((item, index) => {
                    if (item.bo) {
                        return <CachedImage key={index} source={bqImglist[item.content - 1]} style={{width:txtsize,height:txtsize}}/>
                    } else {
                        return <Text key={index} style={[styles._contentfont]}>{item.content}</Text>
                    }
                }) : null
        )
    }

    //放大图片
    bigPicture(url) {
        let _w = Param.screenWidth, _h = Param.screenWidth;
        let _i = this.props.imgs;
        let _url = url.substring(url.lastIndexOf("="),url.length);

        for (let i in _i) {
            let _name = _i[i];
            _name = _name.substring(_name.lastIndexOf("="),_name.length);
            if (_name == _url) {
                Image.getSize(url, (width, height) => {
                    _w = width;
                    _h = height;
                });
                this.setState({visibleflag: true, Img_Width: _w, Img_Height: _h, Img_index: url})
                console.log("click pic ");
                break;
            }
        }

    }

    //听语音
    _listen(url) {
        Toast.loading("正在播放语音",0)
        // console.log("url =",url)
        //下载下来
        // let dirs = RNFetchBlob.fs.dirs;
        // RNFetchBlob.config({
        //     fileCache:true,
        //     appendExt:"mp3",
        // }).fetch('get',url,{}).then((res)=>{
        //     console.log("保存到文件 = ",res.path());
        //     let s = new Sound(res.path(),'',
        //         (e)=>{
        //         if(e){
        //             console.log("语音听取失败error = ",e);
        //             res.flush();
        //             Toast.fail("网络不通",1);
        //             return ;
        //         }
        //         s.play((success)=>{
        //             if (success){
        //                 Toast.success("播放成功",1);
        //             }else{
        //                 Toast.fail("文件损坏，请重试",1);
        //             }
        //             res.flush();
        //         });
        //     });
        // })
        let s = new Sound({uri: url}, (e) => {
            if (e) {
                // console.log("语音听取失败error = ", e);
                Toast.fail("网络不通", 1);
                return;
            }
            s.play((success) => {
                if (success) {
                    Toast.success("播放成功", 1);
                } else {
                    Toast.fail("文件损坏，请重试", 1);
                }
                Toast.hide();
            });
        });
    }

}

const styles = StyleSheet.create({
    _flex: {
        display: "flex",
        flexDirection: "row",
    },
    _center: {
        justifyContent: "center",
        alignItems: "center",
    },
    _conf: {
        marginBottom: 20,
    },
    _fontL: {
        width: betWid,
    },
    _fontM: {
        flex: 1,
    },
    _fontR: {
        width: betWid,
    },
    _HeadImg: {
        width: 50,
        height: 50,
    },
    _contentfont: {
        fontSize: txtsize,
    },
    _contentL: {
        justifyContent: "flex-start",
        paddingLeft: 10,
        paddingRight: 10,
    },
    _contentR: {
        justifyContent: "flex-end",
        paddingLeft: 10,
        paddingRight: 10,
    },
    _contentV: {
        padding: 10,
        borderRadius: 5,
        paddingLeft: 15,
        paddingRight: 15,
        flexWrap: "wrap",
    },
    _systemStyle: {
        marginTop: 20,
        marginBottom: 20,
        marginLeft: 30,
        marginRight: 30,
        padding: 10,
        paddingTop: 5,
        paddingBottom: 5,
        backgroundColor: "#cecece",
        borderRadius: 10,
    },
    _msg0View: {
        display: "flex",
        flexDirection: "row",
        flexWrap: "wrap",
    },

    _boxL: {
        backgroundColor: "white",
        borderWidth: 1,
        borderColor: borderColorL,
        borderRadius: 5,
        position: "relative",
        zIndex: 1,
    },
    _box1L: {
        position: "absolute",
        // left:-5,
        left: 6,
        top: 18,
        width: 0,
        height: 0,
        borderTopWidth: 5,
        borderTopColor: "transparent",
        borderBottomWidth: 5,
        borderBottomColor: "transparent",
        borderRightWidth: 5,
        borderRightColor: borderbgL,
        zIndex: 3,
    },
    _box2L: {
        position: "absolute",
        left: 5,
        top: 17,
        width: 0,
        height: 0,
        borderTopWidth: 6,
        borderTopColor: "transparent",
        borderBottomWidth: 6,
        borderBottomColor: "transparent",
        borderRightWidth: 6,
        borderRightColor: borderColorL,
    },
    _boxR: {
        backgroundColor: borderbgR,
        borderWidth: 1,
        borderColor: borderColorR,
        borderRadius: 5,
        position: "relative",
        zIndex: 1,
    },
    _box1R: {
        position: "absolute",
        right: 6,
        top: 18,
        width: 0,
        height: 0,
        borderTopWidth: 5,
        borderTopColor: "transparent",
        borderBottomWidth: 5,
        borderBottomColor: "transparent",
        borderRightWidth: 5,
        borderRightColor: borderbgR,
        zIndex: 3,
        transform: [{rotate: "180deg"}]
    },
    _box2R: {
        position: "absolute",
        right: 5,
        top: 17,
        width: 0,
        height: 0,
        borderTopWidth: 6,
        borderTopColor: "transparent",
        borderBottomWidth: 6,
        borderBottomColor: "transparent",
        borderRightWidth: 6,
        borderRightColor: borderColorR,
        transform: [{rotate: "180deg"}]
    },
    ImageView: {
        width: 200,
        height: 200,
        borderWidth: 1,
        borderColor: "#000",
    },
    _tjPrd:{
        flex:1,
    }

});