'use strict';

import React, {Component} from 'react';
import {ViewPropTypes,Text, View, StyleSheet, TextInput, Platform, TouchableOpacity, TouchableNativeFeedback} from 'react-native';
import Button from './Button';

import theme from '../constant/styleParam';
// import colors from '../constants/colors';

import formSheet from './tcomb/helpmformstyle';

// var t = require('tcomb-form-native');
var _ = require('lodash');
import PropTypes from 'prop-types'

export default class SmsButton extends Component{

    constructor(props) {
        super(props);
        this.state = {
            timerCount: this.props.timerCount || 90,
            timerTitle: this.props.timerTitle || '获取验证码',
            counting: false,
            selfEnable: true,
            hasError:false,
            checkCode:null,
        };
        this._shouldStartCountting = this._shouldStartCountting.bind(this)
        this._countDownAction = this._countDownAction.bind(this)
    }

    static propTypes = {
        label: PropTypes.string.isRequired,
        maxLength: PropTypes.number.isRequired,
        timerCount: PropTypes.number.isRequired,
        onClick:PropTypes.func.isRequired,
        disableColor:PropTypes.string.isRequired,
        style: ViewPropTypes.style,
        textOnChange:PropTypes.func
    };

    static defaultProps = {
        maxLength:6,
        timerCount:30,
        disableColor:theme.lightGray
    };

    _countDownAction(){
        const codeTime = this.state.timerCount;
        this.interval = setInterval(() =>{
            const timer = this.state.timerCount - 1
            if(timer===0){
                this.interval&&clearInterval(this.interval);
                this.setState({
                    timerCount: codeTime,
                    timerTitle: this.props.timerTitle || '获取验证码',
                    counting: false,
                    selfEnable: true
                })
            }else{
                this.setState({
                    timerCount:timer,
                    timerTitle: `${timer}秒后重新获取`,
                })
            }
        },1000)
    }

    _shouldStartCountting(shouldStart){
        if (this.state.counting) {return}
        if (shouldStart) {
            this._countDownAction()
            this.setState({counting: true,selfEnable:false})
        }else{
            this.setState({selfEnable:true})
        }
    }

    componentWillUnmount(){
        clearInterval(this.interval)
    }

    getValue(){
        // if(!this.state.checkCode || this.state.checkCode.length < 5){
        //     this.setState({
        //         hasError: true
        //     })
        // }else{
        //     this.setState({
        //         hasError: false
        //     })
        // }
        return this.state.checkCode;
    }

    render(){
        const {onClick,style,textStyle,enable,disableColor,} = this.props
        const {counting,timerTitle,selfEnable,hasError} = this.state

        //let stylesheet = _.cloneDeep(t.form.Form.stylesheet);
        var formGroupStyle = formSheet.formGroup.normal;
        var controlLabelStyle = formSheet.controlLabel.normal;
        var textboxStyle = formSheet.textbox.normal;
        var textboxViewStyle = formSheet.checkCodeView.normal;

        if (hasError) {
            formGroupStyle = formSheet.formGroup.error;
            controlLabelStyle = formSheet.controlLabel.error;
            textboxStyle = formSheet.textbox.error;
            textboxViewStyle = formSheet.checkCodeView.error;
        }

        var label = <Text style={controlLabelStyle}>{this.props.label}</Text>;

        return (
            <View style={[formGroupStyle,{marginTop:10,marginBottom:10}]}>
                <View style={[textboxViewStyle]}>
                    <TextInput
                        style={textboxStyle}
                        placeholder='请输入验证码'
                        keyboardType="numeric"
                        maxLength={this.props.maxLength}
                        onChangeText={(text) => {
                            this.props.textOnChange(text);
                            this.setState({
                                checkCode: text
                            })
                        }}
                    />
                </View>
                <Button style={{width:110}}
                        text={timerTitle}
                        backgroundColor={'#c69c6d'}
                        underlineColorAndroid={"transparent"}
                        fontColor={(!counting && enable && selfEnable) ? theme.white : disableColor}
                        btnEnable={this.state.selfEnable}
                        onPress={()=>{
                            if (!counting && enable && selfEnable) {
                                this.setState({selfEnable:false})
                                this.props.onClick(this._shouldStartCountting)
                            };
                        }}/>
            </View>
        );
    }
}
