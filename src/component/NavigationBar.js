import React, {Component} from 'react';
import {Platform, View, Text, StatusBar, TouchableOpacity, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Param from '../constant/globalParam'
import sysStyle from '../constant/styleParam'
import {PropTypes} from 'prop-types'
//var StyleSheet = require('react-native-debug-stylesheet');

export default class NavigationBar extends Component{

    static propTypes = {
        title: PropTypes.string,
        hasBottomLine: PropTypes.bool,
        leftBtnIcon: PropTypes.string,
        leftBtnText: PropTypes.string,
        leftBtnPress: PropTypes.func,
        rightBtnIcon: PropTypes.string,
        rightBtnText: PropTypes.string,
        rightBtnPress: PropTypes.func,
    };

    static defaultProps = {

    };

    constructor(props) {
        super(props);
    }

    render(){
        const {title, hasBottomLine, leftBtnIcon, leftBtnText, leftBtnPress, rightBtnIcon, rightBtnText, rightBtnPress} = this.props;
        return (
            <View style={[styles.container,{borderBottomWidth:(hasBottomLine?1:0)}]}>
                <StatusBar translucent={true} backgroundColor={sysStyle.navigatorBgColor} />
                <View style={[styles.toolbar]}>
                    <View style={styles.fixedCell}>
                        {(leftBtnIcon || leftBtnText) ?
                            <Button icon={leftBtnIcon} text={leftBtnText} onPress={leftBtnPress} size={23} />
                            :
                            null
                        }
                    </View>
                    <View style={styles.centerCell}>
                        <Text style={styles.title}>{title}</Text>
                    </View>
                    <View style={styles.fixedCell}>
                        {(rightBtnIcon || rightBtnText) ?
                            <Button icon={rightBtnIcon} text={rightBtnText} onPress={rightBtnPress} size={18}  />
                            :
                            null
                        }
                    </View>
                </View>
            </View>
        );
    }
}

class Button extends Component{
    static propTypes = {
        icon: PropTypes.string,
        text: PropTypes.string,
        onPress: PropTypes.func,
        size:PropTypes.number,
    };

    render(){
        var icon = null;
        if(this.props.icon) {
            icon = this.props.icon;
        }
        return(
            <TouchableOpacity
                onPress={this.props.onPress}
                activeOpacity={0.8}>
                <View style={styles.btn}>
                    {icon ?
                        <Icon name={icon} color="#FFFFFF" size={this.props.size}/>
                        :
                        <Text style={styles.btnText}>{this.props.text}</Text>
                    }
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        height:Param.NavBarHeight,
        backgroundColor: sysStyle.navigatorBgColor,
    },
    toolbar: {
        height:Param.NavBarHeight,
        flexDirection: 'row',
        alignItems:"flex-end"

    },
    fixedCell: {
        width: 100,
        height:Param.NavBarHeight-20,
        flexDirection:'row',
        justifyContent:"center",
        alignItems:"center",
    },
    centerCell: {
        flex: 1,
        height: Param.NavBarHeight-20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    title: {
        fontSize: 18,
        color: "#fff"
    },
    btn: {
        justifyContent:'center',
        alignItems:'center',
        flex: 1,
        width: 50,
        height:Param.NavBarHeight-20,
    },
    btnText: {
        color: "#fff",
        fontSize: 18
    }
});
