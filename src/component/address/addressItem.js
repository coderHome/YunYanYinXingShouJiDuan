import React, {Component} from 'react';
import { ListView, Text, Image, View, TouchableHighlight, FlatList, StyleSheet} from 'react-native';
import {Checkbox} from 'antd-mobile/lib'
import PropTypes from 'prop-types';
// var StyleSheet = require('react-native-debug-stylesheet');

const PIC_URL = require('../../assets/Image/customer/head.jpg');
export default class addressItem extends Component {
    static propTypes = {
        userInfo: PropTypes.object,
        isShowCheckBox: PropTypes.bool,
        headImg:PropTypes.string,
        text:PropTypes.string,
        onClickItem:PropTypes.func,
        isCheck:PropTypes.bool,
        imageType:PropTypes.string,
    }

    static defaultProps = {
        isShowCheckBox:false,
        headImg:'',
        isCheck:false,
        imageType:'0'
    };

    constructor(props) {
        super(props);
        this.state={
            isCheck:this.props.isCheck
        }
    }

    //初始化
    componentDidMount(){

    }

    //销毁
    componentWillUnmount(){

    }

    _handlerClick(){
        console.log("item被点击");
        if(this.props.isShowCheckBox){
            this.setState({
                isCheck:!this.state.isCheck
            },()=>{
                this.props.onClickItem(this.state.isCheck);
            });

        }else{
            this.props.onClickItem();
        }
    }


    render(){

        // let data = this.props.userInfo;
        // console.log(data);
        const {isShowCheckBox,headImg,text,onClickItem,imageType} = this.props;
        let pic_url;
        switch (imageType){
            case '0':
                pic_url=require('../../assets/Image/addressbook/head.png');
                break;
            case '1':
                pic_url=require('../../assets/Image/addressbook/groupIcon.png');
                break;
            case '2':
                pic_url=require('../../assets/Image/addressbook/tagIcon.png');
                break;
            default:
                pic_url=require('../../assets/Image/addressbook/head.png');
        }

        if(headImg !='undefined'){
            if(headImg.indexOf('https')>=0){
                pic_url={uri:data.head}
            }
        }

        return (
            <TouchableHighlight underlayColor="rgba(0,0,0,0.2)" onPress={()=>{this._handlerClick()}}>
                <View style={styles.item}>
                    {isShowCheckBox?
                    <Checkbox
                        style={styles.checkBoxStyle}
                        checked={this.state.isCheck}
                    />:null}
                    <View style={styles.msg}>
                        <View style={styles.picBox}>
                            <Image style={styles.pic} source={pic_url}/>
                        </View>
                        <View style={styles.block}>
                            <View style={styles.blockTop}>
                                <View style={styles.title}>
                                    <Text numberOfLines={1} style={styles.titleText}>{text}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableHighlight>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    line: {
        flex: 1,
        height: 1,
        flexDirection: 'row',
        backgroundColor: "#cccccc",
    },
    item: {
        flex: 1,
        height: 60,
        overflow: 'hidden',
        position: 'relative',
        paddingVertical: 5,
        paddingHorizontal:5,
        borderBottomWidth:1,
        borderStyle:'solid',
        borderBottomColor:'#cccccc',
        flexDirection:'row',
    },
    unread: {
        position: 'absolute',
        zIndex:10,
        top: 2,
        left: 52,
        width: 6,
        height: 6,
        borderRadius: 3,
        backgroundColor: 'red',
    },
    menu: {
        width: 50,
        height: 50,
        flexDirection: 'row',
        position: 'absolute',
        right: -50,
        top: 0,
    },
    delete: {
        flex: 1,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'red'
    },
    menuText: {
        color: '#fff',
        fontSize: 16,
    },
    msg: {
        flexDirection: 'row',
        flex:1
    },
    picBox: {
        width: 50,
        height: 50,
    },
    pic: {
        flex: 1,
        width: 50,
    },
    block: {
        paddingLeft:5,
        flex: 1,
        height: 50,

    },
    blockTop: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    title:{
        flex:1,
        height:20,
        flexDirection:'row',

    },
    date: {
        width: 60,
        paddingLeft:10,
    },
    blockBot: {
        flex: 1,
        paddingTop:10,
    },
    detail: {
        flexDirection: 'row',
    },
    titleText: {
        flex:1,
        fontSize: 16,
    },
    dateText: {
        fontSize: 12,
    },
    detailLeft: {
        fontSize: 12,
    },
    detailRight: {
        flex:1,
        fontSize: 12,
    },
    sectionHeadertText:{
        fontSize:15,
    },
    sectionHeaderContainer:{
        height:30,
        paddingLeft:5,
        justifyContent: 'center',
        backgroundColor:'#eee'
    },
    checkBoxStyle:{
        marginLeft:10,
        marginRight:10,
    }

})
