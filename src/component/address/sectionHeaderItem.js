import React, {Component} from 'react';
import { ListView, Text, Image, View, TouchableHighlight, FlatList, StyleSheet} from 'react-native';
import PropTypes from 'prop-types';
// var StyleSheet = require('react-native-debug-stylesheet');

export default class sectionHeaderItem extends Component {
    static propTypes = {
        text:PropTypes.string,
    }

    static defaultProps = {
        text:''
    };

    constructor(props) {
        super(props);
        this.state={
            isCheck:this.props.isCheck
        }
    }

    //初始化
    componentDidMount(){

    }

    //销毁
    componentWillUnmount(){

    }

    render(){
        const {text} = this.props;
        return (
            <View style={styles.sectionHeaderContainer}>
                <Text style={styles.sectionHeadertText}>{text}</Text>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    sectionHeadertText:{
        fontSize:15,
    },
    sectionHeaderContainer:{
        height:30,
        paddingLeft:5,
        justifyContent: 'center',
        backgroundColor:'#eee'
    }
})
