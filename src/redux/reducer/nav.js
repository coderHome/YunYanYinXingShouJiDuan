/**
 * Created by crcb on 2017/12/1.
 */

import { AppNavigator } from '../../AppNavigator/AppNavigators';
import { NavigationActions } from 'react-navigation';


const firstAction = AppNavigator.router.getActionForPathAndParams('HomePage');
const tempNavState = AppNavigator.router.getStateForAction(firstAction);
const initialNavState = AppNavigator.router.getStateForAction(
    tempNavState
);


export default function nav(state = initialNavState, action) {
    let nextState;
    switch (action.type) {
        case 'Back':
            nextState = AppNavigator.router.getStateForAction(
                NavigationActions.back({key:action.key}),
                state
            );
            break;
        case 'Navigation':
            nextState = AppNavigator.router.getStateForAction(
                NavigationActions.navigate({
                    routeName: action.url,
                    params:action.params,
                }),
                state
            );
            break;
        case 'Reset':
            nextState = AppNavigator.router.getStateForAction(
                NavigationActions.reset({
                    index:0,
                    key:null,
                    actions:[NavigationActions.navigate({
                        routeName:action.url,
                        params:action.params,
                    })]
                }),
                state
            );
            break;
        default:
            nextState = AppNavigator.router.getStateForAction(action, state);
            break;
    }
    return nextState || state;
}
