/**
 * Created by crcb on 2017/11/30.
 * 用户reducer
 */
import * as Types from '../../constant/Types';
import mock from '../../assets/mock/group.json';
//存放用户信息
const initialAuthState = {
    userLogin:false,   //登录标识
    members:mock.members,
    userInfo:null,
    otherUserInfo:null,
};

export default function todos(state = initialAuthState, action) {
    switch (action.type) {
        case Types.LOGIN_SUCCESS:
            // console.log("登录结果为   登录成功");
            return {
                ...state,
                userLogin:true,
                userInfo:action.data,
                // loginToken:action.loginToken
            }
        case Types.GETUSERINFO:
            return {
                ...state,
                otherUserInfo:action.otherUserInfo
            }
        case Types.LOGIN_FAIL:
            // console.log("登录结果为   登录失败");
            return {
                ...state,
                userLogin:false
            }
        case Types.FETCH_USERINFO_DATA_SUCCESS:
            // console.log("用户信息获取成功");
            let obj = state.userInfo;
            obj.headIcon = action.userInfo.headIcon;
            obj.nickName = action.userInfo.nickName;
            obj.realName = action.userInfo.realName;
            obj.role = action.userInfo.role;
            obj.spell = action.userInfo.spell;
            obj.yanId = action.userInfo.yanId;
            return Object.assign({},state,{
                userInfo:obj
            })
        case Types.FETCH_MEMBERS_DATA_SUCCESS:
            return Object.assign({},state,{
                members:action.members
            })
        case Types.LOGOUT_SUCCESS:
            return {
                ...state,
                userLogin:false,
                // userInfo : null
            }
        default:
            return state
    }
}