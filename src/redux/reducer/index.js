/**
 * Created by sm on 2017/11/29.
 * 定义了reducers总控制器
 */
import { combineReducers } from 'redux'
import userReducer from './userReducer'
import nav from './nav'
import chatReducer from './chatReducer'
import sessionsReducer from './sessionsReducer'
import addressBookReducer from './addressBookReducer'
import managerReducer from './managerReducer'
import socketReducer from './socketReducer'


//所有的reducer在这里注册
export default combineReducers({
    nav,userReducer,chatReducer,sessionsReducer,addressBookReducer,managerReducer,socketReducer
});
