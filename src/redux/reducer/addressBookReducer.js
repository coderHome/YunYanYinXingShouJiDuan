/**
 * Created by crcb on 2017/11/30.
 * 用户reducer
 */
import * as Types from '../../constant/Types';
import mock from '../../assets/mock/addressBook.json';
//存放用户信息
const initialAuthState = {
    addressBookMembers:[],//处理后通讯录所有成员
    addressBookMembersByGroupId:[],//处理后通讯录  群组成员
    addressBookGroupMembers:[],//通讯录成员按群组分类
    addressBookTagMembers:[],//标签页展示成员
    addressBookVersion:null,//通讯录版本号
    originMember:[],//未处理的成员信息
    groupDatas:this.defaultGroupData,//群组信息
    tagList:[],//标签列表
    customerInfo:[],//客户信息
    addressBookMembersByTag:[],//处理后通讯录  标签成员
    defaultGroupData:[{
        id:'0',
        name:'未分组'
    }]
};

export default function todos(state = initialAuthState, action) {
    switch (action.type) {
        case Types.FETCH_ADDRESSBOOK_DATA_SUCCESS:
            // console.log("通讯录获取成功");
            return {
                ...state,
                addressBookMembers:action.addressBookMembers,
                originMember:action.originMember,
            }
        case Types.FETCH_MEMBERS_DATA_SUCCESS:
            return {
                ...state,
                members:action.members
            }
        case Types.FETCH_GROUP_DATA_SUCCESS:
            return {
                ...state,
                groupDatas:state.defaultGroupData.concat(action.groupDatas)
            }
        case Types.CREATE_GROUP_DATA_SUCCESS:
            return {
                ...state,
                groupDatas:state.groupDatas.concat(action.groupData)
            }
        case Types.DELETE_GROUP_DATA_SUCCESS:
            return {
                ...state,
                groupDatas:action.groupDatas
            }
        case Types.FETCH_ADDRESSBOOK_BY_GROUP:
            return {
                ...state,
                addressBookMembersByGroupId:action.addressBookMembersByGroupId
            }
        case Types.FETCH_SORT_MEMBERS_DATA:
            return {
                ...state,
                addressBookGroupMembers:action.addressBookGroupMembers
            }
        case Types.FETCH_CUSTOMER_INFO_DATA:
            return {
                ...state,
                customerInfo:action.customerInfo
            }
        case Types.FETCH_TAG_LIST:
            return {
                ...state,
                tagList:action.tagList
            }
        case Types.FETCH_ADDRESSBOOK_BY_TAG:
            return {
                ...state,
                addressBookMembersByTag:action.addressBookMembersByTag
            }
        case Types.UPDATE_CUSTOMER_INFO:
            return {
                ...state,
                customerInfo:action.customerInfo
            }
        default:
            return state
    }
}