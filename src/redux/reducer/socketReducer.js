import * as Types from '../../constant/Types';
//存放用户信息
const initialAuthState = {
    connectFlag:true,           //连接标识
};

export default function todos(state = initialAuthState, action) {
    switch (action.type) {
        case Types.DISCONNECT:
            // console.log("主动断开连接 ");
            return {
                ...state,
                connectFlag:false
            };
        case Types.CONNECT_SUCCESS:
            // console.log("主动连接 ");
            return {
                ...state,
                connectFlag:true
            };
        default:
            return state
    }
}