import * as TYPES from '../../constant/Types';
const defaultData={
    sessions:[],
    members:[],
    title:'云燕客服'
}

export  default function sessionsReducer(state=defaultData,action){
    switch (action.type){
        case TYPES.FETCH_SESSIONS_DATA_SUCCESS :{
            return {
                ...state,
                sessions:action.sessions
            }
        }
        case TYPES.UPDATE_SESSIONS_DATA :{
            return {
                ...state,
                sessions:action.sessions
            }
        }
        case TYPES.FETCH_MEMBERS_DATA_SUCCESS :{
            return {
                ...state,
                members:action.members
            }
        }
        case TYPES.UPDATE_MEMBERS_DATA :{
            return {
                ...state,
                members:action.members
            }
        }
        case TYPES.UPDATE_SESSION_TITLE:{
            return {
                ...state,
                title:action.title
            }
        }
        default :
            return state
    }

}