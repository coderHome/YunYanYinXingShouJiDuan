/**
 * Created by crcb on 2017/12/19.
 */
import * as Types from '../../constant/Types';

const initialAuthState = {
    chatRecords: [],    //聊天记录
    groupId:-1,         //当前的groupId
    sessionId:null,     //当前聊天页面的客户编号
    imgs:[],            //当前聊天页面的图片缓存
};

export default function chatReducers(state = initialAuthState, action) {
    switch (action.type) {
        case Types.GET_CHAT_LIST_SUCCESS:
            return {
                ...state,
                chatRecords: action.data,
                groupId:action.groupId,
                imgs:action.imgs,
            };
        case Types.GET_CHAT_REFRESH_LIST_SUCCESS:
            return {
                ...state,
                chatRecords: action.data.concat(state.chatRecords),
                imgs:action.imgs.concat(state.imgs),
            };
        case Types.SERVER_MSG:
            return {
                ...state,
                chatRecords: state.chatRecords.concat(action.data),
                imgs:state.imgs.concat(action.imgs),
            };
        case Types.RESET_GROUPID:
            return {
                ...state,
                groupId: -1,
                sessionId:null,
            };
        case Types.SET_SESSIONID_SUCCESS:
            return {
                ...state,
                sessionId: action.sessionId,
            };

        default:
            return state
    }
}
