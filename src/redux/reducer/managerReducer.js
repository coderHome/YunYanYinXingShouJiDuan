/**
 * Created by crcb on 2017/11/30.
 * 用户reducer
 */
import * as Types from '../../constant/Types';
import mock from '../../assets/mock/manager.json';
const initialAuthState = {
    manageActivities:[],
    isRefresh:false
};

export default function todos(state = initialAuthState, action) {
    switch (action.type) {
        case Types.FETCH_MANAGER_SUCCESS:
            return {
                ...state,
                manageActivities:action.data,
                isRefresh:false
            }
        case Types.FETCH_MARKETINGS_LIST_REQUEST:
            return {
                ...state,
                isRefresh:true
            }
        case Types.FETCH_MANAGER_FAIL:
            return {
                ...state,
                isRefresh:false
            }
        default:
            return state
    }
}