/**
 * Created by sm on 2017/11/29.
 * 使用redux-thunk解决redux异步数据问题
 */
import {createStore,applyMiddleware} from 'redux'
import thunk from 'redux-thunk'
import reducers from '../reducer/index'
export const store = createStore(
    reducers,
    applyMiddleware(thunk)
);