/**
 * Created by crcb on 2017/11/30.
 * 用户操作
 */
import * as Types from '../../constant/Types'
import Param from '../../constant/HttpParam'
import {Alert} from 'react-native'
import * as Io from '../../socketIO/index'
import HttpFetch from '../../util/HttpFetch'
import Api from '../../constant/APIParam'
import Error from '../../util/ErrorUtil'
import sessionsUtil from '../../util/sessionsUtil';
import {Toast} from 'antd-mobile/lib'
import * as pSub from '../../PubSub/index'
import userJson from '../../assets/mock/userInfo.json'
import Config from '../../constant/globalParam'
// import storageUtil from '../../util/storageUtil'
import * as userDao from '../../dao/userDao'
import JPushModule from 'jpush-react-native';
import * as commonUtils from '../../util/commonUtils'
/**
 * 用户登录接口
 * @param obj
 * @param callback
 * @returns {function(*)}
 */
export function loginuser(obj,callback) {
    console.log("obj",obj);
    global.loginType = '2';
    let pushId = commonUtils.getPushId();
    return (dispatch)=>{
        HttpFetch(Api.loginApi,{
            jobNo:obj.username,
            password:obj.password,
            pushId:pushId,
            deviceType:Param.deviceType,
            deviceVersion:Param.deviceVersion,
            deviceId:Param.deviceId,
            secretType:Param.secretType
        },"正在登陆").then((userinfo)=>{
            // Alert.alert("http登录成功");
            if(Config.DEBUG){
                userinfo = userJson.userInfo;
            }
            console.log("loginsucc userinfo:",userinfo);

            //更新state
            dispatch(loginSuccess(userinfo));


            global.userInfo = userinfo;
            global.loginToken = userinfo.loginToken;
            //设置推送别名
            JPushModule.setAlias(pushId,(result)=>{
                console.log("别名设置成功 pushId",pushId);
                console.log("别名设置成功",result);
            })
            Io.socketConnect(obj.username,obj.password);
            callback();
        }).catch((err)=>{
            Error(err);
            dispatch(loginFail());
        })
        // callback();
    }
}

/**
 * 获取验证码
 * @param phone
 * @param callback
 * @returns {function(*)}
 */
export function getSMS(phone,callback){
    return (dispatch)=>{
        HttpFetch(Api.getSMSApi,{
            phone:phone,
            type:'1'
        },"正在发送").then((json)=>{
            console.log("getSMS :"+json);
        }).catch((err)=>{
            Error(err);
            // Toast.info("短信发送失败，请重试！");
        })
    }
}
/**
 * 设备绑定
 * @param phone
 * @param code
 * @param callback
 * @returns {function(*)}
 */
export function bindDevice(phone,code,callback){
    return (dispatch)=>{
        if(false){
            callback();
        }else{
            HttpFetch(Api.checkMsgCode,{
                phone:phone,
                code:code,
            }).then((json)=>{
                console.log("getbindDevice :"+json);
                callback();
            }).catch((err)=>{
                if(Config.DEBUG){
                    callback();
                }
                Error(err);
            })
        }
        // callback();
    }
}

/**
 * token登录
 * @param succCallback
 * @param errorCallback
 * @returns {function(*)}
 */
export function tokenLogin(succCallback,errorCallback){
    global.loginType = '1';
    return (dispatch)=>{
        const loginToken = global.loginToken;
        const userInfo = global.userInfo;
        HttpFetch(Api.loginToken,{
            loginToken:loginToken,
            jobNo:userInfo.userno?userInfo.userno:'',
            deviceType:Param.deviceType,
            deviceVersion:Param.deviceVersion,
            deviceId:Param.deviceId,
            secretType:Param.secretType
        },null,false).then((userinfo)=>{
            if(Config.DEBUG){
                userinfo = userJson.userInfo;
            }
            console.log("tokenloginsucc userinfo:",userinfo);

            //更新state
            dispatch(loginSuccess(userinfo));
            //更新global
            global.userInfo = userinfo;

            let pushId = commonUtils.getPushId();
            //设置推送别名
            JPushModule.setAlias(pushId,(result)=>{
                console.log("别名设置成功 pushId",pushId);
                console.log("别名设置成功",result);
            })
            createSingleToken(global.loginToken,(tmpToken)=>{
                //消息服务器登录
                Io.socketConnectToken(tmpToken);
                // succCallback();
            },()=>{
                errorCallback();
            })
        }).catch((err)=>{
            Error(err);
            console.log('token登录异常===',err);
            errorCallback();
        })
    }
}

/**
 * 获取消息服务器临时token
 * @param loginToken
 * @param callback
 * @param errorCallback
 */
export function createSingleToken(loginToken,callback,errorCallback){
    HttpFetch(Api.createSingleToken,{
        loginToken:loginToken,
    },null,false).then((data)=>{
        console.log("createSingleToken:",data);
        callback(data);
    }).catch((err)=>{
        console.log('获取消息服务器临时token异常===',err);
        errorCallback();
    })
}

export function logout(userInfo,callback,errorCallback){
    return (dispatch)=>{
        HttpFetch(Api.logout,{
            // loginToken:loginToken,
            deviceType:Param.deviceType,
            deviceVersion:Param.deviceVersion,
            deviceId:Param.deviceId,
            pushId:Param.deviceId
        }).then((data)=>{
            console.log("logout:",data);
            dispatch(logoutSuccess());
            //断开连接
            Io.closeConnection();
            clearAllData();

            callback();
        }).catch((err)=>{
            Error(err);
            console.log('logout异常===',err);
            dispatch(logoutSuccess());
            clearAllData();
            callback();
        })
    }
}


/**
 * 清除用户本地缓存数据
 */
function clearUserInfo(){
    userDao.clearAllData();
}

/**
 * 清空所有缓存
 */
function clearAllData() {
    global.storageUtil.clearAllData();
}

export const fetchUserInfo=userInfo=>{
    return {
        type:Types.FETCH_USERINFO_DATA_SUCCESS,
        userInfo,
    }
}
export const fetchMembers=members=>{
    return {
        type:Types.FETCH_MEMBERS_DATA_SUCCESS,
        members,
    }
}

function logoutSuccess(){
    return {
        type:Types.LOGOUT_SUCCESS
    }
}

function loginSuccess(user) {
    return {
        type:Types.LOGIN_SUCCESS,
        data:user,
    }
}
function loginFail() {
    return {
        type:Types.LOGIN_FAIL
    }
}

function getSessions(sessions) {
    return {
        type:Types.FETCH_SESSIONS_DATA_SUCCESS,
        sessions:sessions
    }
}