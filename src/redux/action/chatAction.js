/**
 * Created by crcb on 2017/12/19.
 */
import * as Type from '../../constant/Types'
import * as PackagingMsg from '../../util/PackagingMsg'
import * as chatStorage from '../../dao/chatStorageDao'
import {Toast} from "antd-mobile/lib"
import Param from '../../constant/globalParam'
import HttpParam from "../../constant/HttpParam";
import ApiParam from "../../constant/APIParam";
import HttpUtil from '../../util/HttpFetch';
import fileHttpUtil from '../../util/fileHttpFetch'
import chatReducer from "../reducer/chatReducer";
import userReducer from "../reducer/userReducer";
import Error from '../../util/ErrorUtil'

/**
 * 获取聊天记录
 * @param groupId
 * @param numbers
 * @returns {function(*)}
 */
export function getChatRecords(groupId, numbers) {
    return (dispatch) => {
        //获取聊天记录的缓存
        chatStorage.getRecords(groupId,numbers,(data)=>{
            PackagingMsg.packagingMoreMsg(data,function (msgs) {
                setTimeout(function () {
                    // console.log("聊天记录 = ",msgs);
                    catchPic(msgs,(imgs)=>{
                        dispatch(getChatReducers(msgs,imgs,groupId));
                    })
                },1000)
            })

        })
    }
}

/**
 * 获取群组信息
 * @param groupId
 */
export function getGroupInfo(groupId) {
    return (dispatch)=>{
        global.socket.emit("client_create_session",{groupId:groupId},function (data) {
            // console.log("获取群组信息成功 = ",data);
            HttpUtil(ApiParam.getCustomerDetailsById,{
                yanId:data.sessionId
            }).then((res)=>{
                dispatch(setUserInfo(res))
            }).catch((error)=>{
                Error("用户信息获取失败");
            })
        })
    }
}


/**
 * 下拉刷新，获取聊天记录
 * @param groupId
 * @param startId
 * @returns {function(*=)}
 */
export function refreshChatRecords(groupId, startId) {
    return (dispatch,getState) => {
        let list = getState().chatReducer.chatRecords;
        let yuLen = list.length+Param.chatRefreshNum;
        chatStorage.getNowRecordNum(groupId,(numbers)=>{
            //如果缓存中的数据>=当前聊天页面显示的数据+预刷新的数据条数，则取缓存中的数据
            if (numbers.length>=yuLen){
                // console.log("缓存数据充足，读取缓存");
                // console.log("缓存 = ",numbers)
                let nowRecords = numbers.filter((item,index)=>{if(index<yuLen&&index>=list.length)return true});
                PackagingMsg.packagingMoreMsg(nowRecords.reverse(),function (msgs) {
                    setTimeout(function () {
                        catchPic(msgs,(imgs)=>{
                            dispatch(refreshChatReducers(msgs,imgs));
                        })
                    },100)

                })
            }else{
                // console.log("缓存数据不足，发送接口");
                global.socket.emit("client_msg_record", {
                    groupId: groupId,
                    startId: startId,
                    countMsg: Param.chatRefreshNum,
                }, function (data) {
                    PackagingMsg.packagingMoreMsg(data,function (msgs) {
                        setTimeout(function () {
                            catchPic(msgs,(imgs)=>{
                                dispatch(refreshChatReducers(msgs,imgs));
                            })
                        },1000)

                    })
                    chatStorage.addMoreRecords(data,list.length);
                })
            }
        })
    }
}

/**
 * 发送消息
 * @param groupId
 * @param msg
 * @returns {function(*)}
 */
export function sendMsg(groupId,msg) {
    return (dispatch,getState)=>{
        let user = getState().userReducer.userInfo;
        global.socket.emit("client_msg_normal",{
            groupId:groupId,
            content:msg,
            ctm:user.yanId
        },function (data) {
            // console.log("消息已送达",data);
        })
    }
}

/**
 * 发送营销消息
 * @param data
 * @param callback
 * @returns {function(*, *)}
 */
export function sendType5Msg(data,callback){
    return (dispatch,getState)=>{
        let groupId = getState().chatReducer.groupId;
        let userId = getState().userReducer.userInfo.yanId;
        // console.log("发送营销消息data = ",data);
        if(groupId!==-1){
            global.socket.emit("client_msg_recommend",{
                groupId:groupId,
                busType:data.prdType,
                busId:data.prdId,
                ctm:userId,
            },function (data) {
                // console.log("推荐消息消息已送达",data);
                callback();
            })
        }else{
            Error("客户不存在");
        }

    }
}

/**
 * 封装一条聊天记录
 * @param msg
 * @returns {function(*)}
 */
export function server_msg(msg) {
    // console.log("封装一条聊天记录 = ",msg)
    return (dispatch, getState) => {
        let _r = getState().chatReducer.groupId === msg.groupId;
        _r ? PackagingMsg.packagingOneMsg(msg, function (_msg) {
            let _time = _msg.type === 5?1000:10;
            setTimeout(function () {
                catchPic(_msg,(imgs)=>{
                    dispatch(parseMsg(_msg,imgs));
                })
            },_time)
        }) : null;
        let obj = {
            msg: msg,
            unread: !_r,
        }
        //告诉session消息是否已读
        global.PubSub.emit(Type.SESSIONS_CHANGE, obj);
        //把消息加入到缓存
        chatStorage.addOneRecords(msg)
    }
}


/**
 * 重置GroupID并且告诉服务器这个群组消息都是已读的
 * @returns {{type}}
 */
export function resetGroupId(groupId) {
    return (dispatch)=>{
        global.socket.emit("client_unmark_session_unread",{groupId:groupId});
        return {
            type: Type.RESET_GROUPID,
        }
    }

}

/**
 * 清空单个group的缓存
 * @param groupId
 */
export function clearOneRecord(groupId) {
    global.storageUtil.clearKeyOneId("records",groupId)
    Toast.success("清除成功",1);
}

/**
 * 清空所有聊天记录
 */
export function clearAllRecords() {
    global.storageUtil.clearAllData();
    Toast.success("清除成功",1);
}

/**
 * 发送图片消息
 * @param filePath
 * @param groupId
 */
export function sendPic(filePath,groupId) {
    // console.log("图片路径 = ",filePath);
    return (dispatch,getState)=>{
        let yanId = getState().userReducer.userInfo.yanId;
        let obj = {
            uri:filePath,
            name:filePath.substring(filePath.lastIndexOf('/')+1),
            creator:yanId,
            msgType:"pic",
            mime:"image/jpeg"
        };
        fileHttpUtil(ApiParam.uploadPic2,obj).then((data)=>{
            global.socket.emit("client_msg_picture",{
                groupId:groupId,
                picId:data,
                ctm:yanId,
            },function (id) {
                // console.log("图片消息发送成功 = ",id);
            });
        }).catch((error)=>{
            // console.log("文件上传失败, = ",error);
            Error(error);
        });
    }
}

/**
 * 上传语音
 * @param filePath
 * @param groupId
 * @param len
 * @returns {function(*, *)}
 */
export function sendyuyinMsg(filePath,len,groupId) {
    // console.log("filePath = ",filePath,"len = ",len,"groupId = ",groupId);
    return (dispatch,getState)=>{
        let yanId = getState().userReducer.userInfo.yanId;
        let obj = {
            uri:filePath,
            mime:"audio/x-aac",
            creator:yanId,
            name:filePath.substring(filePath.lastIndexOf('/')+1),
            msgType:"yuyin"
        };
        fileHttpUtil(ApiParam.uploadPic2,obj).then((data)=>{
            global.socket.emit("client_msg_sound",{
                groupId:groupId,
                soundId:data[0],
                length:len,
                ctm:yanId,
            },function (id) {
                // console.log("语音消息发送成功 = ",id);
            });
        }).catch((error)=>{
            // console.log("文件上传失败, = ",error);
            Error(error);
        });
    }
}

function catchPic(msgs,callback) {
    let piclists = [];
    msgs.map((item,index)=>{
        if(item.urlBig){
            piclists.push(item.urlBig);
        }
    });
    // console.log("图片缓存  = ",piclists);
    callback(piclists);
}

function getChatReducers(data,imgs,groupId) {
    return {
        type: Type.GET_CHAT_LIST_SUCCESS,
        data: data,
        imgs:imgs,
        groupId:groupId,
    }
}

function refreshChatReducers(msgs,imgs) {
    return {
        type: Type.GET_CHAT_REFRESH_LIST_SUCCESS,
        data: msgs,
        imgs:imgs
    }
}

function parseMsg(oneMsg,imgs) {
    return {
        type: Type.SERVER_MSG,
        data: oneMsg,
        imgs:imgs
    }
}

function setGroupSessionId(sessionId) {
    return {
        type:Type.SET_SESSIONID_SUCCESS,
        sessionId:sessionId
    }
}

function setUserInfo(otherUserInfo) {
    return {
        type:Type.GETUSERINFO,
        otherUserInfo:otherUserInfo
    }
}