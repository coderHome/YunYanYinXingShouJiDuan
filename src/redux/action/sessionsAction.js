import * as TYPES from '../../constant/Types';
import * as $U from '../../util/sessionsUtil';
import SessionsDao from '../../dao/sessionsData';

const sessionsDao = new SessionsDao();


const fetchSessionsSuccess = (sessions) => ({
    type: TYPES.FETCH_SESSIONS_DATA_SUCCESS,
    sessions,
})

const updateSessions = (sessions) => ({
    type: TYPES.UPDATE_SESSIONS_DATA,
    sessions,
})

const fetchMembersSuccess = (members) => ({
    type: TYPES.FETCH_MEMBERS_DATA_SUCCESS,
    members,
})

const updateMembers = (members) => ({
    type: TYPES.UPDATE_MEMBERS_DATA,
    members,
});

const updateTitleSuccess= (title) =>({
    type:TYPES.UPDATE_SESSION_TITLE,
    title
})
export function updateTitle(title){
    return (dispatch)=>{
        dispatch(updateTitleSuccess(title));
    }
}

function transMbs(payload, callback) {
    let {dispatch, mbs, getState} = payload;
    let context = getState();
    let prevMbs = context.sessionsReducer.members.concat();
    mbs.forEach((item) => {
        prevMbs[item.yanId] = item;
    })

    dispatch(updateMembers(prevMbs));
    if (typeof callback == 'function') {
        callback();
    }
}


function getSessionsSuccess(payload) {
    let {dispatch, sessions, getState} = payload;
    console.log('getSessionsSuccess',sessions)
    // 检测通讯录缓存是否含有会话成员
    $U.checkLocalMember(sessions,function(localMbs){
        if(localMbs.length>0){
            // 新增成员
            transMbs({dispatch, getState, mbs:localMbs}, function () {
                //更新公共数据

                sessionsDao.setItem({localSessions: sessions});
                dispatch(fetchSessionsSuccess(sessions));
            });
        };
        // 检测 本地reducer 的member是否含有会话成员
        let ids = $U.getMissMbs(sessions);

        if (ids.length > 0) {
            global.socket.emit('client_get_acc_simple', {yanIds: ids}, function (mbs) {
                console.log("client_get_acc_simple",mbs);
                    transMbs({dispatch, getState, mbs}, function () {
                        //更新公共数据

                        sessionsDao.setItem({localSessions: sessions});
                        dispatch(fetchSessionsSuccess(sessions));
                    });
            });
        } else {
            //更新公共数据

            sessionsDao.setItem({localSessions: sessions});
            dispatch(fetchSessionsSuccess(sessions));
        }
    });

}

export function deleteSession(sessionId) {
    return (dispatch, getState) => {
        sessionsDao.getItem().then(data => {
            try {
                let localSessions = data.localSessions;
                let curSessionIndex = localSessions.findIndex(item => {
                    return item && item.sessionId == sessionId;
                });
                localSessions.splice(curSessionIndex, 1);
                let payload = {
                    dispatch,
                    getState,
                    sessions: localSessions,
                }
                getSessionsSuccess(payload);
            //    通知服务器
                global.socket.emit('client_delete_session', {groupId: sessionId}, function (data) {
                    console.log('client_delete_session',data);
                });

            } catch (e) {
                console.log(e);
            }
        }).catch(error => {
            console.log('没有缓存');
        })
    }
}

export function changeSessionUnread(sessionId) {
    return (dispatch, getState) => {
        sessionsDao.getItem().then(data => {
            try {
                let localSessions = data.localSessions;
                localSessions.forEach(item => {
                    if (item && item.sessionId == sessionId) {
                        item.unread = false;
                    }
                });
                let payload = {
                    dispatch,
                    getState,
                    sessions: localSessions,
                }
                getSessionsSuccess(payload);
            } catch (e) {
                console.log(e)
            }

        }).catch(error => {
            console.log('没有缓存');
        })
    }
}


// 登陆成功后调用
export function fetchSessions(sessions) {
    return (dispatch, getState) => {
        // sessionsDao.remove().then(data => {
        //     console.log(data)
        // }).catch(error => {
        //     console.log(error)
        // });
        // 添加未读标记
        sessions.forEach(item => {
            item.unread = true;
        })
        sessionsDao.getItem().then((data) => {
            try {
                let localSessions = data.localSessions;
                console.log('存在缓存', localSessions);
                // 更新本地缓存
                sessions.forEach((group) => {
                    let curSessionIndex = localSessions.findIndex(item => {
                        return item && item.sessionId == group.sessionId
                    });
                    if (curSessionIndex >= 0) {
                        localSessions.splice(curSessionIndex, 1);
                    }
                    localSessions.unshift(group);
                });
                //更新公共数据
                let payload = {
                    dispatch,
                    getState,
                    sessions: localSessions,
                }
                getSessionsSuccess(payload);
            } catch (e) {
                console.log(e)
            }

        }).catch((error) => {
            console.log('没有缓存');
            //更新公共数据
            let payload = {
                dispatch,
                getState,
                sessions,
            }
            getSessionsSuccess(payload);
        });

    }
}


// 消息推送时调用
export function changeSessions(message) {
    return (dispatch, getState) => {
        let newSession = {};
        // 判断是否已读

        sessionsDao.getItem().then((data) => {
            try {
                console.log('存在缓存');
                let localSessions = data.localSessions;
                let curSessionIndex = localSessions.findIndex(item => {
                    return item && item.sessionId == message.msg.groupId;
                });

                // 判断会话是否存在
                if (curSessionIndex >= 0) {

                    localSessions[curSessionIndex].lastMsg = message.msg;
                    localSessions[curSessionIndex].unread = message.unread;

                    let group = $U.deepCopy(localSessions[curSessionIndex]);
                    localSessions.splice(curSessionIndex, 1);
                    localSessions.unshift(group);
                    let payload = {
                        dispatch,
                        getState,
                        sessions: localSessions,
                    }
                    getSessionsSuccess(payload);
                } else {
                    global.socket.emit('client_create_session', {groupId: message.msg.groupId}, function (session) {

                        // 点对点时新建会话
                            newSession.sessionId = message.msg.groupId;
                            newSession.lastMsg = session.lastMsg;
                            newSession.cusId = session.cusId;
                            newSession.unread = message.unread;
                            localSessions.unshift(newSession);
                            let payload = {
                                dispatch,
                                getState,
                                sessions: localSessions,
                            }
                            getSessionsSuccess(payload);
                    })
                }
            } catch (e) {
                console.log(e)
            }

        }).catch((error) => {
            console.log('没有缓存');
            global.socket.emit('client_create_session', {groupId: message.msg.groupId}, function (session) {

                    newSession.sessionId = message.msg.groupId;
                    newSession.lastMsg = session.lastMsg;
                    newSession.cusId = session.cusId;
                    newSession.unread = message.unread;
                    let payload = {
                        dispatch,
                        getState,
                        sessions: [newSession],
                    }
                    getSessionsSuccess(payload);

            })

        })
    }
}

export function changeMembers(mbs, callback) {

    return (dispatch, getState) => {
        transMbs({dispatch, getState, mbs}, callback);
    }
}