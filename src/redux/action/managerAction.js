/**
 * Created by crcb on 2017/11/30.
 * 营销列表
 */
import * as Types from '../../constant/Types'
import Param from '../../constant/HttpParam'
import {Alert} from 'react-native'
import * as Io from '../../socketIO/index'
import HttpFetch from '../../util/HttpFetch'
import Api from '../../constant/APIParam'
import Error from '../../util/ErrorUtil'
import sessionsUtil from '../../util/sessionsUtil';
import {Toast} from 'antd-mobile/lib'
import * as pSub from '../../PubSub/index'
import userJson from '../../assets/mock/userInfo.json'
import Config from '../../constant/globalParam'
// import storageUtil from '../../util/storageUtil'
import * as userDao from '../../dao/userDao'
import mock from '../../assets/mock/manager.json';
/**
 * 用户登录接口
 * @param obj
 * @param callback
 * @returns {function(*)}
 */
export function getManageList(callback) {

    return (dispatch, getState)=> {
        dispatch(fetchListRequest());
        HttpFetch(Api.getMarketings, {},"",false).then((json)=> {
            console.log("marketings :" , json);
            dispatch(fetchManageSuccess(json.marketings));
            callback();
        }).catch((err)=> {
            dispatch(fetchManageFail());
            Error(err);
        })
    }
}

function fetchManageSuccess(data) {
    return {
        type: Types.FETCH_MANAGER_SUCCESS,
        data: data,
    }
}
function fetchManageFail() {
    return {
        type: Types.FETCH_MANAGER_FAIL,
    }
}
function fetchListRequest() {
    return {
        type: Types.FETCH_MARKETINGS_LIST_REQUEST,
    }
}