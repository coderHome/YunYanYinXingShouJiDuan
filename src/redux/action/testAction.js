import {Alert} from "react-native";
import HttpFetch from "../../util/HttpFetch";
import Api from "../../constant/APIParam";
import Error from "../../util/ErrorUtil";
import DeviceInfo from "react-native-device-info";

export function login(name, pwd) {
    // console.log("name = ",name," ！！！！！！ pwd = ",pwd);
    return (dispatch) => {
        HttpFetch(Api.testApi, {
            name: name,
            pwd: pwd,
        },"测试发送数据").then((data) => {
            console.log("获取数据成功 data = ", data);
        }).catch((error) => {
            console.log("获取数据失败 err = ", error);
            Error(error);
        })
    }

}

export function testDeviceInfo(){
    return (dispatch) => {
        console.log("device uuid:"+DeviceInfo.getUniqueID());

        console.log("Device Manufacturer", DeviceInfo.getManufacturer());  // e.g. Apple

        console.log("Device Model", DeviceInfo.getModel());  // e.g. iPhone 6

        console.log("Device ID", DeviceInfo.getDeviceId());  // e.g. iPhone7,2 / or the board on Android e.g. goldfish

        console.log("Device Name", DeviceInfo.getSystemName());  // e.g. iPhone OS

        console.log("Device Version", DeviceInfo.getSystemVersion());  // e.g. 9.0

        console.log("Bundle Id", DeviceInfo.getBundleId());  // e.g. com.learnium.mobile

        console.log("Build Number", DeviceInfo.getBuildNumber());  // e.g. 89

        console.log("App Version", DeviceInfo.getVersion());  // e.g. 1.1.0

        console.log("App Version (Readable)", DeviceInfo.getReadableVersion());  // e.g. 1.1.0.89

        console.log("Device Name", DeviceInfo.getDeviceName());  // e.g. Becca's iPhone 6

        console.log("User Agent", DeviceInfo.getUserAgent()); // e.g. Dalvik/2.1.0 (Linux; U; Android 5.1; Google Nexus 4 - 5.1.0 - API 22 - 768x1280 Build/LMY47D)

        console.log("Device Locale", DeviceInfo.getDeviceLocale()); // e.g en-US

        console.log("Device Country", DeviceInfo.getDeviceCountry()); // e.g US

    }

}