/**
 * Created by crcb on 2017/11/30.
 * 用户操作
 */
import * as Types from '../../constant/Types'
import Param from '../../constant/HttpParam'
import {Alert} from 'react-native'
import * as Io from '../../socketIO/index'
import HttpFetch from '../../util/HttpFetch'
import Api from '../../constant/APIParam'
import Error from '../../util/ErrorUtil'
import * as sessionsUtil from '../../util/sessionsUtil';
import {Toast,Modal} from 'antd-mobile/lib'
import * as pSub from '../../PubSub/index'
import userJson from '../../assets/mock/userInfo.json'
import Config from '../../constant/globalParam'
// import storageUtil from '../../util/storageUtil'
import * as addressBookDao from '../../dao/addressBookDao'
import addressbookJson from '../../assets/mock/addressBook.json'
import * as addressBookUtil from '../../util/addressBookUtil'

export function getAddressBook(remoteVersion,callback,errorCallback) {
    return (dispatch)=>{
        addressBookDao.getAddressBookVersion((data)=>{
            if(data){
                console.log("本地通讯录版本号：",data);
                if(data!=remoteVersion){//当前通讯录版本号不等于服务器版本号，需要更新
                    // console.log("version缓存获取成功，当前通讯录版本号小于服务器版本号，从服务端获取数据");
                    fetchAddressBook((json)=>{
                        const addressBookMember = addressBookUtil.categryFriends(json.list);
                        //更新state
                        dispatch(fetchAddressBookInfo(addressBookMember,json));
                        //数据缓存
                        addressBookDao.saveOriginMembers(json);
                        addressBookDao.saveAddressBookVersion(json.version);
                    },errorCallback);//获取通讯录
                }else{//否则无需更新,从本地获取通讯录
                    //获取本地通讯录信息
                    addressBookDao.getOriginMembers((json)=>{
                        if(json){
                            console.log("获取本地通讯录信息",json);
                            const addressBookMember = addressBookUtil.categryFriends(json.list);
                            //更新state
                            dispatch(fetchAddressBookInfo(addressBookMember,json));
                            callback();
                        }else{
                            errorCallback();
                        }
                    })
                }
            }else{
                console.log("version缓存获取失败，从服务端获取数据");
                fetchAddressBook((json)=>{
                    const addressBookMember = addressBookUtil.categryFriends(json.list);
                    //更新state
                    dispatch(fetchAddressBookInfo(addressBookMember,json));
                    //数据缓存
                    addressBookDao.saveOriginMembers(json);
                    addressBookDao.saveAddressBookVersion(json.version);
                },errorCallback);//获取通讯录
            }
        })
        // HttpFetch(Api.getCustomers,{
        //     test:'432'
        // }).then((json)=>{
        //     if(Config.DEBUG){
        //         json = addressbookJson.addressBookTest;
        //     }
        //     console.log("getaddressbook========",json);
        //     const addressBookMember = addressBookUtil.categryFriends(json.list);
        //     //更新state
        //     dispatch(fetchAddressBookInfo(addressBookMember,json));
        //     //数据缓存
        //     addressBookDao.saveOriginMembers(json);
        //     addressBookDao.saveAddressBookVersion(json.version);
        //     callback();
        // }).catch((err)=>{
        //     Error(err);
        //     let json;
        //     // console.log("getaddressbook==error======",error);
        //     if(Config.DEBUG){
        //         json = addressbookJson.addressBookTest.list;
        //         const addressBookMembers = addressBookUtil.categryFriends(json);
        //         //更新state
        //         dispatch(fetchAddressBookInfo(addressBookMembers,json));
        //         //数据缓存
        //         callback();
        //     }
        // })
    }
}

function fetchAddressBook(callback,errorCallback){
    HttpFetch(Api.getCustomers,{
    }).then((json)=>{
        if(Config.DEBUG){
            json = addressbookJson.addressBookTest;
        }
        console.log("getaddressbook========",json);
        callback(json);
    }).catch((err)=>{
        errorCallback();
    })
}

/**
 * 获取本地通讯录信息
 */
export function getLocalAddressBook(callback,errorCallback){
    return (dispatch)=>{
        //获取本地通讯录信息
        addressBookDao.getOriginMembers((json)=>{
            if(json){
                const addressBookMember = addressBookUtil.categryFriends(json);
                //更新state
                dispatch(fetchAddressBookInfo(addressBookMember,json));
                callback(json);
            }else{
                errorCallback();
            }
        })
    }
}

/**
 * 获取通讯录版本号
 * @param callback
 * @returns {function(*)}
 */
export function getAddressBookVersion(callback,errorCallback) {
    return (dispatch)=>{
        HttpFetch(Api.getCustVersion,{
        }).then((json)=>{
            if(Config.DEBUG){
                json = addressbookJson.addressVersion;
            }
            const currVersion = json;
            callback(currVersion);
        }).catch((err)=>{
            let json;
            errorCallback();
        })
    }
}


/**
 * 获取群组
 * @param callback
 * @returns {function(*)}
 */
export function getGroupInfo(callback,errorCallback) {
    return (dispatch,getState)=>{
        HttpFetch(Api.getGroups,{
        }).then((json)=>{
            if(Config.DEBUG){
                json = addressbookJson.addressVersion;
            }
            dispatch(fetchGroupDataSuccess(json));
            let payload = {
                dispatch,
                getState,
            }
            let groups = getState().addressBookReducer.groupDatas.concat();
            sortMembersByGroup(groups,payload);
            callback();
        }).catch((err)=>{
            let json;
            Error(err);
            errorCallback();
        })
    }
}

/**
 * 创建群组
 * @param callback
 * @returns {function(*)}
 */
export function createGroup(name,callback,errorCallback) {
    return (dispatch,getState)=>{
        HttpFetch(Api.createGroup,{
            name:name
        }).then((json)=>{
            if(Config.DEBUG){
                json = addressbookJson.addressVersion;
            }
            const groupData = {
                id:json,
                name:name
            }
            dispatch(createGroupSuccess(groupData));

            callback();
        }).catch((err)=>{
            Error(err);
            errorCallback();
        })
    }
}
/**
 * 删除群组
 * @param callback
 * @returns {function(*)}
 */
export function deleteGroup(id,callback,errorCallback) {
    return (dispatch , getState) => {
        HttpFetch(Api.deleteGroup,{
            id:id
        }).then((json)=>{
            let groupDatas = getState().addressBookReducer.groupDatas.concat();
            let deleteItemIndex = groupDatas.findIndex(item => {
                return item && item.id == id;
            });
            groupDatas.splice(deleteItemIndex,1);
            dispatch(deleteGroupSuccess(groupDatas));
            // callback();
        }).catch((err)=>{
            Error(err);
            // errorCallback();
        })
    }
}


/**
 * 根据群组编号获取该群组成员
 * @param groupId
 * @returns {function(*, *)}
 */
export function getMembersByGroupId(groupId){
    return ( dispatch , getState )=>{
        let members = getState().addressBookReducer.originMember.list.concat();
        let groups = getState().addressBookReducer.groupDatas.concat();
        let results = addressBookUtil.sortAllMembersByGroup(groups,members);
        // let results = getState().addressBookReducer.addressBookGroupMembers;

        let index = results.findIndex((item)=>{
            return item.id == groupId
        });
        let membersByGroupResutlts = addressBookUtil.categryFriends(results[index].data);
        dispatch(fetchMembersByGroup(membersByGroupResutlts));
    }
}

/**
 * 根据yanid获取客户详细信息
 * @param yanId
 * @param callback
 * @param errorCallback
 * @returns {function(*, *)}
 */
export function getCustomerDetailsById(yanId,callback,errorCallback) {
    return (dispatch , getState) => {
        HttpFetch(Api.getCustomerDetailsById,{
            yanId:yanId
        }).then((json)=>{
            // console.log("getCustomerDetailsById=====return",json);
            let groups = getState().addressBookReducer.groupDatas;
            let customer = json;
            customer = refreshCustomerInfo(customer,groups);
            dispatch(fetchCustomerInfo(customer));
            callback(customer);
        }).catch((err)=>{
            Error(err);
            // errorCallback();
            // console.log('客户信息获取失败==',err);
        })
    }
}




/**
 * 将客户移出分组
 * @param yanId
 * @param callback
 * @param errorCallback
 * @returns {function(*, *)}
 */
export function doLeaveGroup(yanId,groupId,callback,errorCallback) {
    return (dispatch , getState) => {
        HttpFetch(Api.leaveGroup,{
            yanId:yanId,
            groupId:groupId
        }).then((json)=>{
            let tmpCustomer =  addressBookUtil.copyValue(getState().addressBookReducer.customerInfo);
            let groups = getState().addressBookReducer.groupDatas;
            tmpCustomer.groupId = null;
            let customer = refreshCustomerInfo(tmpCustomer,groups);
            dispatch(fetchCustomerInfo(customer));

            let originMember = getState().addressBookReducer.originMember;
            let index = originMember.list.findIndex((item)=>{
                return item.yanId == yanId;
            });
            if(index>=0){
                originMember.list[index].groupId = null;
            }
            dispatch(refreshOriginMember(originMember));
            Toast.success('操作成功',1);
            callback();
        }).catch((err)=>{
            Error(err);
            // errorCallback();
            // console.log('客户信息获取失败==',err);
        })
    }
}

/**
 * 将客户加入分组
 * @param yanId
 * @param groupId
 * @param callback
 * @param errorCallback
 * @returns {function(*, *)}
 */
export function doJoinGroup(yanId,groupId,callback,errorCallback) {
    return (dispatch , getState) => {
        HttpFetch(Api.joinGroup,{
            yanId:yanId,
            groupId:groupId
        }).then((json)=>{
            let tmpCustomer = addressBookUtil.copyValue(getState().addressBookReducer.customerInfo);
            let groups = getState().addressBookReducer.groupDatas;
            let originMember = getState().addressBookReducer.originMember;
            tmpCustomer.groupId = groupId;
            tmpCustomer.tips = [{
                name:"test",
                id:"2"
            }];
            let customer = refreshCustomerInfo(tmpCustomer,groups);

            dispatch(fetchCustomerInfo(customer));

            let index = originMember.list.findIndex((item)=>{
                return item.yanId == yanId;
            });
            if(index>=0){
                originMember.list[index].groupId = groupId;
            }
            dispatch(refreshOriginMember(originMember));
            Toast.success('操作成功',1);
        }).catch((err)=>{
            Error(err);
            // errorCallback();
            // console.log('客户信息获取失败==',err);
        })
    }
}


/**
 * 更新用户群组信息
 * @param customer
 * @returns {*}
 */
export function refreshCustomerInfo(customer,groups){
    let group = groups.find((item)=>{
        return item.id == customer.groupId
    });
    if(group){
        customer.groupName = group.name;
    }else{
        customer.groupName = '未分组';
    }
    return customer;
}

/**
 * 对所有通讯录成员进行分类
 * @param groups
 * @returns {function(*, *)}
 */
function sortMembersByGroup(groups,payload){
    let {dispatch, getState} = payload;
    let members = getState().addressBookReducer.originMember.list;
    var results;
    if(members || members == 'undefined'){
        results = addressBookUtil.sortAllMembersByGroup(groups,members);
    }else{
        results = [];
    }

    dispatch(fetchSortMembers(results));
}



function sortMembersTags(payload){
    let {dispatch, getState} = payload;
    let members = getState().addressBookReducer.originMember.list;
    let results = addressBookUtil.sortMembersTag(members);
    // console.log("sortMembersTags results",results);
    let arr = addressBookUtil.tagMapToList(results);
    dispatch(fetchTagList(arr));
}


/**
 * 获取标签列表
 * @returns {function(*, *)}
 */
export function getTagList(){
    return (dispatch,getState)=>{
        let payload = {
            dispatch,
            getState,
        }
        sortMembersTags(payload);
    }
}

/**
 * 根据标签获取通讯录信息
 * @param tagName
 * @returns {function(*, *)}
 */
export function getMembersByTag(tagName){
    return (dispatch,getState)=>{
        let payload = {
            dispatch,
            getState,
        }
        let tagList = getState().addressBookReducer.tagList.concat();
        let tagMember = tagList.find((item)=>{
            return item.name = tagName
        });
        let members = addressBookUtil.categryFriends(tagMember.data);
        dispatch(fetchAddressBookMemberByTag(members));
    }
}


/**
 * 修改客户备注
 * @param yanId
 * @param callback
 * @param errorCallback
 * @returns {function(*, *)}
 */
export function doSetRemark(yanId,name,callback,errorCallback) {
    return (dispatch , getState) => {
        HttpFetch(Api.setRemark,{
            yanId:yanId,
            name:name
        }).then((json)=>{
            let userInfo =getState().addressBookReducer.customerInfo ;
            let copyUserInfo = addressBookUtil.copyValue(userInfo);
            copyUserInfo.remark = name;
            dispatch(updateCustomerInfo(copyUserInfo));
            callback();
        }).catch((err)=>{
            Error(err);
            // console.log('客户信息修改失败==',err);
        })
    }
}

/**
 * 跳转聊天页
 * @param yanId
 * @param callback
 * @param errorCallback
 * @returns {function(*, *)}
 */
export function doChat(yanId,callback,errorCallback) {
    return (dispatch , getState) => {
        let user = getState().userReducer.userInfo;
        global.socket.emit("client_ctm_create_ctm_session",{
           cusYanId:yanId
        },function (data) {
            // console.log("发送成功",data);
            dispatch(setGroupSessionId(data.sessionId));
            callback(data);
        })
    }
}

/**
 * 增量更新
 * @param msg
 * @returns {function(*, *)}
 */
export function addressVersionChange(msg) {
    return (dispatch , getState)=>{
        let localVersion = getState().addressBookReducer.originMember.version;
        let originMemberList = getState().addressBookReducer.originMember.list.concat();
        let newMemberList=[];
        var flag = true;
        //先删除变更联系人
        if(msg.cusYanIds){
            for(var i=0;i<originMemberList.length;i++){
                for(var j=0;j<msg.cusYanIds.length;j++){
                    if(originMemberList[i].yanId==msg.cusYanIds[j]){
                        flag = false;
                        break;
                    }
                }
                if(flag){
                    newMemberList.push(originMemberList[i]);
                }else{
                    flag = true;
                }
            }
        }
        console.log('newMemberList',newMemberList);
        if(localVersion != msg.newVer){
            //根据变更列表获取联系人信息
            getCustomersById(msg.cusYanIds,(data)=>{
                //重组联系人
                for(var j=0;j<msg.cusYanIds.length;j++){
                    let param = msg.cusYanIds[j];
                    if(data[param]){
                        newMemberList.push(data[param]);
                    }
                }

                let membersInfo = {
                    "version":msg.newVer,
                    "list":newMemberList
                }
                const addressBookMember = addressBookUtil.categryFriends(membersInfo.list);
                //更新state
                dispatch(fetchAddressBookInfo(addressBookMember,membersInfo));
                addressBookDao.saveOriginMembers(membersInfo);
                addressBookDao.saveAddressBookVersion(membersInfo.version);
                console.log('newMemberList  11111',newMemberList);
            });
            // Modal.alert(
            //     '消息',
            //     '通讯录有变更，请重启应用',
            //     [
            //         // { text: 'Cancel', onPress: () => console.log('cancel'), style: 'cancel' },
            //         { text: '确定', onPress: () => console.log('ok') },
            //     ],
            // );
        }
    }
}

function getCustomersById(ids,callback){
    HttpFetch(Api.getCustomersById,{
        yanIds:ids,
    }).then((json)=>{
        console.log('getCustomersById return',json);
        callback(json);
    }).catch((err)=>{
        Error(err);
        console.log('getCustomersById==',err);
    })
}

function setGroupSessionId(sessionId) {
    return {
        type:Types.SET_SESSIONID_SUCCESS,
        sessionId:sessionId
    }
}

export function testAction(userInfo){
    return (dispatch,getState)=>{
        userInfo.groupName='test';
        dispatch(fetchCustomerInfo(userInfo));
    }
}

export const fetchAddressBookInfo=(addressBookMembers,originMember)=>{
    return {
        type:Types.FETCH_ADDRESSBOOK_DATA_SUCCESS,
        addressBookMembers,
        originMember,
    }
}


export const fetchGroupDataSuccess=(groupDatas)=>{
    return {
        type:Types.FETCH_GROUP_DATA_SUCCESS,
        groupDatas
    }
}

export const createGroupSuccess=(groupData)=>{
    return {
        type:Types.CREATE_GROUP_DATA_SUCCESS,
        groupData
    }
}

export const deleteGroupSuccess=(groupDatas)=>{
    return {
        type:Types.DELETE_GROUP_DATA_SUCCESS,
        groupDatas
    }
}

export const fetchMembersByGroup=(addressBookMembersByGroupId)=>{
    return {
        type:Types.FETCH_ADDRESSBOOK_BY_GROUP,
        addressBookMembersByGroupId
    }
}

export const fetchSortMembers=(addressBookGroupMembers)=>{
    return {
        type:Types.FETCH_SORT_MEMBERS_DATA,
        addressBookGroupMembers
    }
}

export const fetchCustomerInfo=(customerInfo)=>{
    return {
        type:Types.FETCH_CUSTOMER_INFO_DATA,
        customerInfo
    }
}

export const refreshOriginMember=(originMember)=>{
    return {
        type:Types.REFRESH_ORIGIN_MEMBER,
        originMember
    }
}


export const fetchTagList=(tagList)=>{
    return {
        type:Types.FETCH_TAG_LIST,
        tagList
    }
}
export const fetchAddressBookMemberByTag=(addressBookMembersByTag)=>{
    return {
        type:Types.FETCH_ADDRESSBOOK_BY_TAG,
        addressBookMembersByTag
    }
}

export const updateCustomerInfo=(customerInfo)=>{
    return {
        type:Types.UPDATE_CUSTOMER_INFO,
        customerInfo
    }
}