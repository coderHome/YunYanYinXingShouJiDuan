/**
 * Created by crcb on 2017/12/7.
 */

import * as Types from '../../constant/Types'
import * as Io from '../../socketIO/index'

function login_nav(url) {
    console.log("检测到登录成功，需要跳转页面，url = ", url);
    return (dispatch) => {
        dispatch(navPage(url));
    }
}

/**
 * 跳转页面
 * @param pagename
 * @returns {{type: string, url: *}}
 */
export function navPage(pagename) {
    return {
        type: "Reset",
        url: pagename
    }
}

export function reconnect(){
    return(dispatch) =>{
        Io.doReconnect();
    };
}


export function disConnent() {
    return {
        type:Types.DISCONNECT
    }
}


export function onConnect() {
    return {
        type:Types.CONNECT_SUCCESS
    }
}