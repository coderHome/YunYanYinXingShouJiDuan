import Psub from './pubsub'

/**
 * pubSub实例数据（单例）
 * @type {PubSub}
 */
!global.PubSub&&(global.PubSub = new Psub());


