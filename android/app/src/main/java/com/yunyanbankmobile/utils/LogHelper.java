package com.yunyanbankmobile.utils;

import android.util.Log;

public class LogHelper {

    private static String sRootTag = "RootTag";

    public static void setRootTag(String rootTag) {
        sRootTag = rootTag;
    }

    /**
     * 打印log详细信息
     */
    public static void d(String tag, String content) {
        Log.d(sRootTag + "_" + tag, getThreadName() + content);
    }

    /**
     * 打印log详细信息
     */
    public static void e(String tag, String content, Throwable t) {
        Log.e(sRootTag + "_" + tag, getThreadName() + content ,t);
    }

    /**
     * 打印log详细信息
     */
    public static void e(String content, Throwable t) {
        Log.e(sRootTag, getThreadName() + content ,t);
    }

    /**
     * 打印log详细信息
     */
    public static void e(String content) {
        Log.e(sRootTag, getThreadName() + content);
    }


    /**
     * 得到调用此方法的线程的线程名
     * 
     * @return
     */
    public static String getThreadName() {
        StringBuffer sb = new StringBuffer();
        sb.append(Thread.currentThread().getName());
        sb.append("-> ");
        sb.append(Thread.currentThread().getStackTrace()[3].getMethodName());
        sb.append("()");
        sb.append(" ");
        return sb.toString();
    }
}