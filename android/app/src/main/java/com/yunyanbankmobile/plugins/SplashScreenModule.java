package com.yunyanbankmobile.plugins;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.yunyanbankmobile.utils.SplashScreen;

/**
 * Created by wangdi on 14/11/16.
 */

public class SplashScreenModule extends ReactContextBaseJavaModule {


    public SplashScreenModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "SplashScreen";
    }

    @ReactMethod
    public void hide() {
        SplashScreen.hide(getCurrentActivity());
    }

}
