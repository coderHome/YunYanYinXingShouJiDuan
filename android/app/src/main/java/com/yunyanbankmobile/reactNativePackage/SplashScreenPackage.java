package com.yunyanbankmobile.reactNativePackage;

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;
import com.yunyanbankmobile.plugins.SplashScreenModule;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Created by kyle on 2018/1/26.
 */
public class SplashScreenPackage implements ReactPackage {
    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
        return Collections.emptyList();
    }

    @Override
    public List<NativeModule> createNativeModules(ReactApplicationContext reactContext) {
        List<NativeModule> modules = new ArrayList<>();

        modules.add(new SplashScreenModule(reactContext));

        return modules;
    }
}
