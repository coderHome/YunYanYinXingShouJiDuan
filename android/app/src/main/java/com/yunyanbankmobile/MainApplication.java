package com.yunyanbankmobile;

import android.app.Application;

import com.facebook.react.ReactApplication;
import net.no_mad.tts.TextToSpeechPackage;
import com.react.arron.speech.speechModulePackage;
import com.theweflex.react.WeChatPackage;
import com.yunyanbankmobile.reactNativePackage.SplashScreenPackage;
import com.zmxv.RNSound.RNSoundPackage;
import com.rnfs.RNFSPackage;
import com.rnim.rn.audio.ReactNativeAudioPackage;
import com.reactnative.ivpusic.imagepicker.PickerPackage;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.oblador.vectoricons.VectorIconsPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;

import cn.jpush.reactnativejpush.JPushPackage;

public class MainApplication extends Application implements ReactApplication {

    // 设置为 true 将不弹出 toast
    private boolean SHUTDOWN_TOAST = false;
    // 设置为 true 将不打印 log
    private boolean SHUTDOWN_LOG = false;

    private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
        @Override
        public boolean getUseDeveloperSupport() {
            return BuildConfig.DEBUG;
        }


        @Override
        protected List<ReactPackage> getPackages() {
            return Arrays.<ReactPackage>asList(
                    new MainReactPackage(),
            new TextToSpeechPackage(),
            new speechModulePackage(),
                    new RNSoundPackage(),
                    new RNFSPackage(),
                    new ReactNativeAudioPackage(),
                    new PickerPackage(),
                    new RNFetchBlobPackage(),
                    new RNDeviceInfo(),
                    new VectorIconsPackage(),
                    new WeChatPackage(),
                    new JPushPackage(SHUTDOWN_TOAST, SHUTDOWN_LOG),
                    new SplashScreenPackage()
            );
        }

        @Override
        protected String getJSMainModuleName() {
            return "index";
        }
    };

    @Override
    public ReactNativeHost getReactNativeHost() {
        return mReactNativeHost;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SoLoader.init(this, /* native exopackage */ false);
    }
}
